#!/usr/bin/env python3
import tensorflow as tf
import numpy as np
import horovod.tensorflow as hvd
learn = tf.contrib.learn
from model import models
from config import cfg


def train(inputs, config):
    image_placeholder = tf.placeholder(tf.float32, [cfg.batch_size, 28, 28, 1], name='image')
    label_placeholder = tf.placeholder(tf.int32, [cfg.batch_size,], name='label')
    training_placeholder = tf.placeholder(tf.bool, None, name='training')
    tf.logging.info('Loading Graph...')

    model = models()(image_placeholder, label_placeholder)

    tf.logging.info('Graph loaded')

    hooks = [
        # Horovod: BroadcastGlobalVariablesHook broadcasts initial variable states
        # from rank 0 to all other processes. This is necessary to ensure consistent
        # initialization of all workers when training is started with random weights
        # or restored from a checkpoint.
        hvd.BroadcastGlobalVariablesHook(0),

        # Horovod: adjust number of steps based on number of GPUs.
        # TODO make steps from cfg / hyperparameters w\epochs
        tf.train.StopAtStepHook(last_step=20000 // hvd.size()),

        tf.train.LoggingTensorHook(tensors={'step': model.global_step, 'loss': model.loss, 'acc': model.accuracy,
        # 'labels': model.labels, 'pred': model.argmax, 'Pr': model.precision, 'Sn': model.recall, 'auc': model.auc
        }, every_n_iter=10),
    ]

    # Horovod: save checkpoints only on worker 0 to prevent other workers from corrupting them.
    checkpoint_dir = model.ckpt_dir if hvd.rank() == 0 else None

    # The MonitoredTrainingSession takes care of session initialization,
    # restoring from a checkpoint, saving to a checkpoint, and closing when done
    # or an error occurs.
    with tf.train.MonitoredTrainingSession(checkpoint_dir=checkpoint_dir,
                                           hooks=hooks,
                                           #save_summaries_steps=,
                                           config=config) as mon_sess:
        while not mon_sess.should_stop():
            # Run a training step synchronously.
            image_, label_ = inputs.train.next_batch(cfg.batch_size)
            mon_sess.run(model.train_op, feed_dict={image_placeholder: np.reshape(image_, [-1, 28,28, 1]), label_placeholder: label_, training_placeholder: True})
