#!/usr/bin/env python

import tensorflow as tf
import horovod.tensorflow as hvd
learn = tf.contrib.learn
from model import Model
from config import cfg
from train import train


tf.logging.set_verbosity(tf.logging.INFO)


def main(_):
    tf.logging.info('Loading Graph...')
    model = Model()
    tf.logging.info('Graph loaded')

    # Horovod: save checkpoints only on worker 0 to prevent other workers from
    # corrupting them.
    checkpoint_dir = cfg.logdir if hvd.rank() == 0 else None

    # Create a supervisor
    sv = tf.train.Supervisor(logdir=checkpoint_dir, save_model_secs=0)
    # sv = tf.train.Supervisor(graph=model.graph, logdir=checkpoint_dir, save_model_secs=0)

    if cfg.is_training:
        tf.logging.info(' Start training...')
        train(model, sv)
        tf.logging.info('Training done')
    else:
        evaluation(model, sv)


    train(mnist, config, checkpoint_dir)


if __name__ == "__main__":
    tf.app.run()
