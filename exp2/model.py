import tensorflow as tf
layers = tf.contrib.layers
from config import cfg
from utils import get_batch_data
import horovod.tensorflow as hvd

class Model(object):
    def __init__(self, is_training=True):
        # self.graph = tf.Graph()
        # with self.graph.as_default():
        tf.logging.info('Initializing Horovod')
        hvd.init()
        if is_training:
            self.X, self.labels = get_batch_data(cfg.dataset, cfg.batch_size, cfg.num_threads)
            self.Y = tf.one_hot(self.labels, depth=cfg.num_classes, axis=1, dtype=tf.float32)

            self.build_arch(is_training)
            self.loss()
            self._summary()

            # t_vars = tf.trainable_variables()
            self.global_step = tf.Variable(0, name='global_step', trainable=False)
            self.optimizer()
            self.train_op = self.optimizer.minimize(self.loss, global_step=self.global_step)  # var_list=t_vars)
        else:
            self.X = tf.placeholder(tf.float32, shape=(cfg.batch_size, 28, 28, 1))
            self.labels = tf.placeholder(tf.int32, shape=(cfg.batch_size, ))
            self.Y = tf.reshape(self.labels, shape=(cfg.batch_size, 10, 1))
            self.build_arch(is_training)

        tf.logging.info('Seting up the main structure')

    def build_arch(self, is_training):

        with tf.variable_scope('conv_layer1'):
            h_conv1 = layers.conv2d(
                self.X, 32, kernel_size=[5, 5], activation_fn=tf.nn.relu)
            h_pool1 = tf.nn.max_pool(
                h_conv1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

        # Second conv layer will compute 64 features for each 5x5 patch.
        with tf.variable_scope('conv_layer2'):
            h_conv2 = layers.conv2d(
                h_pool1, 64, kernel_size=[5, 5], activation_fn=tf.nn.relu)
            h_pool2 = tf.nn.max_pool(
                h_conv2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
            # reshape tensor into a batch of vectors
            h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])

        # Densely connected layer with 1024 neurons.
        h_fc1 = layers.dropout(
            layers.fully_connected(
                h_pool2_flat, 1024, activation_fn=tf.nn.relu),
            keep_prob=0.5,
            is_training=is_training)

        # Compute logits (1 per class) and compute loss.
        self.logits = layers.fully_connected(h_fc1, 10, activation_fn=None)
        self.argmax = tf.to_int32(tf.argmax(self.logits, 1))
        # self.argmax = tf.reshape(self.argmax, shape=(cfg.batch_size, ))

    def loss(self):
        self.loss = tf.losses.softmax_cross_entropy(self.Y, self.logits)

    def optimizer(self):
        # Horovod: adjust learning rate based on number of GPUs.
        optimizer = tf.train.AdamOptimizer(cfg.learning_rate * hvd.size(), cfg.adam_beta1, cfg.adam_beta2, cfg.adam_eps)

        # Horovod: add Horovod Distributed Optimizer.
        self.optimizer = hvd.DistributedOptimizer(optimizer)

    # Summary
    def _summary(self):
        train_summary = []
        train_summary.append(tf.summary.scalar('train/loss', self.loss))
        input_img = tf.reshape(self.X, shape=(cfg.batch_size, 28, 28, 1))
        self.train_summary = tf.summary.merge(train_summary)

        correct_prediction = tf.equal(tf.to_int32(self.labels), self.argmax)
        self.accuracy = tf.reduce_sum(tf.cast(correct_prediction, tf.float32))
