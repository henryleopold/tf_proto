# Copyright 2016-2017 Henry Leopold. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Key performance indicator functions """
from __future__ import print_function, division, absolute_import
__author__ = 'Henry Leopold'

import cv2, numpy as np
import tensorflow as tf
import datetime
import time

from config import cfg
# from .visualizations import *
from six.moves import range, zip



__all__ = [
        # ================
        # Metrics
        # ----------------
            'acc',
            'mcc',
            'sensitivity',
            'specificity',
            'precision',
            'fscore',
            'gmean',
            'auc',
            'dice_accuracy',
        # DLTK Imports
            'tf_categorical_accuracy',
            'tf_categorical_dice',
            'dice',
            'absolute_volume',
            'np_categorical_dice',
            'distance_metric',
        # ================
        # Helpers
        # ----------------
            'streaming_perf',
            'get_performance',
            'calc_kpis',
            'print_dict',
            'get_global_performance',
            'dense_to_one_hot',
            'convert_to_one_hot',
        # ================
        # Confusion Matrix
        # ----------------
            # 'make_confusion_matrix'
            # 'calc_kappa',
            # 'calc_acc',
        # ================
        # tf related (imported from many others) TODO
        # ----------------


          ]

################################################################################
# Performance Metrics
################################################################################
def acc(tp=0, fp=0, fn=0, tn=0):
    """Returns accuracy"""
    return (tp+tn)/(tp + fp + fn + tn)

def mcc(tp=0, fp=0, fn=0, tn=0, smooth=1e-8):
    """Returns MCC / Matthew's Correlation Coeficient"""
    N = tp + fp + fn + tn
    P = (tp + fp) / N
    S = (tp + fn) / N
    return ((tp/N)-(S*P) + smooth)/(np.sqrt(P*S*(1-S)*(1-P)) + smooth)

def sensitivity(tp=0, fn=0, smooth=1e-8):
    """Returns sensitivity"""
    return (tp+smooth)/(tp+fn+smooth)

def specificity(tn=0, fp=0, smooth=1e-8):
    """Returns specifity"""
    return (tn+smooth)/(tn+fp+smooth)

def precision(tp=0, fp=0, smooth=1e-8):
    """Returns precision"""
    return (tp+smooth)/(tp+fp+smooth)

def fscore(tp=0, fp=0, fn=0, beta=cfg.fscore_beta, smooth=1e-8):
    """Returns F measure"""
    b2 = beta * beta
    top = (1. + b2) * tp
    bottom = top + (b2 * fn) + fp
    return ((top + smooth)/(bottom + smooth))

def gmean(sn, sp):
    """Returns G-mean"""
    return np.sqrt(sn*sp)

def auc(sn,sp):
    """Returns area under the curve"""
    return (sn+sp)/2

def kappa(tp=0, fp=0, fn=0, tn=0, smooth=1e-8):#TODO
    """Returns kappa score"""
    n1 = tp + fp
    m1 = tp + fn
    n0 = tn + fn
    m0 = tn + fp
    N = tp + fp + fn + tn
    o_acc = acc(tp=tp, fp=fp, fn=fn, tn=tn)
    e_acc = ((n1/N)*(m1/N))+((n0/N)*(m0/N))
    return (o_acc - e_acc + smooth) / (1 - e_acc + smooth)

#TODO find and credit source // make own.
def dice_accuracy(decoded_predictions, annotations, class_nums):
    DiceRatio = tf.constant(0,tf.float32)
    misclassnum = tf.constant(0,tf.float32)
    class_num   = tf.constant(class_nums,tf.float32)
    sublist   =  []
    for index in range(1,class_nums-2):
        current_annotation   =     tf.cast(tf.equal(tf.ones_like(annotations)*index,\
                                        annotations),tf.float32)
        cureent_prediction   =     tf.cast(tf.equal(tf.ones_like(decoded_predictions)*index,\
                                            decoded_predictions),tf.float32)
        Overlap              =     tf.add(current_annotation,cureent_prediction)
        Common               =     tf.reduce_sum(tf.cast(tf.equal(tf.ones_like(Overlap)*2,Overlap),\
                                                            tf.float32),[0,1,2,3])
        annotation_num       =     tf.reduce_sum(current_annotation,[0,1,2,3])
        predict_num          =     tf.reduce_sum(cureent_prediction,[0,1,2,3])
        all_num              =     tf.add(annotation_num,predict_num)
        Sub_DiceRatio        =     Common*2/tf.clip_by_value(all_num, 1e-10, 1e+10)
        misclassnum          =     tf.cond(tf.equal(Sub_DiceRatio,0.0), lambda: misclassnum + 1, lambda: misclassnum)
        sublist.append(Sub_DiceRatio)
        DiceRatio            =     DiceRatio + Sub_DiceRatio
    DiceRatio                =     DiceRatio/tf.clip_by_value(tf.cast((class_num-misclassnum-3),tf.float32),1e-10,1e+1000)
    return DiceRatio, sublist

# ==============================================================================
# Credit: Deep Learning Toolkit (DLTK) for Medical Imaging
# ==============================================================================
#TODO make dltk own
def tf_categorical_accuracy(pred, truth):
    """ Accuracy metric """
    return tf.reduce_mean(tf.cast(tf.equal(pred, truth), dtype=tf.float32))

def tf_categorical_dice(pred, truth, k):
    """ Dice overlap metric for label k """
    A = tf.cast(tf.equal(pred, k), dtype=tf.float32)
    B = tf.cast(tf.equal(truth, k), dtype=tf.float32)
    return 2 * tf.reduce_sum(tf.multiply(A, B)) / (tf.reduce_sum(A) + tf.reduce_sum(B))

def dice(predictions, labels, num_classes):
    """Calculates the categorical Dice similarity coefficients for each class between labels and predictions.
    # Credit: Deep Learning Toolkit (DLTK) for Medical Imaging
    Args:
        predictions (np.ndarray): predictions
        labels (np.ndarray): labels
        num_classes (int): number of classes to calculate the dice coefficient for
    Returns:
        np.ndarray: dice coefficient per class
    """
    dice_scores = np.zeros((num_classes))
    for i in range(num_classes):
        tmp_den = (np.sum(predictions == i) + np.sum(labels == i))
        tmp_dice = 2. * np.sum((predictions == i) * (labels == i)) / \
            tmp_den if tmp_den > 0 else 1.
        dice_scores[i] = tmp_dice
    return dice_scores.astype(np.float32)

def absolute_volume(predictions, labels, num_classes):
    """Calculates the absolute volume difference for each class between labels and predictions.
    Args:
        predictions (np.ndarray): predictions
        labels (np.ndarray): labels
        num_classes (int): number of classes to calculate avd for
    Returns:
        np.ndarray: avd per class
    """
    avd = np.zeros((num_classes))
    eps = 1e-6
    for i in range(num_classes):
        avd[i] = np.abs(np.sum(predictions == i) - np.sum(labels == i)
                        ) / (np.float(np.sum(labels == i)) + eps)

    return avd.astype(np.float32)

def np_categorical_dice(pred, truth, k):
    """ Dice overlap metric for label k """
    A = (pred == k).astype(np.float32)
    B = (truth == k).astype(np.float32)
    return 2 * np.sum(A * B) / (np.sum(A) + np.sum(B))

def distance_metric(seg_A, seg_B, dx):
    """
        Measure the distance errors between the contours of two segmentations.
        The manual contours are drawn on 2D slices.
        We calculate contour to contour distance for each slice.
        """
    table_md = []
    table_hd = []
    X, Y, Z = seg_A.shape
    for z in range(Z):
        # Binary mask at this slice
        slice_A = seg_A[:, :, z].astype(np.uint8)
        slice_B = seg_B[:, :, z].astype(np.uint8)

        # The distance is defined only when both contours exist on this slice
        if np.sum(slice_A) > 0 and np.sum(slice_B) > 0:
            # Find contours and retrieve all the points
            _, contours, _ = cv2.findContours(cv2.inRange(slice_A, 1, 1),
                                              cv2.RETR_EXTERNAL,
                                              cv2.CHAIN_APPROX_NONE)
            pts_A = contours[0]
            for i in range(1, len(contours)):
                pts_A = np.vstack((pts_A, contours[i]))

            _, contours, _ = cv2.findContours(cv2.inRange(slice_B, 1, 1),
                                              cv2.RETR_EXTERNAL,
                                              cv2.CHAIN_APPROX_NONE)
            pts_B = contours[0]
            for i in range(1, len(contours)):
                pts_B = np.vstack((pts_B, contours[i]))

            # Distance matrix between point sets
            M = np.zeros((len(pts_A), len(pts_B)))
            for i in range(len(pts_A)):
                for j in range(len(pts_B)):
                    M[i, j] = np.linalg.norm(pts_A[i, 0] - pts_B[j, 0])

            # Mean distance and hausdorff distance
            md = 0.5 * (np.mean(np.min(M, axis=0)) + np.mean(np.min(M, axis=1))) * dx
            hd = np.max([np.max(np.min(M, axis=0)), np.max(np.min(M, axis=1))]) * dx
            table_md += [md]
            table_hd += [hd]

    # Return the mean distance and Hausdorff distance across 2D slices
    mean_md = np.mean(table_md) if table_md else None
    mean_hd = np.mean(table_hd) if table_hd else None
    return mean_md, mean_hd

################################################################################
# Helpers #TODO TF KPIs
################################################################################
def streaming_perf(mask, labels, set_id, save_path = None, weights=None, collection=None):
    performance = dict()
    labels = tf.reshape(labels, [-1])
    preds = tf.reshape(mask, [-1])
    with tf.Session() as sess:
        img_id = set_id.eval()
        performance['auc_%i'%img_id] = tf.contrib.metrics.streaming_auc(preds, labels, weights, metrics_collections=collection, name = 'auc_%i'%img_id)
        performance['acc_%i'%img_id] = tf.contrib.metrics.streaming_accuracy(preds, labels, weights, metrics_collections=collection, name = 'acc_%i'%img_id)
        performance['fn_%i'%img_id] = tf.contrib.metrics.streaming_false_negatives(preds, labels, weights, metrics_collections=collection, name = 'fn_%i'%img_id)
        performance['fp_%i'%img_id] = tf.contrib.metrics.streaming_false_positives(preds, labels, weights, metrics_collections=collection, name = 'fp_%i'%img_id)
        performance['tn_%i'%img_id] = tf.contrib.metrics.streaming_true_negatives(preds, labels, weights, metrics_collections=collection, name = 'tn_%i'%img_id)
        performance['tp_%i'%img_id] = tf.contrib.metrics.streaming_true_positives(preds, labels, weights, metrics_collections=collection, name = 'tp_%i'%img_id)
        if save_path:
            save_pickle(performance, save_path)
        return performance

# def streaming_perf(mask, labels, save_path = None, weights=None, collection=None):
#     performance = dict()
#     labels = tf.reshape(labels, [-1])
#     for img_id in range(cfg.batch_size):
#         preds = tf.reshape(mask[img_id], [-1])
#         performance['auc_%i'%img_id] = tf.contrib.metrics.streaming_auc(preds, labels, weights, metrics_collections=collection, name = 'auc_%i'%img_id)
#         performance['acc_%i'%img_id] = tf.contrib.metrics.streaming_accuracy(preds, labels, weights, metrics_collections=collection, name = 'acc_%s'%img_id)
#         performance['fn_%i'%img_id] = tf.contrib.metrics.streaming_false_negatives(preds, labels, weights, metrics_collections=collection, name = 'fn_%i'%img_id)
#         performance['fp_%i'%img_id] = tf.contrib.metrics.streaming_false_positives(preds, labels, weights, metrics_collections=collection, name = 'fp_%i'%img_id)
#         performance['tn_%i'%img_id] = tf.contrib.metrics.streaming_true_negatives(preds, labels, weights, metrics_collections=collection, name = 'tn_%i'%img_id)
#         performance['tp_%i'%img_id] = tf.contrib.metrics.streaming_true_positives(preds, labels, weights, metrics_collections=collection, name = 'tp_%i'%img_id)
#     return performance

def _get_performance(prediction, label, mask=None):
    """ Calculates the performance metrics on an example and masked them if provided
    Args:
        prediction
        label
        mask (optional):
    Returns:

    """
    #TODO This is presently configured for segmentation of 2 classes; need to modify for more or use confusion matrices...
    # print('pred:%s\nlabel:%s\n'%(prediction.mean(), label.mean()))
    if mask is None:
        mask = np.ones(prediction.shape, dtype=np.uint8)
    prediction = np.multiply(prediction, mask)
    label = np.multiply(label, mask)
    not_prediction = np.logical_not(prediction)
    not_label = np.logical_not(label)
    tp = (prediction * label).sum()
    fp = (prediction * not_label).sum()
    fn = (not_prediction * label).sum()
    tn = (not_prediction * not_label).sum()
    if cfg.debug: print('tp:%i\nfp:%i\nfn:%i\ntn:%i'%(tp,fp,fn,tn))
    assert label.size == tp + fp + fn + tn, "Total did not sum up!\nlabel.size (%i) != tp + fp + fn + tn (%s)\nPred:%s, Label:%s"%(label.size,tp + fp + fn + tn,prediction.shape, label.shape)
    return calc_kpis(tp=tp, fp=fp, fn=fn, tn=tn)

def get_performance(prediction, label, minval=0.95,mask=None):
    """ Wrapper for get_performance that binarizes label images.
    Args:
        prediction
        label
        minval
        mask (optional):
    Returns:

    """
    if prediction.ndim == 3:
        prediction = np.squeeze(prediction)

    with tf.variable_scope('analysis'):
            # Process incoming data
        if cfg.debug:
            print('pred:', np.max(prediction), np.min(prediction))
            print('shape:', prediction.shape)
        binary_pred = np.zeros(prediction.shape, dtype=np.float64)
        binary_pred[np.greater_equal(prediction,minval)] = 1
        if cfg.debug:
            print('bin_pred:',np.max(binary_pred), np.min(binary_pred))
            print('\nlabel:', np.max(label), np.min(label))
        binary_label = np.zeros(label.shape, dtype=np.float64)
        binary_label[np.greater_equal(label, 1e-3)] = 1
        if cfg.debug:
            print('shape:', binary_label.shape)
            print('binary_label:',np.max(binary_label), np.min(binary_label))
            print('\nmask:', np.max(mask), np.min(mask))
            print('shape:', mask.shape)
        return _get_performance(binary_pred, binary_label, mask)
        # results = streaming_perf(pred, label, set_id)


def calc_kpis(tp=0, fp=0, fn=0, tn=0):
    """ Generator function for calculating various kpis from base performance metrics
    Args:

    Returns:

    """
    results = dict()
    sn = sensitivity(tp=tp,fn=fn)
    sp = specificity(tn=tn,fp=fp)
    results['tp'] = tp
    results['fp'] = fp
    results['tn'] = tn
    results['fn'] = fn
    if cfg.debug: print_dict(results)
    results['sn'] = sn
    results['sp'] = sp
    results['acc'] = acc(tp=tp, fp=fp, fn=fn, tn=tn)
    results['auc'] = auc(sn=sn,sp=sp)
    if cfg.kappa:
        results['kappa'] = kappa(tp=tp, fp=fp, fn=fn, tn=tn)
    if cfg.mcc:
        results['mcc'] = mcc(tp=tp, fp=fp, fn=fn, tn=tn)
    if cfg.gmean:
        results['gmean'] = gmean(sn=sn,sp=sp)
    if cfg.fscore:
        results['fscore-%.0f'%cfg.fscore_beta] = fscore(tp=tp, fp=fp, fn=fn, beta=cfg.fscore_beta)
    if cfg.precision:
        results['precision'] = precision(tp=tp, fp=fp)
    # if cfg.dice:
    #     results['dice'] = 0 #TODO
    return results

def print_dict(d):
    for k, v in d.iteritems():
        print('%s: %s'%(k, v))

def get_global_performance(results):
    """ Unrolls all the results for an entire dataset and recalculates the network performance. """
    tp = 0
    fp = 0
    tn = 0
    fn = 0
    for result in results.values():
        tp += result['performance']['tp']
        fp += result['performance']['fp']
        tn += result['performance']['tn']
        fn += result['performance']['fn']
    return calc_kpis(tp=tp, fp=fp, fn=fn, tn=tn)

def convert_to_one_hot(Y, C):
    Y = np.eye(C)[Y.reshape(-1)].T
    return Y

def dense_to_one_hot(labels, n_classes=2):
    """Convert class labels from scalars to one-hot vectors."""
    labels = np.array(labels)
    n_labels = labels.shape[0]
    index_offset = np.arange(n_labels) * n_classes
    one_hot = np.zeros((n_labels, n_classes), dtype=np.float32)
    one_hot.flat[index_offset + labels.ravel()] = 1
    return one_hot
################################################################################
# Confusion Matrix
################################################################################

def confusion_mat2csv(confusion_matrix):
    csv_row = ""
    for i in range(confusion_matrix.shape[0]):
        for j in range(confusion_matrix.shape[1]):
            csv_row += ", " + str(confusion_matrix[i, j])
    return csv_row

def calc_kappa(confusion_matrix): #TODO
	num_class, _ = confusion_matrix.shape
	total = sum(sum(confusion_matrix))

	correct = 0
	for i in range(num_class):
		correct += confusion_matrix[i,i]
	o_acc = float(correct) / total
	print("o_acc", o_acc)

	exs = np.zeros(num_class)
	for i in range(num_class):
		exs[i] = sum(confusion_matrix[:,i]) * sum(confusion_matrix[i,:]) / total
	e_acc = sum(exs) / total
	print("e_acc", e_acc)

	kappa = (o_acc - e_acc) / (1 - e_acc)
	return kappa

def calc_acc(confusion_matrix, smooth=1e-8):
    acc = (np.diag(confusion_matrix).sum() + smooth) / (confusion_matrix.sum() + smooth)
    miu = np.diag(confusion_matrix) / (confusion_matrix.sum(1) + confusion_matrix.sum(0) - np.diag(confusion_matrix))
    return acc, miu


def make_confusion_matrix(labels, ground_truth):
    pass

def fast_hist(a, b, n):
    k = (a >= 0) & (a < n)
    return np.bincount(n * a[k].astype(int) + b[k], minlength=n**2).reshape(n, n)

def get_hist(predictions, labels):
    num_class = predictions.shape[3]
    batch_size = predictions.shape[0]
    hist = np.zeros((num_class, num_class))
    for i in range(batch_size):
        hist += fast_hist(labels[i].flatten(), predictions[i].argmax(2).flatten(), num_class)
    return hist

def print_hist_summery(hist):
    acc_total = np.diag(hist).sum() / hist.sum()
    print ('accuracy = %f'%np.nanmean(acc_total))
    iu = np.diag(hist) / (hist.sum(1) + hist.sum(0) - np.diag(hist))
    print ('mean IU  = %f'%np.nanmean(iu))
    for ii in range(hist.shape[0]):
        if float(hist.sum(1)[ii]) == 0:
            acc = 0.0
        else:
            acc = np.diag(hist)[ii] / float(hist.sum(1)[ii])
        print("    class # %d accuracy = %f "%(ii, acc))

def per_class_acc(predictions, label_tensor):
    labels = label_tensor
    size = predictions.shape[0]
    num_class = predictions.shape[3]
    hist = np.zeros((num_class, num_class))
    for i in range(size):
        hist += fast_hist(labels[i].flatten(), predictions[i].argmax(2).flatten(), num_class)
    acc_total = np.diag(hist).sum() / hist.sum()
    print ('accuracy = %f'%np.nanmean(acc_total))
    iu = np.diag(hist) / (hist.sum(1) + hist.sum(0) - np.diag(hist))
    print ('mean IU  = %f'%np.nanmean(iu))
    for ii in range(num_class):
        if float(hist.sum(1)[ii]) == 0:
            acc = 0.0
        else:
            acc = np.diag(hist)[ii] / float(hist.sum(1)[ii])
        print("    class # %d accuracy = %f "%(ii,acc))

def tf_accuracy(logits, labels):
    softmax = tf.nn.softmax(logits)
    argmax = tf.argmax(softmax, 3)
    shape = logits.get_shape().as_list()
    n = shape[3]
    one_hot = tf.one_hot(argmax, n, dtype=tf.float32)
    equal_pixels = tf.reduce_sum(tf.to_float(color_mask(one_hot, labels)))
    total_pixels = reduce(lambda x, y: x * y, shape[:3])
    return equal_pixels / total_pixels

#below credit to ultrasound nerve seg # TODO
def get_f_stats(predictions, labels,
                true_positives=0, false_positives=0,
                false_negatives=0, true_negatives=0):
    preds = np.argmax(predictions, axis=3)
    not_preds = np.logical_not(preds)
    not_labels = np.logical_not(labels)
    tp = (preds * labels).sum()
    fp = (preds * not_labels).sum()
    fn = (not_preds * labels).sum()
    tn = (not_preds * not_labels).sum()
    assert labels.size == tp + fp + fn + tn, "Total did not sum up!"
    return (true_positives + tp, false_positives + fp,
            false_negatives + fn, true_negatives + tn)

def f_beta_score(y_pred, y_true, beta=1.0, smooth=1e-8):
    """Returns F measure using tensorflow"""
    preds = tf.argmax(y_pred, 3)
    tp = tf.cast(tf.reduce_sum(preds * y_true), dtype=tf.float32)
    fp = tf.reduce_sum(tf.cast(tf.greater(preds, y_true), dtype=tf.float32))
    fn = tf.reduce_sum(tf.cast(tf.less(preds, y_true), dtype=tf.float32))
    b2 = beta * beta
    top = (1. + b2) * tp
    bottom = top + (b2 * fn) + fp
    return ((top + smooth)/(bottom + smooth))

def np_f_beta_score(true_positives=0, false_positives=0,
                    false_negatives=0, beta=1.0, smooth=1e-8):
    """Returns F measure using numpy"""
    b2 = beta * beta
    top = (1. + b2) * true_positives
    bottom = top + (b2 * false_negatives) + false_positives
    return ((top + smooth)/(bottom + smooth))

def print_data_stats(set_name, x, y):
    print("\n{}:".format(set_name))
    print("    ... x: {}, dtype: {}".format(x.shape, x.dtype))
    print("           max: {}, min: {}".format(x.max(), x.min()))
    print("           mean: {}, std: {}".format(x.mean(), x.std()))
    print("\n    ... y: {}, dtype: {}".format(y.shape, y.dtype))
    print("           max: {}, min: {}".format(y.max(), y.min()))
