# Copyright 2016-2017 Henry Leopold. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" General data loading and processing """
from __future__ import print_function, division, absolute_import
__author__ = 'Henry Leopold'

from glob import glob
from PIL import Image as im
import os, cv2, numpy as np
import tensorflow as tf
from sklearn.model_selection import GroupKFold
from config import cfg
from utils import build_path, write2csv, load_image_filenames, make_dir
from tqdm import trange
import matplotlib.pyplot as plt



__all__ = [
        # =====================
        # Dataset
        # ---------------------
            'get_pair_filenames',
            'load_image_files',
            'save_image',
            'build_xval_index',
            'idx2list',
            'build_image_set',
            'load_image_set',
            'find',
            'preview_dataset',
        # =======================
        # Tensorflow Records
        # -----------------------
            'create_tfrecords',
            'write_tfrecords',
        # ========================
        # Processors
        # ------------------------
        # Tensorflow processors
        # ------------------------
            'read_tfrecord',
            'inputs',
            'online_pair_selector',
            #'_generate_image_label_batch', #internal function
            #'_generate_imageset_batch', #internal function
        # ========================
        # Image processors (WIP)
        # ------------------------
            'read_imageset',
            'process_imageset',
        ]

# ==============================================================================
# Dataset
# ==============================================================================
def get_pair_filenames(filenames, image_ext = None, mask_ext=cfg.mask_ext, rtype='array'):
    """
    Args:
    Returns:
    """
    mask_filenames = [s for s in filenames if mask_ext in s]
    image_filenames = [s for s in filenames if mask_ext not in s]
    pair_filenames = []
    pair_dict = {}
    images = []
    masks = []
    keys = []
    for image in image_filenames:
        key = image[:-4].rsplit('/')[-1]
        mask = [s for s in mask_filenames if key in s][0]
        images.append(image)
        masks.append(mask)
        keys.append(key)
        pair_filenames.append((image, mask, key))
        pair_dict[key] = {'image':image, 'mask':mask}
    if rtype is 'dict': return pair_dict
    elif rtype is 'array': return np.array(pair_filenames)
    elif rtype is 'marray': return images, masks, keys

def load_image_files(filenames, stage='test', rtype='array'):
    """
    Args:
    Returns:
    """
    image_dict = dict()
    image_set = []
    # if not cfg.use_xval:
    pair_filenames = get_pair_filenames(filenames)
    for pair in pair_filenames:
        image, mask, set_id = load_image_set(pair, write_tfrecords=False)
        image_dict[set_id] = {'image':image, 'mask':mask}
        image_set.append([image, mask, set_id])
    # if cfg.use_xval:
    #     for j in xrange(len(image_sets)):
    #         if cfg.debug: print('Image: %s, Mask: %s, Pair: %s'%(image_sets[j][0], image_sets[j][1],image_sets[j][2]))
    #         image, mask, set_id = load_image_set([image_sets[j][0], image_sets[j][1], image_sets[j][2]])
    #         image_dict[set_id] = {'image':image, 'mask':mask}
    #         image_set.append([image, mask, set_id])
    if rtype is 'dict': return image_dict
    elif rtype is 'array': return np.array(image_set)

def save_image(data, path, channels = 3):
    """
    Args:
    Returns:
    """
    image = np.zeros((data.shape[0], data.shape[1], channels))
    # image[:,:,0] = r/1.0
    # image[:,:,1] = g/1.0
    # image[:,:,2] = b/1.0
    img = im.fromarray(np.uint8(image))
    img.save(path)

def storeImageQueue(data, labels, step):
    """ data and labels are all numpy arrays """
    for i in range(cfg.batch_size):
        index = 0
        img = data[i]
        label = labels[i]
        saveImage(img, build_path([path,"batch_img_s%d_%d.png"%(step,i)]))
        saveImage(label, build_path([path,"batch_label_s%d_%d.png"%(step,i)]))
        # writeImage(np.reshape(labels,(data.shape[0], data.shape[1])), "batch_labels_s%d_%d.png"%(step,i))

# def writeImage(image, filename):
#     """ store label data to colored image """
#     Sky = [128,128,128]
#     Building = [128,0,0]
#     Pole = [192,192,128]
#     Road_marking = [255,69,0]
#     Road = [128,64,128]
#     Pavement = [60,40,222]
#     r = image.copy()
#     g = image.copy()
#     b = image.copy()
#     label_colours = np.array([Sky, Building, Pole, Road, Road_marking, Pavement])
#     for l in range(0,6):
#         r[image==l] = label_colours[l,0]
#         g[image==l] = label_colours[l,1]
#         b[image==l] = label_colours[l,2]
#     rgb = np.zeros((image.shape[0], image.shape[1], 3))
#     rgb[:,:,0] = r/1.0
#     rgb[:,:,1] = g/1.0
#     rgb[:,:,2] = b/1.0
#     im = Image.fromarray(np.uint8(rgb))
#     im.save(filename)

def build_xval_index(filenames, n_splits=cfg.xval_splits, clip_key_f=cfg.xval_key_clip_f, clip_key_b=cfg.xval_key_clip_b, mask_ext=cfg.mask_ext):
    """ Splits a dataset into training and testing sets
    Args:
        filenames (str):
        n_splits (int):
        clip_key_f (int):
        clip_key_b (int):
    Returns:
        Sets of filenames for training and testing sets (X_train, y_train, X_test, y_test)
    """
    pairs = get_pair_filenames(filenames, mask_ext=mask_ext)
    tr_idx = [] # list to store training sets
    tst_idx = [] # list to store test sets
    X = []
    y = []
    keys = []
    for pair in pairs:
        X.append(pair[0])
        y.append(pair[1])
        keys.append(pair[2][clip_key_f:len(pair[2])-clip_key_b])
    kf = GroupKFold(n_splits=n_splits)
    kf.get_n_splits(X, y, keys)
    # return kf.split(X, y, keys)
    for train_index, test_index in kf.split(X, y, keys):
        tr_idx.append(train_index)
        tst_idx.append(test_index)
        if cfg.debug:
            print("TRAIN:", train_index, "TEST:", test_index)
    return tr_idx, tst_idx, X, y, keys

def idx2list(idx, data):
    """
    Args:
    Returns:
    """
    return [data[i] for i in idx]

def build_image_set(data_dir, file_ext=None):
    """
    Args:
    Returns:
    """
    if not cfg.use_xval:
        filenames = load_image_filenames(data_dir) #build_path([cfg.root_dir, cfg.data_dir, cfg.test_dir])
        image_set = get_pair_filenames(filenames)
    if cfg.use_xval:
        for i in trange(cfg.xval_splits):
            image_set = []
            xval_records = read_from_csv((build_path([data_dir, file_ext+'_%i'%i+'.csv'])), has_header=False)
            for record in xval_records:
                if cfg.debug: print('Image: %s, Mask: %s, Pair: %s'%(record[0], record[1],record[2]))
                image_set.append((record[0], record[1],record[2]))
    return image_set

def load_image_set(image_set, write_tfrecords=False, xval=None):
    """
    Args:
    Returns:
    """
    print(image_set[0])
    image = np.array(im.open(image_set[0]), dtype = np.uint8)
    mask = np.array(im.open(image_set[1]), dtype = np.uint8)
    mask = mask[...,np.newaxis]
    if len(image_set) < 3:
        set_id = get_file_id(image_set[0])
    else:
        set_id = image_set[2]
    if cfg.debug: print("Image Shape: %s\nMask Shape: %s"%(image.shape, mask.shape))
    if write_tfrecords:
        if cfg.split_channels:
            image = np.zeros([image.shape[0],image.shape[1],1], dtype=np.uint8)
            for e,c in enumerate(['r','g','b']):
                image = image[:,:,e]
                image = image[...,np.newaxis]
                write_tfrecords(image, mask, image_set[2], name=cfg.tfrecord_name+'_'+c, xval=xval)
        else:
            write_tfrecords(image, mask, set_id, name=cfg.tfrecord_name, xval=xval)
    else:
        return image, mask, set_id

def find(lst, m, y):
    """ Wrapper for conditional retrival from a list.
    Args:
        lst (list)
        m (str): Method
        y: parameter to search by
    Returns:
        List of all indexes matching query
    """
    try:
        if m == 'string':
            return [i for i, x in enumerate(lst) if y in str(x)]
        elif m == 'starts':
            return [i for i, x in enumerate(lst) if str(x).startswith(y)]
        elif m == 'ends':
            return [i for i, x in enumerate(lst) if str(x).endswith(y)]
        elif m == 'less':
            return [i for i, x in enumerate(lst) if x<y]
        elif m == 'more':
            return [i for i, x in enumerate(lst) if x>y]
        elif m == 'eq':
            return [i for i, x in enumerate(lst) if x==y]
        elif m == 'eqg':
            return [i for i, x in enumerate(lst) if x>=y]
        elif m == 'eql':
            return [i for i, x in enumerate(lst) if x<=y]
    except TypeError as e:
        print('Nothing found, or error in input.\n%s'%e)

def preview_dataset(dataset, key_dict=None):
    """ Wrapper for showing the image contents of a dataset.
    Args:
        dataset (dict): Numpy dictionary (generated by test at present).
        key_dict (dict): Dictionary of keys and colour spaces. (WIP)
    Returns:
        None.
    """
    l = len(dataset.keys())
    plt.figure(figsize=(4*5, l*5))
    for i, k in enumerate(dataset.keys()):
        plt.subplot(l,4,i*4+1),plt.imshow(dataset[k]['image'])
        plt.subplot(l,4,i*4+2),plt.imshow(dataset[k]['raw_pred'],'gray')
        plt.subplot(l,4,i*4+3),plt.imshow(dataset[k]['label'],'gray')
        plt.subplot(l,4,i*4+4),plt.imshow(dataset[k]['mask'],'gray')
    plt.show()

# ==============================================================================
# Tensorflow Records
# ==============================================================================
def _bytes_feature(value):
    """
    Args:
    Returns:
    """
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))
def _int64_feature(value):
    """
    Args:
    Returns:
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def create_tfrecords(split_channels=cfg.split_channels):
    """
    Args:
    Returns:
    """
    filenames = load_image_filenames(build_path([cfg.root_dir, cfg.data_dir, cfg.train_dir]))
    if not cfg.use_xval:
        pair_filenames = get_pair_filenames(filenames)
        for pair in pair_filenames:
            load_image_set(pair, write_tfrecords=True)
    if cfg.use_xval:
        tr_idx, tst_idx, X, y, keys = build_xval_index(filenames)
        for i in trange(len(tr_idx)):
            train_set = []
            for image, msk, key in zip(idx2list(tr_idx[i], X), idx2list(tr_idx[i], y), idx2list(tr_idx[i], keys)):
                if cfg.debug: print('Image: %s, Mask: %s, Pair: %s'%(image, msk,key))
                train_set.append([image, msk, key])
                load_image_set([image, msk, key], xval=str(i), write_tfrecords=True)
            write2csv(build_path([cfg.tfrecord_dir, cfg.exp_id, 'train_%i'%i+'.csv']),train_set)
        for i in trange(len(tst_idx)):
            test_set = []
            for image, msk, key in zip(idx2list(tst_idx[i], X), idx2list(tst_idx[i], y), idx2list(tst_idx[i], keys)):
                test_set.append([image, msk, key])
                if cfg.debug: print('Image: %s, Mask: %s, Pair: %s'%(image, msk,key))
            write2csv( build_path([cfg.tfrecord_dir, cfg.exp_id, 'test_%i'%i+'.csv']),test_set)

def write_tfrecords(image, mask, image_id, name=cfg.tfrecord_name, xval=None):
    """
    Function wrapper for creating tfrecords from image files.
    At present, source datafiles are selected by config flags.
    Args:
        name (string): name for tfrecords
        split_channels (boolean): If true, saves individual channels.
    Returns: None
    """
    if xval is None:
        record_filenames = build_path(
            [cfg.tfrecord_dir, cfg.exp_id, name + '.tfrecords'])
    else:
        make_dir(build_path([cfg.tfrecord_dir, cfg.exp_id, 'xval_'+str(xval)]))
        record_filenames = build_path(
            [cfg.tfrecord_dir, cfg.exp_id, 'xval_'+str(xval), name + '.tfrecords'])
    writer = tf.python_io.TFRecordWriter(record_filenames)
    shape = image.shape
    image = preprocess_image(image).ravel().tostring()
    mask = preprocess_mask(mask).ravel().tostring()

    # create record and write it
    record = tf.train.Example(features=tf.train.Features(feature={
        'image': _bytes_feature(image),
        'mask': _bytes_feature(mask),
        'height': _int64_feature(shape[0]),
        'width': _int64_feature(shape[1]),
        'depth': _int64_feature(shape[2]),
        'id': _bytes_feature(image_id)
    }))
    writer.write(record.SerializeToString())


# ==============================================================================
# Processors
# ==============================================================================
# TFRecord Processors
# -------------------------------------------------------------------------
def read_tfrecord(filename_queue):
    """ Reads data from tfrecord files.
    Args:
        filename_queue: A que of strings with filenames
    Returns:
        image (tensor): Images tensor of [height, width, num_channels] size.
        mask (tensor): Masks tensor of [height, width, 1] size. (eventually 1 will be replaced with num_classes)
    """
    reader = tf.TFRecordReader()
    key, serialized_example = reader.read(filename_queue)
    features = tf.parse_single_example(
        serialized_example,
        features={
            'image': tf.FixedLenFeature([], tf.string),
            'mask': tf.FixedLenFeature([], tf.string),
            'height': tf.FixedLenFeature([], tf.int64),
            'width': tf.FixedLenFeature([], tf.int64),
            'depth': tf.FixedLenFeature([], tf.int64),
            'id': tf.FixedLenFeature([], tf.string)
        })
    height = tf.cast(features['height'], tf.int32)
    width = tf.cast(features['width'], tf.int32)
    depth = tf.cast(features['depth'], tf.int32)
    image = tf.decode_raw(features['image'], tf.uint8)
    mask = tf.decode_raw(features['mask'], tf.uint8)
    set_id = tf.cast(features['id'], tf.string)
    image = tf.reshape(image, [height, width, cfg.num_channels])
    # image = tf.transpose(image, [1,0,2])
    mask = tf.reshape(mask, [height, width, cfg.mask_channels])
    # mask = tf.transpose(mask, [1,0,2])
    if cfg.use_fp16:
        dtype=tf.float16
    else:
        dtype=tf.float32
    image = tf.cast(image, dtype)
    mask = tf.cast(mask, dtype)
    if cfg.lab_cs:
        image = image / 255.0  # shift to [0,1] for conversion to Lab
        image = rgb_to_lab(image) # Unconfirmed, but lab should be [-1,1]
    else:
        image = ((image - 127.5) / 127.5) # shift to [-1,1]
    mask = mask / 255.0
    return image, mask, height, width, depth, set_id

def inputs(filenames, batch_size):
    """ Plain input reader ops.
    Args:
        filenames (str): list of filenames.
        batch_size (int): Number of images per batch.
    Returns:
        images (tensor): Images tensor of [batch_size, height, width, num_channels] size.
        masks (tensor): Masks tensor of [batch_size, height, width, 1] size. (eventually 1 will be replaced with num_classes)
    """
    filename_queue = tf.train.string_input_producer(filenames)
    image, mask, height, width, depth, img_id = read_tfrecord(filename_queue)

    if cfg.use_distortion:
        image, mask = distort_pair(image, mask)
    else:
        image, mask = normalize_pair(image, mask)
    return _generate_image_label_batch(image, mask, batch_size, shuffle=cfg.shuffle)

# Credit lipnet
def online_pair_selector(sess, X1, X2, y, distance_fn, is_training ):
    distance = sess.run(distance_fn(X1, X2, y, is_training = False))
    label_keep = []
    hard_margin = 10

    # Max-Min distance in genuines
    max_gen = 0
    min_gen = 100
    for j in range(y.shape[0]):
        if y[j] == 1:
            if max_gen < distance[j, 0]:
                max_gen = distance[j, 0]
            if min_gen > distance[j, 0]:
                min_gen = distance[j, 0]

    # Min-Max distance in impostors
    min_imp = 100
    max_imp = 0
    for k in range(y.shape[0]):
        if y[k] == 0:
            if min_imp > distance[k, 0]:
                min_imp = distance[k, 0]
            if max_imp < distance[k, 0]:
                max_imp = distance[k, 0]

    ### Keeping hard impostors and genuines
    for i in range(y.shape[0]):
        # imposter
        if y[i] == 0:
            if distance[i, 0] < max_gen + hard_margin:
                label_keep.append(i)
        elif y[i] == 1:
            # if distance[i, 0] > min_imp - hard_margin:
            label_keep.append(i)

    #### Choosing the pairs ######
    X1 = X1[label_keep]
    X2 = X2[label_keep]
    y = y[label_keep]
    return X1, X2, y

def _generate_image_label_batch(image, mask, batch_size, shuffle=False):
    """Construct a queued batch of images.
    Args:
        image (tensor): 3-D Tensor of [height, width, num_channels]
        mask (tensor): 3-D Tensor of [height, width, 1]
        batch_size (int): Number of images per batch.
    Returns:
        images (tensor): Images tensor of [batch_size, height, width, num_channels] size.
        masks (tensor): Masks tensor of [batch_size, height, width, 1] size. (eventually 1 will be replaced with num_classes)
    """

    num_preprocess_threads = cfg.num_preprocess_threads
    if shuffle:
        # Create a queue that shuffles the examples, and then
        # read 'batch_size' images + labels from the example queue.
        images, masks = tf.train.shuffle_batch(
            [image, mask],
            batch_size=batch_size,
            num_threads=num_preprocess_threads,
            capacity=cfg.min_queue_examples + 3 * batch_size,
            min_after_dequeue=cfg.min_queue_examples,
            allow_smaller_final_batch=True,
        )
    else:
        images, masks = tf.train.batch(
            [image, mask],
            batch_size=batch_size,
            num_threads=num_preprocess_threads,
            capacity=cfg.min_queue_examples + 3 * batch_size,
            allow_smaller_final_batch=True,
            dynamic_pad = True,
        )

    # display in tf summary page
    tf.summary.image('images', images)
    tf.summary.image('masks', masks)

    # NCHW for faster cuda processing
    if cfg.data_format is 'NCHW':
        images = tf.transpose(images, [0, 3, 1, 2])
        # masks = tf.transpose(masks, [0, 3, 1, 2])
    return images, masks

def _generate_imageset_batch(image, mask, set_id, batch_size, shuffle=False):
    """Construct a queued batch of images.
    Args:
        image (tensor): 3-D Tensor of [height, width, num_channels]
        mask (tensor): 3-D Tensor of [height, width, 1]
        batch_size (int): Number of images per batch.
    Returns:
        images (tensor): Images tensor of [batch_size, height, width, num_channels] size.
        masks (tensor): Masks tensor of [batch_size, height, width, 1] size. (eventually 1 will be replaced with num_classes)
    """
    num_preprocess_threads = cfg.num_preprocess_threads
    if shuffle:
        # Create a queue that shuffles the examples, and then
        # read 'batch_size' images + labels from the example queue.
        images, masks, set_ids = tf.train.shuffle_batch(
            [image, mask, set_id],
            batch_size=batch_size,
            num_threads=num_preprocess_threads,
            capacity=cfg.min_queue_examples + batch_size,
            min_after_dequeue=cfg.min_queue_examples,
            dynamic_pad = True,
            allow_smaller_final_batch=True)
    else:
        images, masks, set_ids = tf.train.batch(
            [image, mask, set_id],
            batch_size=batch_size,
            num_threads=num_preprocess_threads,
            capacity=cfg.min_queue_examples + batch_size,
            allow_smaller_final_batch=True,
            dynamic_pad = True,
            )

    # display in tf summary page
    tf.summary.image('images', images)
    tf.summary.image('masks', masks)

    # NCHW for faster cuda processing
    if cfg.data_format is 'NCHW':
        images = tf.transpose(images, [0, 3, 1, 2])
        # masks = tf.transpose(masks, [0, 3, 1, 2])
    return images, masks, set_ids

# -------------------------------------------------------------------------
# Image Processors (WIP)
# -------------------------------------------------------------------------
def read_imageset(imageset, minval=0.0525, minval2=0.40):
    """ Function for normalizing an image set for processing.
    Args:
        imageset (list): List containing [image, mask, set_id]:
            image (np.ndarray): Image as a [height, width, channel] numpy array.
            label (np.ndarray): Mask as a [height, width] or [height, width, labels] numpy array.
            set_id (str): Number for pair// original image ID
    Returns:
        image (tensor):
        mask (tensor):
        set_id (tensor):
    """
    image, label, set_id = load_image_set(imageset, write_tfrecords=False)

    if cfg.debug: print('\nReading image set (%s)...'%set_id)
    if cfg.debug: print('image_raw:%s\t%s\nmask_raw:%s\t%s'%(type(image), image.shape, type(mask), label.shape))

    # Apply same preprocessing as create_tfrecords method
    shape = image.shape
    mask = generate_mask(image,smooth=True, ksize=13, gray=False, threshold='otsu', minval=minval, minval2=minval2)
    image = preprocess_image(image)
    label = preprocess_mask(label, smooth=True)

    if cfg.debug: print('image_preprocessed:%s\t%s\nmask_preprocessed:%s\t%s'%(type(image), image.shape,type(mask), mask.shape))

    image = resize(image, cfg.norm_resize, cfg.norm_resize, smooth=False, ksize=3, threshold=None)
    label = resize(label, cfg.norm_resize, cfg.norm_resize, smooth=False, ksize=3, threshold='otsu')

    # Add channel axis if it's missing
    if image.ndim is 2:
        image = np.expand_dims(image, axis=2)
    if label.ndim is 2:
        label = np.expand_dims(label, axis=2)
    if mask.ndim is 2:
        mask = np.expand_dims(mask, axis=2)
    if cfg.debug: print('image_expdims: %s\t%s\nlabel_expdims:%s\t%s'%(type(image), image.shape,type(label), label.shape))

    if cfg.lab_cs:
#         image = image / 255.0  # shift to [0,1] for conversion to Lab
        image = rgb_to_lab(image) # Unconfirmed, but lab should be [-1,1]
#     else:
#         image = ((image - 127.5) / 127.5) # shift to [-1,1]
    label = label / 255.0
    if cfg.debug:  print('image_floated:%s\t%s\t(min:%g,max:%g)\nmask_floated:%s\t%s\t(min:%g,max:%g)'%(type(image), image.shape,np.max(image), np.min(image), type(label), label.shape, np.max(label), np.min(label)))

    # Add batch axis if it's missing
    if image.ndim is 3:
        image = np.expand_dims(image, axis=0)
    if label.ndim is 3:
        label = np.expand_dims(label, axis=0)
    if mask.ndim is 3:
        mask = np.expand_dims(mask, axis=0)
    if cfg.debug: print('image_expdims: %s\t%s\nmask_expdims:%s\t%s'%(type(image), image.shape,type(label), label.shape))

    # NCHW for faster cuda processing
    if cfg.data_format is 'NCHW':
        if cfg.debug: print('\nTransposing image set (%s) to NCHW...'%set_id)
        image = np.transpose(image, (0, 3, 1, 2))
        # label = tf.transpose(label, [0, 3, 1, 2])
        if cfg.debug: print('image_transposed: %s\t%s\nmask_transposed:%s\t%s'%(type(image), image.shape,type(mask), mask.shape))
    return image, label, mask/255.0, shape[0], shape[1], shape[2], set_id

def process_imageset(imageset, ksize=7, minval=0.0525, minval2=0.40):
    """ Function for normalizing an image set for processing as tensors.
    Args:
        imageset (list): List containing [image, mask, set_id]:
            image (np.ndarray): Image as a [height, width, channel] numpy array.
            mask (np.ndarray): Mask as a [height, width] or [height, width, labels] numpy array.
            set_id (str): Number for pair// original image ID
    Returns:
        image (tensor):
        mask (tensor):
        set_id (tensor):
    """
    image, label, set_id = load_image_set(imageset, write_tfrecords=False)

    if cfg.debug: print('\nReading image set (%s)...'%set_id)
    if cfg.debug: print('image_raw:%s\t%s\nlabel_raw:%s\t%s'%(type(image), image.shape, type(label), label.shape))

    # Apply same preprocessing as create_tfrecords method
    shape = image.shape
    mask = generate_mask(image,smooth=True, ksize=ksize, gray=False, threshold='otsu', minval=minval, minval2=minval2)
    image = preprocess_image(image)
    label = preprocess_mask(label, smooth=True)
    if cfg.debug: print('image_preprocessed:%s\t%s\nlabel_preprocessed:%s\t%s'%(type(image), image.shape,type(label), label.shape))

    # Add channel axis if it's missing
    if image.ndim is 2:
        image = np.expand_dims(image, axis=2)
    if label.ndim is 2:
        label = np.expand_dims(label, axis=2)
    if mask.ndim is 2:
        mask = np.expand_dims(mask, axis=2)
    if mask.ndim is 3:
        mask = np.expand_dims(mask, axis=0)
    if cfg.debug: print('image_expdims: %s\t%s\nlabel_expdims:%s\t%s'%(type(image), image.shape,type(label), label.shape))

    label = tf.to_float(label)
    height = tf.cast(shape[0], tf.int32)
    width = tf.cast(shape[1], tf.int32)
    depth = tf.cast(shape[2], tf.int32)
    if cfg.use_fp16:
        dtype=tf.float16
    else:
        dtype=tf.float32
    image = tf.cast(image, dtype)
    label = tf.cast(label, dtype)
    if cfg.lab_cs:
#         image = image / 255.0  # shift to [0,1] for conversion to Lab
        image = rgb_to_lab(image) # Unconfirmed, but lab should be [-1,1]
#     else:
#         image = ((image - 127.5) / 127.5) # shift to [-1,1]
    label = label / 255.0
    if cfg.debug:  print('image_floated:%s\t%s\t(min:%g,max:%g)\nlabel_floated:%s\t%s\t(min:%g,max:%g)'%(type(image), image.shape,np.max(image.eval()), np.min(image.eval()), type(label), label.shape, np.max(label.eval()), np.min(label.eval())))

    if cfg.use_distortion:
        if cfg.debug: print('\nDistorting image set (%s)...'%set_id)
        image, label = distort_pair(image, label)
    else:
        if cfg.debug: print('\nNormalizing image set (%s)...'%set_id)
#         image = tf.image.adjust_saturation(image, 0.9)
#         image = tf.image.adjust_contrast(image, 0.8)
#         image = tf.image.adjust_brightness(image, 0.8)
#         image = tf.image.adjust_gamma(image, gamma=2, gain=1)
        image, label = normalize_pair(image, label)

    # display in tf summary page
    tf.summary.image('images', image)
    tf.summary.image('labels', label)

    # Add batch axis if it's missing
    if tf.assert_rank(image, 3):
        image = tf.expand_dims(image, axis=0)
    if tf.assert_rank(label, 3):
        label = tf.expand_dims(label, axis=0)
    if cfg.debug: print('image_expdims: %s\t%s\nlabel_expdims:%s\t%s'%(type(image), image.get_shape(),type(label), label.get_shape()))

    # NCHW for faster cuda processing
    if cfg.data_format is 'NCHW':
        if cfg.debug: print('\nTransposing image set (%s) to NCHW...'%set_id)
        image = tf.transpose(image, [0, 3, 1, 2])
        # label = tf.transpose(label, [0, 3, 1, 2])
        if cfg.debug: print('image_transposed: %s\t%s\nlabel_transposed:%s\t%s'%(type(image), image.get_shape(),type(label), label.get_shape()))
    return image, label, mask, height, width, depth, set_id
