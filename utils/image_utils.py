# Copyright 2016-2017 Henry Leopold. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Image processing utility functions """
from __future__ import print_function, division, absolute_import
__author__ = 'Henry Leopold'

from glob import glob
from PIL import Image as im
import os, cv2, numpy as np
import tensorflow as tf
from sklearn.model_selection import GroupKFold
from config import cfg
from utils import build_path, write2csv, load_image_filenames, make_dir
from tqdm import trange
import matplotlib.pyplot as plt



__all__ = [
         # ================
         # Image Processing
         # ----------------
         # Normalization
            'histogram_equalization',
            'clahe',
            'mean_normalization',
            'gamma_correction',
            'apply_threshold',
            'binary_thresh',
            'normalize_pair',
         # Augmentation
            'gaussian_blur',
            'preprocess_mask',
            'preprocess_image',
            'generate_mask',
            # 'crop_and_resize', # TODO ~ Work in progress
            'resize',
            'resize_image_tensor',
            'distort_pair',
            'paint_image',
        # from DLTK TODO
            'resize_image_with_crop_or_pad',
            'flip',
            'add_gaussian_offset',
            'add_gaussian_noise',
            'elastic_transform',
            'extract_class_balanced_example_array',
            'extract_random_example_array',
            'crop_image3d',
            'rescale_intensity',
            'data_augmenter',

        # ==================
        # Image Colourspace
        # ------------------
            # 'preprocess','deprocess','preprocess_lab','deprocess_lab','augment','check_image', # Called internally.
            'shift_range',
            'rgb_to_lab',
            'lab_to_rgb',
        # ================
        # Tensor Utilities
        # ----------------
            'randint',
            'preprocess_test',
            'gabor',
            'convert2int',
            'convert2float',
            'batch_convert2int',
            'batch_convert2float',
        # ================
        # Display Utilities
        # ----------------
            'cycleimages',
            'check_images', # WIP
            'show_slice',
        # ==================
        # Read/Write
        # ------------------
        # Read
            'imread',
            'read_image_and_resize',
        # Write
            'imsave',
            'save_image',
            '_gif2png',
            'save2channel',

        ]

# ==============================================================================
# Image Processing
# ==============================================================================
# Normalization
# -------------------------------------------------------------------------
def histogram_equalization(image):
    """
    Args:
    Returns:
    """
    adj_image = np.zeros(image.shape, dtype=np.uint8)
    for i in xrange(image.shape[-1]):
        adj_image[...,i] = cv2.equalizeHist(image[...,i])
    return adj_image

def clahe(image):
    """Contrast Limited Adaptive Histogram Equalization.
    Args/Returns:
        image (array): Image in a numpy array (HWC).
    """
    clahe = cv2.createCLAHE(clipLimit=int(cfg.prep_clahe_clip), tileGridSize=(int(cfg.prep_clahe_ksize),int(cfg.prep_clahe_ksize)))
    adj_image = np.zeros(image.shape, dtype=np.uint8)
    for i in xrange(image.shape[-1]):
        adj_image[...,i] = clahe.apply(image[...,i])
    return adj_image

def gamma_correction(image, gamma=cfg.prep_gamma_val):
    """ Gamma correction in numpy.
    Args
        image (array): Image in a numpy array (HWC).
        gamma (float): Gamma value to apply to image.
    Returns:
        image (array): Gamma corrected image (HWC).
    """
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
    return cv2.LUT(image, table)

def mean_normalization(images, epsilon=1e-8):
    """ Mean image pixel value normalization. AKA: Whitening.
    Args/Returns:
        images (array): One or more images in an array (NHWC)
        epsilon (float): Divisor to ensure there isnt a division by zero.
    """
    images_normalized = np.empty(images.shape)
    images_normalized = (images-np.mean(images))/(np.std(image) + epsilon)
    if images.ndim is 3: images = np.expand_dims(images, axis=0)
    for i in range(images.shape[0]):
        images_normalized[i] = ((images_normalized[i] - np.min(images_normalized[i])) / (np.max(images_normalized[i])-np.min(images_normalized[i])))
    return images_normalized

def apply_threshold(image, threshold='otsu', **kwargs):
    """Applies a threshold to each channel individually.
    Args:
        image (array): Numpy array as HWC.
        threshold (str): Type of threshold to apply ['binary', 'otsu', None]
    Returns:
        adj_image (array): Numpy array as HWC.
    """
    if image.ndim is 2: image = np.expand_dims(image, axis=2)
    image=image.astype(np.uint8)
    adj_image = np.zeros(image.shape, dtype=np.uint8)
    for i in xrange(image.shape[-1]):
        if threshold is 'otsu':
            _,adj_image[...,i] = cv2.threshold(image[...,i], 0,image[...,i].max(), cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        elif threshold is 'binary':
            _, adj_image[...,i] = cv2.threshold(image[...,i],image[...,i].max()/2,image[...,i].max(),cv2.THRESH_BINARY)
        elif threshold is 'skew':
            _, adj_image[...,i] = cv2.threshold(image[...,i], 0, 1, cv2.THRESH_BINARY)
        else: adj_image[...,i] = image[...,i]
    return adj_image

def binary_thresh(image, minval):
    if image.ndim == 3:
        image = np.squeeze(image)
    mask = np.zeros(image.shape, dtype=np.float64)
    mask[np.greater_equal(image, minval)] = 1
    return mask

def normalize_pair(image, mask):
    """ Normalization pipeline for images/masks. Primarily uses cfg instead of args.
    Args/Returns:
      image (tensor): Image tensor
      mask (tensor): Mask tensor
    """
    if cfg.mean_norm:
        # Subtract off the mean and divide by the variance of the pixels.
        image = tf.image.per_image_standardization(image)
    if cfg.crop_resize or cfg.resize:
        image = resize_image_tensor(image, cfg.num_channels, cfg.norm_resize)
        mask = resize_image_tensor(mask, cfg.mask_channels, cfg.norm_resize)
        # image = tf.expand_dims(image, axis=0)
        # mask = tf.expand_dims(mask, axis=0)
    return image, mask

# -------------------------------------------------------------------------
# Augmentation
# -------------------------------------------------------------------------
def gaussian_blur(image, ksize):
    """ Wrapper for gaussian blur.
    Args:
        image (array): Numpy array as HWC.
        ksize (int or list): Kernel size of gaussian blur filter.
    Returns:
        image (generator): Generator for numpy array as HWC.
    """
    if type(ksize) is int:
        ksize = [ksize, ksize]
    return cv2.GaussianBlur(image, (ksize[0], ksize[1]),  0)

def preprocess_image(image):
    """ Wrapper for processing a numpy image.
    Args/Returns:
        image (array): Numpy array as HWC.
    """
    if cfg.prep_histo:
        image = histogram_equalization(image)
    if cfg.prep_clahe:
        image = clahe(image)
    if cfg.prep_gamma:
        image = gamma_correction(image)
    return image

def preprocess_mask(mask, smooth=False, ksize=3, threshold=None):
    """ Wrapper for processing a label mask.
    Args:
        mask (array): Numpy array as HWC.
        smooth (bool): If True, applies a gaussian blur when resizing.
        ksize (int): Kernel size of gaussian blur filter.
        threshold (str): Type of threshold to apply ['binary', 'otsu', None]
    Returns:
    """
    if smooth: mask = gaussian_blur(mask, ksize)
    mask = apply_threshold(mask, threshold)
    return mask

# TODO generate_mask cleanup
def generate_mask(image, smooth=True, gray=False, ksize=7, threshold='otsu', minval=0.0525, minval2=0.50, debug=False):
    """ Create a fundus mask/ROI by otsu thresholding.
    Args:
        image (array): Numpy array as HWC.
        smooth (bool): If True, applies a gaussian blur when resizing.
        gray (bool): Convert RGB to gray before otsu.
        ksize (int): Kernel size of gaussian blur filter.
        threshold (str): Type of threshold to apply ['binary', 'otsu', None]
        minval (float): Value to clip the mask by.
    Returns:
        mask (array): Numpy array as HWC.
    """
#     if smooth: image = gaussian_blur(image, ksize)
#     if gray: image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
#     mask = apply_threshold(image, threshold)
#     mask1 = resize(mask, cfg.norm_resize, cfg.norm_resize)
#     _kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(9,9))
#     mask = cv2.morphologyEx(np.squeeze(mask1), cv2.MORPH_CLOSE, _kernel)
#     m2 = np.zeros(mask.shape[:-1], dtype=np.float64)
#     for i in range(mask.shape[-1]):
#         m2 += mask[...,i]/mask.shape[-1]
#     m2 = gaussian_blur(m2, 7)
#     return mask1, mask, m2
    if smooth: image = gaussian_blur(image, ksize)
    if gray: image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    mask = np.zeros(image.shape[:-1], dtype=np.float64)
    m1 = np.zeros(image.shape[:-1], dtype=np.float64)
    for i in range(image.shape[-1]):
        mask += image[...,i]/image.shape[-1]
    mask = mask / np.max(mask)
    m1[np.greater_equal(mask, minval)] = 1
    m2 = cv2.resize(m1,
                           (cfg.norm_resize, cfg.norm_resize),
                           interpolation=cv2.INTER_AREA)
    m3 = gaussian_blur(m2, ksize)
    m4 = np.zeros(m3.shape, dtype=np.float64)
    m4[np.greater_equal(m3, minval2)] = 1
    if debug: return mask, m1, m2, m3,m4
    else: return m4

# def crop_and_resize(image, height=None, width=None):
#     """Crops the target image to the same proportion with training images, and
#     then resize to the same size.
#     """
#     if (height, width) == image.shape:
#         return image, image.shape

#     # Center crop to proportion
#     ratio = float(image.shape[0]) / image.shape[1]
#     if ratio != HEIGHT_WIDTH_RATIO:
#         if ratio > HEIGHT_WIDTH_RATIO:
#             w = image.shape[1]
#             h = int(w * HEIGHT_WIDTH_RATIO)
#         else:
#             h = image.shape[0]
#             w = int(h / HEIGHT_WIDTH_RATIO)

#         h0 = (image.shape[0] - h) // 2
#         h1 = h0 + h
#         w0 = (image.shape[1] - w) // 2
#         w1 = w0 + w
#         image = image[h0:h1, w0:w1]

#     crop_size = tuple(image.shape)

#     # resize to target dimension
#     interpolation = cv2.INTER_AREA \
#                         if image.size > width * height else \
#                         cv2.INTER_CUBIC
#     image = cv2.resize(image,
#                      (width, height),
#                       interpolation=interpolation)
#     return image, crop_size

def resize(image, height, width, smooth=False, ksize=3, threshold=None, **kwargs):
    """ Resize function in numpy, including option to apply a gaussian blur
    Args:
        image (array): Numpy array as HWC.
        height (int): Height to resize to in pixels.
        width (int): Width to resize to in pixels.
        smooth (bool): If True, applies a gaussian blur when resizing.
        ksize (int): Kernel size of gaussian blur filter.
        threshold (str): Type of threshold to apply ['binary', 'otsu', None]
    Returns:
        image (array): Resized image in HWC.
    """
    if (height, width) == image.shape[:2]: return image
    elif np.prod(image.shape[:2]) < height * width:
        image = cv2.resize(image,
                           (width, height),
                           interpolation=cv2.INTER_CUBIC)
        if smooth: image = gaussian_blur(image, ksize)
    else:
        image = cv2.resize(image, (width, height), interpolation=cv2.INTER_AREA)
        if smooth: image = gaussian_blur(image, ksize)
        image = apply_threshold(image, threshold, **kwargs)
    return image

def resize_image_tensor(tensor, channels, nsize):
    """ Resize function for tensorflow Tensors.
    Args:
        tensor (Tensor): Image tensor (HWC).
        channels (int): Number of channels to expect.
        nsize (int): New size for the image.
    Returns:
    """
    tensor = tf.image.resize_bicubic(tf.expand_dims(tensor, 0), [nsize, nsize])
    return tf.reshape(tensor, [nsize, nsize, channels])

def distort_pair(image, mask):
    """ Distortion pipeline for images/masks. Primarily uses cfg instead of args.
    Args/Returns:
      image (tensor): Image tensor
      mask (tensor): Mask tensor
    """
    combo = tf.concat([image, mask], -1)
    # Randomly flip the image
    combo = tf.image.random_flip_left_right(combo)
    combo = tf.image.random_flip_up_down(combo)
    combo = tf.image.rot90(combo, k=randint(0, 4))
    # Randomly crop a [height, width] section of the image.
    if cfg.crop_resize:
        combo = resize_image_tensor(combo, cfg.num_channels + cfg.mask_channels, cfg.norm_resize)
        combo = tf.random_crop(
            combo, [randint(cfg.min_crop, cfg.crop_size), randint(cfg.min_crop, cfg.crop_size), cfg.num_channels + cfg.mask_channels])
    elif cfg.crop:
        combo = tf.random_crop(
            combo, [cfg.crop_size, cfg.crop_size, cfg.num_channels + cfg.mask_channels])

    image = combo[..., :-cfg.mask_channels]
    mask = combo[..., -cfg.mask_channels:]
    image = tf.image.random_brightness(image, max_delta=63)
    image = tf.image.random_contrast(image, lower=0.2, upper=1.8)
    image, mask = normalize_pair(image, mask)
    return image, mask

def paint_image(image, num_classes=2):
    norm = mpl.colors.Normalize(vmin=0., vmax=num_classes)
    mycm = mpl.cm.get_cmap('Set1')
    return mycm(norm(image))

# ==============================================================================
# DLTK TODO MAKE OWN
# ==============================================================================
from scipy.ndimage.interpolation import map_coordinates, affine_transform
from scipy.ndimage.filters import gaussian_filter

def resize_image_with_crop_or_pad(image, img_size=(64, 64, 64), **kwargs):
    """Image resizing. Resizes image by cropping or padding dimension to fit specified size.
    Args:
        image (np.ndarray): image to be resized
        img_size (list or tuple): new image size
        kwargs (): additional arguments to be passed to np.pad

    Returns:
        np.ndarray: resized image
    """

    assert isinstance(image, (np.ndarray, np.generic))
    assert (image.ndim - 1 == len(img_size) or image.ndim == len(img_size)), \
        'Example size doesnt fit image size'

    # Get the image dimensionality
    rank = len(img_size)

    # Create placeholders for the new shape
    from_indices = [[0, image.shape[dim]] for dim in range(rank)]
    to_padding = [[0, 0] for dim in range(rank)]

    slicer = [slice(None)] * rank

    # For each dimensions find whether it is supposed to be cropped or padded
    for i in range(rank):
        if image.shape[i] < img_size[i]:
            to_padding[i][0] = (img_size[i] - image.shape[i]) // 2
            to_padding[i][1] = img_size[i] - image.shape[i] - to_padding[i][0]
        else:
            from_indices[i][0] = int(np.floor((image.shape[i] - img_size[i]) / 2.))
            from_indices[i][1] = from_indices[i][0] + img_size[i]

        # Create slicer object to crop or leave each dimension
        slicer[i] = slice(from_indices[i][0], from_indices[i][1])

    # Pad the cropped image to extend the missing dimension
    return np.pad(image[slicer], to_padding, **kwargs)

def flip(imagelist, axis=1):
    """Randomly flip spatial dimensions

    Args:
        imagelist (np.ndarray or list or tuple): image(s) to be flipped
        axis (int): axis along which to flip the images

    Returns:
        np.ndarray or list or tuple: same as imagelist but randomly flipped
            along axis
    """

    # Check if a single image or a list of images has been passed
    was_singular = False
    if isinstance(imagelist, np.ndarray):
        imagelist = [imagelist]
        was_singular = True

    # With a probility of 0.5 flip the image(s) across `axis`
    do_flip = np.random.random(1)
    if do_flip > 0.5:
        for i in range(len(imagelist)):
            imagelist[i] = np.flip(imagelist[i], axis=axis)
    if was_singular:
        return imagelist[0]
    return imagelist


def add_gaussian_offset(image, sigma=0.1):
    """
    Add Gaussian offset to an image. Adds the offset to each channel
    independently.

    Args:
        image (np.ndarray): image to add noise to
        sigma (float): stddev of the Gaussian distribution to generate noise
            from

    Returns:
        np.ndarray: same as image but with added offset to each channel
    """

    offsets = np.random.normal(0, sigma, ([1] * (image.ndim - 1) + [image.shape[-1]]))
    image += offsets
    return image


def add_gaussian_noise(image, sigma=0.05):
    """
    Add Gaussian noise to an image

    Args:
        image (np.ndarray): image to add noise to
        sigma (float): stddev of the Gaussian distribution to generate noise
            from

    Returns:
        np.ndarray: same as image but with added offset to each channel
    """

    image += np.random.normal(0, sigma, image.shape)
    return image


def elastic_transform(image, alpha, sigma):
    """
    Elastic deformation of images as described in [1].

    [1] Simard, Steinkraus and Platt, "Best Practices for Convolutional
        Neural Networks applied to Visual Document Analysis", in Proc. of the
        International Conference on Document Analysis and Recognition, 2003.

    Based on gist https://gist.github.com/erniejunior/601cdf56d2b424757de5

    Args:
        image (np.ndarray): image to be deformed
        alpha (list): scale of transformation for each dimension, where larger
            values have more deformation
        sigma (list): Gaussian window of deformation for each dimension, where
            smaller values have more localised deformation

    Returns:
        np.ndarray: deformed image
    """

    assert len(alpha) == len(sigma), \
        "Dimensions of alpha and sigma are different"

    channelbool = image.ndim - len(alpha)
    out = np.zeros((len(alpha) + channelbool, ) + image.shape)

    # Generate a Gaussian filter, leaving channel dimensions zeroes
    for jj in range(len(alpha)):
        array = (np.random.rand(*image.shape) * 2 - 1)
        out[jj] = gaussian_filter(array, sigma[jj],
                                  mode="constant", cval=0) * alpha[jj]

    # Map mask to indices
    shapes = list(map(lambda x: slice(0, x, None), image.shape))
    grid = np.broadcast_arrays(*np.ogrid[shapes])
    indices = list(map((lambda x: np.reshape(x, (-1, 1))), grid + np.array(out)))

    # Transform image based on masked indices
    transformed_image = map_coordinates(image, indices, order=0,
                                        mode='reflect').reshape(image.shape)

    return transformed_image


def extract_class_balanced_example_array(image,
                                         label,
                                         example_size=[1, 64, 64],
                                         n_examples=1,
                                         classes=2,
                                         class_weights=None):
    """Extract training examples from an image (and corresponding label) subject
        to class balancing. Returns an image example array and the
        corresponding label array.

    Args:
        image (np.ndarray): image to extract class-balanced patches from
        label (np.ndarray): labels to use for balancing the classes
        example_size (list or tuple): shape of the patches to extract
        n_examples (int): number of patches to extract in total
        classes (int or list or tuple): number of classes or list of classes
            to extract

    Returns:
        np.ndarray, np.ndarray: class-balanced patches extracted from full
            images with the shape [batch, example_size..., image_channels]
    """
    assert image.shape[:-1] == label.shape, 'Image and label shape must match'
    assert image.ndim - 1 == len(example_size), \
        'Example size doesnt fit image size'
    assert all([i_s >= e_s for i_s, e_s in zip(image.shape, example_size)]), \
        'Image must be larger than example shape'
    rank = len(example_size)

    if isinstance(classes, int):
        classes = tuple(range(classes))
    n_classes = len(classes)

    assert n_examples >= n_classes, \
        'n_examples need to be greater than n_classes'

    if class_weights is None:
        n_ex_per_class = np.ones(n_classes).astype(int) * int(np.round(n_examples / n_classes))
    else:
        assert len(class_weights) == n_classes, \
            'Class_weights must match number of classes'
        class_weights = np.array(class_weights)
        n_ex_per_class = np.round((class_weights / class_weights.sum()) * n_examples).astype(int)

    # Compute an example radius to define the region to extract around a
    # center location
    ex_rad = np.array(list(zip(np.floor(np.array(example_size) / 2.0),
                               np.ceil(np.array(example_size) / 2.0))),
                      dtype=np.int)

    class_ex_images = []
    class_ex_lbls = []
    min_ratio = 1.
    for c_idx, c in enumerate(classes):
        # Get valid, random center locations belonging to that class
        idx = np.argwhere(label == c)

        ex_images = []
        ex_lbls = []

        if len(idx) == 0 or n_ex_per_class[c_idx] == 0:
            class_ex_images.append([])
            class_ex_lbls.append([])
            continue

        # Extract random locations
        r_idx_idx = np.random.choice(len(idx),
                                     size=min(n_ex_per_class[c_idx], len(idx)),
                                     replace=False).astype(int)
        r_idx = idx[r_idx_idx]

        # Shift the random to valid locations if necessary
        r_idx = np.array(
            [np.array([max(min(r[dim], image.shape[dim] - ex_rad[dim][1]),
                           ex_rad[dim][0]) for dim in range(rank)])
             for r in r_idx])

        for i in range(len(r_idx)):
            # Extract class-balanced examples from the original image
            slicer = [slice(r_idx[i][dim]
                            - ex_rad[dim][0], r_idx[i][dim]
                            + ex_rad[dim][1]) for dim in range(rank)]

            ex_image = image[slicer][np.newaxis, :]

            ex_lbl = label[slicer][np.newaxis, :]

            # Concatenate them and return the examples
            ex_images = np.concatenate((ex_images, ex_image), axis=0) \
                if (len(ex_images) != 0) else ex_image
            ex_lbls = np.concatenate((ex_lbls, ex_lbl), axis=0) \
                if (len(ex_lbls) != 0) else ex_lbl

        class_ex_images.append(ex_images)
        class_ex_lbls.append(ex_lbls)

        ratio = n_ex_per_class[c_idx] / len(ex_images)
        min_ratio = ratio if ratio < min_ratio else min_ratio

    indices = np.floor(n_ex_per_class * min_ratio).astype(int)

    ex_images = np.concatenate([cimage[:idxs] for cimage, idxs in zip(class_ex_images, indices)
                                if len(cimage) > 0], axis=0)
    ex_lbls = np.concatenate([clbl[:idxs] for clbl, idxs in zip(class_ex_lbls, indices)
                              if len(clbl) > 0], axis=0)

    return ex_images, ex_lbls


def extract_random_example_array(image_list,
                                 example_size=[1, 64, 64],
                                 n_examples=1):
    """Randomly extract training examples from image (and a corresponding label).
        Returns an image example array and the corresponding label array.

    Args:
        image_list (np.ndarray or list or tuple): image(s) to extract random
            patches from
        example_size (list or tuple): shape of the patches to extract
        n_examples (int): number of patches to extract in total

    Returns:
        np.ndarray, np.ndarray: class-balanced patches extracted from full
        images with the shape [batch, example_size..., image_channels]
    """

    assert n_examples > 0

    was_singular = False
    if isinstance(image_list, np.ndarray):
        image_list = [image_list]
        was_singular = True

    assert all([i_s >= e_s for i_s, e_s in zip(image_list[0].shape, example_size)]), \
        'Image must be bigger than example shape'
    assert (image_list[0].ndim - 1 == len(example_size)
            or image_list[0].ndim == len(example_size)), \
        'Example size doesnt fit image size'

    for i in image_list:
        if len(image_list) > 1:
            assert (i.ndim - 1 == image_list[0].ndim
                    or i.ndim == image_list[0].ndim
                    or i.ndim + 1 == image_list[0].ndim), \
                'Example size doesnt fit image size'

            assert all([i0_s == i_s for i0_s, i_s in zip(image_list[0].shape, i.shape)]), \
                'Image shapes must match'

    rank = len(example_size)

    # Extract random examples from image and label
    valid_loc_range = [image_list[0].shape[i] - example_size[i] for i in range(rank)]

    rnd_loc = [np.random.randint(valid_loc_range[dim], size=n_examples)
               if valid_loc_range[dim] > 0
               else np.zeros(n_examples, dtype=int) for dim in range(rank)]

    examples = [[]] * len(image_list)
    for i in range(n_examples):
        slicer = [slice(rnd_loc[dim][i], rnd_loc[dim][i] + example_size[dim])
                  for dim in range(rank)]

        for j in range(len(image_list)):
            ex_image = image_list[j][slicer][np.newaxis]
            # Concatenate and return the examples
            examples[j] = np.concatenate((examples[j], ex_image), axis=0) \
                if (len(examples[j]) != 0) else ex_image

    if was_singular:
        return examples[0]
    return examples

def crop_image3d(image, cx, cy, size):
    """ Crop a 3D image using a bounding box centred at (cx, cy) with specified size """
    X, Y = image.shape[:2]
    r = int(size / 2)
    x1, x2 = cx - r, cx + r
    y1, y2 = cy - r, cy + r
    x1_, x2_ = max(x1, 0), min(x2, X)
    y1_, y2_ = max(y1, 0), min(y2, Y)
    # Crop the image
    crop = image[x1_: x2_, y1_: y2_]
    # Pad the image if the specified size is larger than the input image size
    if crop.ndim == 3:
        crop = np.pad(crop,
                      ((x1_ - x1, x2 - x2_), (y1_ - y1, y2 - y2_), (0, 0)),
                      'constant')
    elif crop.ndim == 4:
        crop = np.pad(crop,
                      ((x1_ - x1, x2 - x2_), (y1_ - y1, y2 - y2_), (0, 0), (0, 0)),
                      'constant')
    else:
        print('Error: unsupported dimension, crop.ndim = {0}.'.format(crop.ndim))
        exit(0)
    return crop


def rescale_intensity(image, thres=(1.0, 99.0)):
    """ Rescale the image intensity to the range of [0, 1] """
    val_l, val_h = np.percentile(image, thres)
    image2 = image
    image2[image < val_l] = val_l
    image2[image > val_h] = val_h
    image2 = (image2.astype(np.float32) - val_l) / (val_h - val_l)
    return image2

def data_augmenter(image, label, shift, rotate, scale, intensity, flip):
    """
        Online data augmentation
        Perform affine transformation on image and label,
        which are 4D tensor of shape (N, H, W, C) and 3D tensor of shape (N, H, W).
    """
    image2 = np.zeros(image.shape, dtype=np.float32)
    label2 = np.zeros(label.shape, dtype=np.int32)
    for i in range(image.shape[0]):
        # For each image slice, generate random affine transformation parameters
        # using the Gaussian distribution
        shift_val = [np.clip(np.random.normal(), -3, 3) * shift,
                     np.clip(np.random.normal(), -3, 3) * shift]
        rotate_val = np.clip(np.random.normal(), -3, 3) * rotate
        scale_val = 1 + np.clip(np.random.normal(), -3, 3) * scale
        intensity_val = 1 + np.clip(np.random.normal(), -3, 3) * intensity

        # Apply the affine transformation (rotation + scale + shift) to the image
        row, col = image.shape[1:3]
        M = cv2.getRotationMatrix2D((row / 2, col / 2), rotate_val, 1.0 / scale_val)
        M[:, 2] += shift_val
        for c in range(image.shape[3]):
            image2[i, :, :, c] = affine_transform(image[i, :, :, c],
                                                                        M[:, :2], M[:, 2], order=1)

        # Apply the affine transformation (rotation + scale + shift) to the label map
        label2[i, :, :] = affine_transform(label[i, :, :],
                                                                 M[:, :2], M[:, 2], order=0)

        # Apply intensity variation
        image2[i] *= intensity_val

        # Apply random horizontal or vertical flipping
        if flip:
            if np.random.uniform() >= 0.5:
                image2[i] = image2[i, ::-1, :, :]
                label2[i] = label2[i, ::-1, :]
            else:
                image2[i] = image2[i, :, ::-1, :]
                label2[i] = label2[i, :, ::-1]
    return image2, label2

# ==============================================================================
# Image Colourspace
# ==============================================================================
# TODO convert into image processor class
# TODO test shift range
def shift_range(image, rmin=0, rmax=1, epsilon=1e-8):
    """ Shifts image pixel range (using numpy).
    Args:
        image (array): Numpy compatable array of image.
        min, max (int or float): Represent desired range (WIP).
        epsilon (float): Divisor to ensure there isnt a division by zero.
    Returns: Normalized image
    """
    # First shifts range to [0,1]
    image = image.astype(np.float32)
    image = (image - np.min(image))
    image /= (np.max(image) + epsilon)
    image *= (float(rmax)+(np.abs(float(rmin))))
    image += float(rmin)
    return image

# TODO verify usage and modify: preprocess, deprocess, preprocess_lab, deprocess_lab,
def preprocess(image):
    """
    Args:
    Returns:
    """
    with tf.name_scope("preprocess"):
        # [0, 1] => [-1, 1]
        return image * 2 - 1

def deprocess(image):
    """
    Args:
    Returns:
    """
    with tf.name_scope("deprocess"):
        # [-1, 1] => [0, 1]
        return (image + 1) / 2

# TODO verify usage and modify: preprocess_lab, deprocess_lab,
def preprocess_lab(lab):
    """
    Args:
    Returns:
    """
    with tf.name_scope("preprocess_lab"):
        L_chan, a_chan, b_chan = tf.unstack(lab, axis=2)
        # L_chan: black and white with input range [0, 100]
        # a_chan/b_chan: color channels with input range ~[-110, 110], not exact
        # [0, 100] => [-1, 1],  ~[-110, 110] => [-1, 1]
        return [L_chan / 50 - 1, a_chan / 110, b_chan / 110]

def deprocess_lab(L_chan, a_chan, b_chan):
    """
    Args:
    Returns:
    """
    with tf.name_scope("deprocess_lab"):
        # this is axis=3 instead of axis=2 because we process individual images but deprocess batches
        return tf.stack([(L_chan + 1) / 2 * 100, a_chan * 110, b_chan * 110], axis=3)

# TODO verify usage and modify: augment, check_image
def augment(image, brightness):
    """
    Args:
    Returns:
    """
    # (a, b) color channels, combine with L channel and convert to rgb
    a_chan, b_chan = tf.unstack(image, axis=3)
    L_chan = tf.squeeze(brightness, axis=3)
    lab = deprocess_lab(L_chan, a_chan, b_chan)
    rgb = lab_to_rgb(lab)
    return rgb

def check_image(image):
    """
    Args:
    Returns:
    """
    assertion = tf.assert_equal(tf.shape(image)[-1], 3, message="image must have 3 color channels")
    with tf.control_dependencies([assertion]):
        image = tf.identity(image)

    if image.get_shape().ndims not in (3, 4):
        raise ValueError("image must be either 3 or 4 dimensions")

    # make the last dimension 3 so that you can unstack the colors
    shape = list(image.get_shape())
    shape[-1] = 3
    image.set_shape(shape)
    return image

# TODO lab space
# Credit (pixelcnn++?)
def rgb_to_lab(srgb):
    """ Converts an sRGB color value to LAB.
    Based on http://www.brucelindbloom.com/index.html?Equations.html.
    Assumes r, g, and b are contained in the set [0, 1].
    LAB output is NOT restricted to [0, 1]!

    Args:
    Returns:
    """
    with tf.name_scope("rgb_to_lab"):
        srgb = check_image(srgb)
        srgb_pixels = tf.reshape(srgb, [-1, 3])

        with tf.name_scope("rgb_to_bytes"):
            linear_mask = tf.cast(srgb_pixels <= 0.04045, dtype=tf.float32)
            exponential_mask = tf.cast(srgb_pixels > 0.04045, dtype=tf.float32)
            rgb_pixels = (srgb_pixels / 12.92 * linear_mask) + (((srgb_pixels + 0.055) / 1.055) ** 2.4) * exponential_mask
            rgb_to_bytes = tf.constant([
                #    X        Y          Z
                [0.412453, 0.212671, 0.019334], # R
                [0.357580, 0.715160, 0.119193], # G
                [0.180423, 0.072169, 0.950227], # B
            ])
            bytes_pixels = tf.matmul(rgb_pixels, rgb_to_bytes)

        # https://en.wikipedia.org/wiki/Lab_color_space#CIELAB-CIEbytes_conversions
        with tf.name_scope("bytes_to_lab"):
            # convert to fx = f(X/Xn), fy = f(Y/Yn), fz = f(Z/Zn)

            # normalize for D65 white point
            bytes_normalized_pixels = tf.multiply(bytes_pixels, [1/0.950456, 1.0, 1/1.088754])

            epsilon = 6/29
            linear_mask = tf.cast(bytes_normalized_pixels <= (epsilon**3), dtype=tf.float32)
            exponential_mask = tf.cast(bytes_normalized_pixels > (epsilon**3), dtype=tf.float32)
            fxfyfz_pixels = (bytes_normalized_pixels / (3 * epsilon**2) + 4/29) * linear_mask + (bytes_normalized_pixels ** (1/3)) * exponential_mask

            # convert to lab
            fxfyfz_to_lab = tf.constant([
                #  l       a       b
                [  0.0,  500.0,    0.0], # fx
                [116.0, -500.0,  200.0], # fy
                [  0.0,    0.0, -200.0], # fz
            ])
            lab_pixels = tf.matmul(fxfyfz_pixels, fxfyfz_to_lab) + tf.constant([-16.0, 0.0, 0.0])

        return tf.reshape(lab_pixels, tf.shape(srgb))

def lab_to_rgb(lab):
    """ Converts an LAB color value to sRGB.
    Based on http://www.brucelindbloom.com/index.html?Equations.html.
    Args:
    Returns:
    returns r, g, and b in the set [0, 1].
    """
    with tf.name_scope("lab_to_rgb"):
        lab = check_image(lab)
        lab_pixels = tf.reshape(lab, [-1, 3])

        # https://en.wikipedia.org/wiki/Lab_color_space#CIELAB-CIEXYZ_conversions
        with tf.name_scope("lab_to_bytes"):
            # convert to fxfyfz
            lab_to_fxfyfz = tf.constant([
                #   fx      fy        fz
                [1/116.0, 1/116.0,  1/116.0], # l
                [1/500.0,     0.0,      0.0], # a
                [    0.0,     0.0, -1/200.0], # b
            ])
            fxfyfz_pixels = tf.matmul(lab_pixels + tf.constant([16.0, 0.0, 0.0]), lab_to_fxfyfz)

            # convert to bytes
            epsilon = 6/29
            linear_mask = tf.cast(fxfyfz_pixels <= epsilon, dtype=tf.float32)
            exponential_mask = tf.cast(fxfyfz_pixels > epsilon, dtype=tf.float32)
            bytes_pixels = (3 * epsilon**2 * (fxfyfz_pixels - 4/29)) * linear_mask + (fxfyfz_pixels ** 3) * exponential_mask

            # denormalize for D65 white point
            bytes_pixels = tf.multiply(bytes_pixels, [0.950456, 1.0, 1.088754])

        with tf.name_scope("bytes_to_rgb"):
            bytes_to_rgb = tf.constant([
                #     r           g          b
                [ 3.2404542, -0.9692660,  0.0556434], # x
                [-1.5371385,  1.8760108, -0.2040259], # y
                [-0.4985314,  0.0415560,  1.0572252], # z
            ])
            rgb_pixels = tf.matmul(bytes_pixels, bytes_to_rgb)
            # avoid a slightly negative number messing up the conversion
            rgb_pixels = tf.clip_by_value(rgb_pixels, 0.0, 1.0)
            linear_mask = tf.cast(rgb_pixels <= 0.0031308, dtype=tf.float32)
            exponential_mask = tf.cast(rgb_pixels > 0.0031308, dtype=tf.float32)
            srgb_pixels = (rgb_pixels * 12.92 * linear_mask) + ((rgb_pixels ** (1/2.4) * 1.055) - 0.055) * exponential_mask

        return tf.reshape(srgb_pixels, tf.shape(lab))

# ==============================================================================
# Tensor Utilities
# ==============================================================================
def randint (minval, maxval, seed=None, name=None):
    """Wrapper for generating single random integers at runtime.
    Args:
    Returns:
    """
    return tf.reshape(tf.random_uniform([1],minval=minval,maxval=maxval,dtype=tf.int32,seed=seed,name=name), [])

def preprocess_test(image):
    """
    Args:
    Returns:
    """
    # if cfg.mean_norm:
    image = tf.squeeze(image, axis=0)
    image = tf.image.per_image_standardization(image)
    return tf.expand_dims(image, axis=0)

def gabor(n_values=32, sigma=1.0, mean=0.0):
    """
    Args:
    Returns:
    """
    x = tf.linspace(-3.0, 3.0, n_values)
    z = (tf.exp(tf.negative(tf.pow(x - mean, 2.0) /
                       (2.0 * tf.pow(sigma, 2.0)))) *
         (1.0 / (sigma * tf.sqrt(2.0 * 3.1415))))
    gauss_kernel = tf.matmul(
        tf.reshape(z, [n_values, 1]), tf.reshape(z, [1, n_values]))
    x = tf.reshape(tf.sin(tf.linspace(-3.0, 3.0, n_values)), [n_values, 1])
    y = tf.reshape(tf.ones_like(x), [1, n_values])
    gabor_kernel = tf.multiply(tf.matmul(x, y), gauss_kernel)
    return gabor_kernel

# ==================================================================================
# Display methods
# ==================================================================================
# TODO modify for matplotlib or scipy instead of cv2
def cycleimages(images, number = None, chan = False, fit = False, name='Image', wait = 500):
    print(images.shape)
    h = 500
    w = 500
    if number is None:
        number = images.shape[0]
    if fit:
        h = images.shape[1]
        w = images.shape[2]
    for i in xrange(number):
        if chan and images[i].ndim >= 3:
            for j in xrange(images.shape[3]):
                cv2.namedWindow("%s_channel_%i"%(name,j), cv2.WINDOW_NORMAL)
                cv2.resizeWindow("%s_channel_%i"%(name,j), h,w)
                cv2.imshow("%s_channel_%i"%(name,j), images[i,...,j])
                cv2.waitKey(wait)
        else:
            cv2.namedWindow(name, cv2.WINDOW_NORMAL)
            cv2.resizeWindow(name, h,w)
            cv2.imshow(name, images[i])
            cv2.waitKey(wait)
# # display image
#         cv2.imshow('image', np.uint8(image[0,:,:,0]*255.0))
#         cv2.waitKey(0)
#         cv2.imshow('mask', generated_mask[:,:,0])
#         cv2.waitKey(0)
#         if cv2.waitKey(1) & 0xFF == ord('q'):
#           break
# WIP
def check_images(path, out_dir, image_path = 'images', label_path = '1st_manual', mask_path = 'mask', name = 'training', use = 'train', batch_sampling_method = 1, borderSize = 0, omitSlices = [], omitLabels = [], show=True):
#     if not os.path.exists(out_dir):
#         os.makedirs(out_dir)
    image_filenames = utils._load_filenames(path + image_path)
    for index in xrange(len(image_filenames)):
        image = np.array(im.open(image_filenames[index]))
        if show:
            fig = plt.figure(figsize=(30,10))
            ax = fig.add_subplot(1, 3, 1)
            ax.imshow(image, interpolation='nearest')
            ax.set_title('Axis [1,0]')
            print('image_shape:', image.shape)

        label_filenames = utils._load_filenames(path + label_path)
        label = np.array(im.open(label_filenames[index]))
        ret,label = cv2.threshold(label,127,255,cv2.THRESH_BINARY)
        label = label / 255
        if show:
            ax = fig.add_subplot(1, 3, 2)
            ax.imshow(label, interpolation='nearest')

            print('label_shape:', label.shape)

        if mask_path is not None:
            mask_filenames = utils._load_filenames(path + mask_path)
            mask = np.array(im.open(mask_filenames[index]))
            ret,mask = cv2.threshold(mask,127,255,cv2.THRESH_BINARY)
            mask = mask / 255
        else:
            mask = np.ones(label.shape, dtype=np.uint8)
        if show:
            ax = fig.add_subplot(1, 3, 3)
            ax.imshow(mask, interpolation='nearest')
            print('mask:', mask.shape)

def show_slice(X, Y=None, cm2=plt.cm.hsv):
    plt.figure(figsize=(15,15))
    plt.imshow(X, cmap=plt.cm.gray, interpolation='none')
    if Y is not None:
        plt.hold(True)
        plt.imshow(Y, cmap=cm2, interpolation='none')
    plt.gca().axes.get_xaxis().set_ticks([])
    plt.gca().axes.get_yaxis().set_ticks([])

# ==================================================================================
# Read/Write
# ==================================================================================
def imread(path):
    image = scipy.misc.imread(path).astype(np.float)
    if len(image.shape) == 2:
        # grayscale
        image = np.dstack((image,image,image))
    elif image.shape[2] == 4:
        # PNG with alpha channel
        image = image[:,:,:3]
    return image

def imsave(path, image, quality=100):
    image = np.clip(image, 0, 255).astype(np.uint8)
    im.fromarray(image).save(path, quality=quality)

def save_image(image, name, save_dir, v=False):
    """ A wrapper for saving files.
    Args:  image, name, v: verbose (T/F) ~ TODO might be inbuilt; check.
    """
    scp.misc.imsave(os.path.join(save_dir , name), image)
    if v: print('Image saved to: %s' % save_dir)

def _gif2png(path, outpath, label=False):
    files = glob.glob(path)

    for imageFile in files:
        filepath,filename = os.path.split(imageFile)
        filterame,exts = os.path.splitext(filename)
        print("Processing: ", imageFile,filterame)
        im = im.open(imageFile)
        if label is True:
            gray = im.convert('L')
            bw = gray.point(lambda x: 0 if x<128 else 255, '1')
            bw.save(outpath+filterame+'.png','PNG')
        else:
            im.save( outpath+filterame+'.png','PNG')

def save2channel(path, outpath = None, saveto = '.png', quality=100):
    """
    Opens an image file and saves each of its channels as a seperate image.
    Args
        path: Path to image file
        outpath: Path to save channels. If it isn't provided, files saved in source directory.
        saveto: Image file type to save to. Defaults to png.
        quality: Quality to save images to. Default for Scipy (and PIL) is 95, so this is an override to 100.
    Returns
        None.
    """
    filepath,filename = os.path.split(path)
    filterame,exts = os.path.splitext(filename)
    if outpath is None:
        outpath = filepath
    if not os.path.exists(outpath):
        os.makedirs(outpath)
    print("Processing: %s [%s]"%(filterame, filepath))
    image = imread(path) #imread forces files without channels into 3 as well as png files with 4 channels to 3.
    # Need to double check the order of channels in different file formats. May need to add some int
    for e,n in enumerate(['r','g','b']):
        p = build_path([outpath,filterame+'_'+n+saveto])
        imsave(p, image[...,e], quality)
        print("Saved: %s [%s]"%(filterame+'_'+n+saveto, outpath))

def read_image_and_resize(image_path, new_dims=(512, 512), save_dir='resize', saveto = None, quality=100):
    """Reads an image, resizes it, saves it and returns the path
    Args:
        image_path (str): /path/to/image.jpg
        new_dims (tuple): Target width & height to resize
        save_dir (str): Directory name to save a resized image
        saveto (str): Image extenstion / type to save to. Defaults to png.
        quality (int): Quality to save images to. Default for Scipy (and PIL) is 95, so this is an override to 100.
    Returns:
        image_path (str): same as input `image_path`
    """
    filepath,filename = os.path.split(image_path)
    filterame,exts = os.path.splitext(filename)
    if save_dir is None:
        save_dir = filepath
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    if saveto is None:
        saveto = '.'+str(exts)
    new_path = build_path([save_dir,filterame+saveto])
    image = cv2.imread(image_path)
    image = cv2.resize(image, new_dims, interpolation=cv2.INTER_AREA)
    cv2.imwrite(new_path, image)
    return image_path


# ==============================================================================
#
# ==============================================================================
# TODO reorganize utils
# Credit cyclegan...
def convert2int(image):
  """ Transfrom from float tensor ([-1.,1.]) to int image ([0,255])
  """
  return tf.image.convert_image_dtype((image+1.0)/2.0, tf.uint8)

def convert2float(image):
  """ Transfrom from int image ([0,255]) to float tensor ([-1.,1.])
  """
  image = tf.image.convert_image_dtype(image, dtype=tf.float32)
  return (image/127.5) - 1.0

def batch_convert2int(images):
  """
  Args:
    images: 4D float tensor (batch_size, image_size, image_size, depth)
  Returns:
    4D int tensor
  """
  return tf.map_fn(convert2int, images, dtype=tf.uint8)

def batch_convert2float(images):
  """
  Args:
    images: 4D int tensor (batch_size, image_size, image_size, depth)
  Returns:
    4D float tensor
  """
  return tf.map_fn(convert2float, images, dtype=tf.float32)
