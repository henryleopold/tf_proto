# Copyright 2016-2017 Henry Leopold. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Initializer helper functions """
from __future__ import print_function, division, absolute_import
__author__ = 'Henry Leopold'
from six.moves import xrange  # pylint: disable=redefined-builtin

import os
import sys
import numpy as np
import tensorflow as tf
from config import cfg

__all__ = [
                'set_initializer',
                'msra_initializer',
                'orthogonal_initializer',
            # ------------------
            # Variables Creation
            # ------------------
                '_variable',
                '_variable_with_weight_decay',
                'build_layer_name',
                'int_shape',
                'var2list',
                'var_prep',
                'coerce_shape',
                'insert2list'
        ]


def set_initializer(name='xavier', x=0.01, m=0):
    """ Retrieves the proper initialization function from a string.
    Args:
        name (str): Name of the activation
        x (float):
        m (int):

    Returns: Tensorflow activation function
    """
    try:
        if name == 'xavier':
            return tf.contrib.layers.xavier_initializer_conv2d()
        elif name == 'xavier_dense':
            return tf.contrib.layers.xavier_initializer()
        elif name == 'msra':
            return msra_initializer(float(x), float(m))
        elif name == 'ortho':
            return orthogonal_initializer(float(x))
        elif name == 'rnormal':
            return tf.random_normal_initializer(stddev=float(x))
        elif name == 'tnormal':
            # TODO fix so stddev = 1 / math.sqrt(np.prod(input_shape))
            return tf.truncated_normal_initializer(stddev=float(x), mean=float(m))
        elif name == 'zeros':
            return tf.zeros_initializer()
        elif name == 'constant':
            return tf.constant_initializer(float(x))
        elif name is None:
            return None
        else: raise Exception
    except Exception as e:
        print('Initializer "%s" is not supported: %s' % (name, e))


def msra_initializer(kl, dl):  # TODO description
    stddev = math.sqrt(2. / (kl**2 * dl))
    return tf.truncated_normal_initializer(stddev=stddev)


def orthogonal_initializer(scale=1.1):
    """ From Lasagne and Keras. Reference: Saxe et al., http://arxiv.org/abs/1312.6120
    """
    def _initializer(shape, dtype=tf.float32, partition_info=None):
        flat_shape = (shape[0], np.prod(shape[1:]))
        a = np.random.normal(0.0, 1.0, flat_shape)
        u, _, v = np.linalg.svd(a, full_matrices=False)
        # pick the one with the correct shape
        q = u if u.shape == flat_shape else v
        q = q.reshape(shape)  # this needs to be corrected to float32
        return tf.constant(scale * q[:shape[0], :shape[1]], dtype=tf.float32)
    return _initializer

# ==============================================================================
# Variable creation functions
# ==============================================================================
# ------------------------------------------------------------------------------
def _variable(name, shape, initializer, reuse=False):
    """Helper to create a Variable.
    Args:
        name: name of the variable
        shape: list of ints
        initializer: initializer for Variable
    Returns:
        Variable Tensor
    """
    with tf.variable_scope(tf.get_variable_scope(), reuse=reuse):
        if cfg.use_fp16:
            dtype = tf.float16
        else:
            dtype = tf.float32
        if cfg.var_on_cpu:
            with tf.device('/cpu:0'):
                var = tf.get_variable(
                    name, shape, dtype=dtype, initializer=initializer)
            return var
        var = tf.get_variable(name, shape, dtype=dtype,
                              initializer=initializer)
        return var


def _variable_with_weight_decay(name, shape, stddev, wd):
    """Helper to create an initialized Variable with weight decay.

    Note that the Variable is initialized with a truncated normal distribution.
    A weight decay is added only if one is specified.

    Args:
    name: name of the variable
    shape: list of ints
    stddev: standard deviation of a truncated Gaussian
    wd: add L2Loss weight decay multiplied by this float. If None, weight
        decay is not added for this Variable.

    Returns:
    Variable Tensor
    """
    var = _variable(name, shape,
                         tf.truncated_normal_initializer(stddev=stddev))
    if wd is not None:
        weight_decay = tf.multiply(tf.nn.l2_loss(var), wd, name='weight_loss')
        tf.add_to_collection('losses', weight_decay)
    return var


def build_layer_name(ids, joiner='_'):
    """Helper for creating layer names
    Args:
        ids (list): List of scopes/layer names
        joiner (str): string to link ids
    Returns:
        Layer name (str)
    """
    ln = ''
    for i in range(len(ids)):
        if len(ids[i]) > 0:
            ln = ln + str(ids[i])
            if i is not len(ids): ln = ln + joiner
    return ln


def int_shape(x):
    return list(map(int, x.get_shape()))


def var2list(variable, n=2, name=None, as_tuple=False):
    """Transforms a single integer or iterable of integers into an integer tuple. This version is based on tf.layers.utils.normalize_tuple

    Arguments:
        variable (int or tuple of ints): The variable to validate and convert.
        n (int): The size of the tuple to be returned.
        name (str): The name of the argument being validated, e.g. "strides" or
        "kernel_size". This is only used to format error messages.
        as_tuple (boolean): Return list or tuple.

    Returns:
        A tuple of n integers.

    Raises:
        ValueError: If something else than an int/long or iterable thereof was passed.
    """
    if any(isinstance(variable, x) for x in (int, float)):
        l = (variable,) * n
    else:
        try:
            variable_tuple = tuple(variable)
        except TypeError:
            raise ValueError('The %s argument must be a tuple of %s integers. Received: %s'%(name,str(n)))
        if len(variable_tuple) != n:
            raise ValueError('The %s argument must be a tuple of %s integers. Received: %s'%(name,str(n)))
        for single_variable in variable_tuple:
            try:
                int(single_variable)
            except ValueError:
                raise ValueError('The %s argument must be a tuple of %s integers. Received: %s including element %s of type %s'%(name,str(n),str(variable) ,str(single_variable)))
        l = variable_tuple
    if not as_tuple: l = list(l)
    # if cfg.debug: print('VAR2LIST %s: Input: %s (type: %s)\t Output: %s (type: %s)'%(name, variable, type(variable), l, type(l)))
    return l

def var_prep(inputs, strides=1, kernel_size=3, filters=None, data_format='NCHW', rate=None, coerce_dims=True):
    """ Wrapper for getting input shape, strides and kernel size based on data format.
    Args:
        inputs (tensor):
        strides (int or list): Int is converted to dimensional list.
        kernel_size (int or list): Int is converted to dimensional list.
        filters (int): Number of output filters. Defaults to elementwise product of kernel_size.
        data_format (str): NCHW or NHWC; or some variant.
        rate (int or list): Dilation rate
        coerce_dims (boolean): Compress input dimensions to data_format.
    """
    input_shape = inputs.get_shape().as_list()
    dims = len(data_format)

    if 'NC' in data_format:
        channel_axis = 1
    else:
        channel_axis = -1

    kernel_size = (var2list(kernel_size, dims-2, 'kernel_size', as_tuple=True)) if kernel_size is not None else None
    strides = insert2list([1,] + var2list(strides, dims-2, 'strides'), 1, channel_axis) if strides is not None else None
    rate = insert2list([1,] + var2list(rate, dims-2, 'dilation_rate'), 1, channel_axis)  if rate is not None else None

    if coerce_dims:
        input_shape = coerce_shape(input_shape,dims,channel_axis)

    if filters is None:
        filters = np.prod(kernel_size)

    kernel_shape = kernel_size + (input_shape[channel_axis], filters)

    if cfg.debug: print('input_shape: %s (type: %s)\nchannel_axis: %s, coerced: %s\nfilters: %s (type: %s)\nkernel_size: %s (type: %s)\nstrides: %s (type: %s)\ndilation rate: %s (type: %s)\nkernel_shape: %s (type: %s)\n'%(str(input_shape), type(input_shape), str(channel_axis), str(coerce_dims), str(filters),type(filters), str(kernel_size), type(kernel_size), str(strides), type(strides), str(rate), type(rate),str(kernel_shape), type(kernel_shape)))

    # return input_shape, strides, kernel_shape, rate
    return (x for x in [input_shape, strides, kernel_shape, rate] if x is not None)

def coerce_shape(input_shape,dims,channel_axis=1):
    """ Reshapes input_shape for dims
    """
    if len(input_shape) == dims:
        return input_shape
    if cfg.debug: print('\nCoercing input_shape: %s (type: %s)'%(str(input_shape), type(input_shape)))
    if len(input_shape) > dims:
        c = input_shape.pop(channel_axis)
        n = np.prod(input_shape[dims-1:])
        cc = c*n if n >= 1 else c
        c_shape = insert2list(input_shape[:dims-1], cc, channel_axis)
    else:
        # TODO coerce/flatten
        # if flatten:
        #     dim = input_shape[1] * input_shape[2] * input_shape[3]
        #     input_processed = tf.reshape(input, [-1, dim])
        pass

    if cfg.debug: print('to: %s (type: %s)'%(c_shape, type(c_shape)))
    # if cfg.debug: print('to: %s (type: %s)\nc:%s\nn:%s\ncc:%s\nc_shape:%s (type: %s)\n'%(input_shape, type(input_shape), str(c), str(n),str(cc),str(c_shape),type(c_shape)))
    return c_shape

def insert2list(inputs, variable, axis=-1):
    """ Helper to insert a variable into List
    """
    inputs.append(variable) if axis == -1 else inputs.insert(axis, variable)
    return inputs
