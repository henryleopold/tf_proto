# Copyright 2016-2017 Henry Leopold. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Helper functions for training and testing models """
from __future__ import print_function, division, absolute_import #, unicode_literals
__author__ = 'Henry Leopold'
from six.moves import xrange  # pylint: disable=redefined-builtin

import os, sys
import numpy as np
import tensorflow as tf

from config import cfg


__all__ = [
        # ==========================
        # Loss
        # --------------------------
            'loss',
        # --------------------------
            'softmax_loss',
            'image_loss',
            'weighted_loss',
            'hint_loss',
            'dense_loss',
            'huber_loss',
        # ==========================
        # Non-callable with helper
        # --------------------------
            'margin_loss',
            'reconstruction_error',
            'capsule_loss',
            'contrastive_loss',
            'crossentropy',
            'sparse_balanced_crossentropy',
            'dice_loss',
        # ==========================
        # Error and Distance
        # --------------------------
            'entropy',
            'mean_squared_error',
            'clipped_error',
            'weighted_dist',
            'euclidean_dist',
          ]
# ==============================================================================
# Core Functions
# ==============================================================================
# TODO make loss into a wrapper class
def loss(predictions, labels, name = 'softmax', add2collection=False, batch_size=None, head=None, delta=1.0):
    with tf.variable_scope('loss'):
        if name == 'softmax':
            _loss =  softmax_loss(predictions, labels, add2collection)
        elif name == 'image':
            _loss =  image_loss(predictions, labels, add2collection)
        elif name == 'weighted':
            _loss =  weighted_loss(predictions, labels, head, add2collection)
        elif name == 'hint':
            _loss =  hint_loss(predictions, labels) # technically this is the loss between two feature banks
        elif name == 'dense':
            _loss =  dense_loss(predictions, labels, batch_size)
        elif name == 'huber':
            _loss =  huber_loss(predictions, labels, delta=delta)
        # TODO Dice loss
        # elif name == 'dice':
        #     _loss =  dice_loss(?)
        elif name is None:
            _loss =  None
        else:
            raise('Loss type: "%s" is not recognized'%name)
    if add2collection: tf.add_to_collection('losses', _loss)
    tf.summary.scalar('%s_loss'%name, _loss)
    return _loss

# -------------------------------------------------------------------------
# Actual loss functions
# -------------------------------------------------------------------------
def softmax_loss(predictions, labels, add2collection=False):
    """Calculate the average cross entropy L2-Loss for all trainable variables.
    Add summary for "Loss" and "Loss/avg".
    Args:
        predictions: predictions from inference().
        labels: Labels from distorted_inputs or inputs(). 1-D tensor
                of shape [batch_size]

    Returns:
        Loss tensor of type float.
    """
    predictions = tf.reshape(predictions, (-1,cfg.num_classes))
    labels = tf.reshape(labels, [-1]) # TODO >> Make for multiclass / onehot

    cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_predictions(
            predictions=predictions, labels=labels, name='cross_entropy_per_example')
    cross_entropy_mean = tf.reduce_mean(cross_entropy, name='cross_entropy')
    if add2collection:
        tf.add_to_collection('losses', cross_entropy_mean)
        return tf.add_n(tf.get_collection('losses'), name='total_loss')
    return cross_entropy_mean

def image_loss(predictions, labels, add2collection=True):
    """Calculates per image loss based on overlap.
    """
    # loss = _configure_loss(predictions, labels, X2) # WIP
    if cfg.debug:
        print(predictions.get_shape())
        print(labels.get_shape())
    intersection = tf.reduce_sum(predictions * labels)
    loss = -(2. * intersection + 1.) / (tf.reduce_sum(labels) + tf.reduce_sum(predictions) + 1.)
    return loss

def weighted_loss(predictions, labels, head=None, add2collection=True):
    """ median-frequency re-weighting """
    predictions = tf.reshape(predictions, (-1, num_classes))
    epsilon = tf.constant(value=1e-10)
    predictions = predictions + epsilon

    # consturct one-hot label array
    label_flat = tf.reshape(labels, (-1, 1))

    # should be [batch ,num_classes]
    labels = tf.reshape(tf.one_hot(label_flat, depth=num_classes), (-1, num_classes))
    softmax = tf.nn.softmax(predictions)
    cross_entropy = -tf.reduce_sum(tf.multiply(labels * tf.log(softmax + epsilon), head), axis=[1])
    cross_entropy_mean = tf.reduce_mean(cross_entropy, name='cross_entropy')
    if add2collection: tf.add_to_collection('losses', cross_entropy_mean)
    loss = tf.add_n(tf.get_collection('losses'), name='total_loss')
    return loss

def huber_loss(predictions, labels, delta=1.0):
    """ Huber loss, from stanford tensorflow tutorials """
    residual = tf.abs(predictions - labels)
    def f1(): return 0.5 * tf.square(residual)
    def f2(): return delta * residual - 0.5 * tf.square(delta)
    return tf.cond(residual < delta, f1, f2)

# ==============================================================================
# Not presently callable through the 'loss' helper function
# ==============================================================================
def margin_loss(vector, labels, m_plus, m_minux, lambda_val, num_classes, batch_size=-1):
    """ Calculates the margin loss.

    Reference:
        [1] Dynamic Routing with capsules.
    """
    max_plus = tf.square(tf.maximum(0.,m_plus - vector))
    max_plus = tf.reshape(m_plus, shape=(batch_size, num_classes))
    max_minus = tf.reshape(m_minus, shape=(batch_size, num_classes))
    max_minus = tf.square(tf.maximum(0., vector - m_minus))

    T_c = labels
    L_c = T_c * m_plus + lambda_val * (1 - T_c) * m_minus

    return tf.reduce_mean(tf.reduce_sum(L_c, axis=1))

#TODO make these own
#TODO add descriptions

def reconstruction_error(inputs, recon, num_classes, batch_size=-1):
    return tf.reduce_mean(tf.square(recon - tf.reshape(inputs, shape=(batch_size, num_classes))))

def capsule_loss(inputs, labels, recon, vector, m_plus, m_minux, lambda_val, regularization_scale, batch_size):
    margin_loss = margin_loss(labels, vector, m_plus, m_minux, lambda_val, batch_size)
    recon_err = reconstruction_error(inputs, recon, batch_size)
    return margin_loss + regularization_scale * recon_err

def spread_loss(labels, logits, margin, regularizer=None):
    ''' Credit to naturomics CapsLayer
    Args:
        labels: [batch_size, num_label, 1].
        logits: [batch_size, num_label, 1].
        margin: Integer or 1-D Tensor.
        regularizer: use regularization.

    Returns:
        loss: Spread loss.
    '''
    # a_target: [batch_size, 1, 1]
    a_target = tf.matmul(labels, logits, transpose_a=True)
    dist = tf.maximum(0., margin - (a_target - logits))
    loss = tf.reduce_mean(tf.square(tf.matmul(1 - labels, dist, transpose_a=True)))
    if regularizer is not None:
        regularizer = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
        loss += tf.reduce_mean(regularizer)
    return loss

def contrastive_loss(labels, predictions, margin_gen=0, margin_imp=1, scope=None):
    """
    """
    with tf.name_scope(scope, "contrastive_loss", [labels, predictions]) as scope:
        labels = tf.cast(labels, predictions.dtype)
        term_1 = tf.multiply(labels, tf.square(tf.maximum((predictions - margin_gen), 0)))
        term_2 = tf.multiply(1 - labels, tf.square(tf.maximum((margin_imp - predictions), 0)))
        # Contrastive
        Contrastive_Loss = tf.add(term_1, term_2) / 2
        loss = tf.losses.compute_weighted_loss(Contrastive_Loss, scope=scope)
        return loss

#TODO make dltk own
def crossentropy(predictions, labels, logits=True):
    """ Calculates the crossentropy loss between predictions and labels using numpy.
    # Credit: Deep Learning Toolkit (DLTK) for Medical Imaging
    Args:
        prediction (np.ndarray): predictions
        labels (np.ndarray): labels
        logits (bool): flag whether predictions are logits or probabilities
    Returns:
        float: crossentropy error
    """
    if logits:
        maxes = np.amax(predictions, axis=-1, keepdims=True)
        softexp = np.exp(predictions - maxes)
        softm = softexp / np.sum(softexp, axis=-1, keepdims=True)
    else:
        softm = predictions
    loss = np.mean(-1. * np.sum(labels * np.log(softm + 1e-8), axis=-1))
    return loss.astype(np.float32)

def sparse_balanced_crossentropy(logits, labels):
    """
    Calculates a class frequency balanced crossentropy loss from sparse labels.
    # Credit: Deep Learning Toolkit (DLTK) for Medical Imaging
    Args:
        logits (tf.Tensor): logits prediction for which to calculate
            crossentropy error
        labels (tf.Tensor): sparse labels used for crossentropy error
            calculation
    Returns:
        tf.Tensor: Tensor scalar representing the mean loss
    """

    epsilon = tf.constant(np.finfo(np.float32).tiny)

    num_classes = tf.cast(tf.shape(logits)[-1], tf.int32)

    probs = tf.nn.softmax(logits)
    probs += tf.cast(tf.less(probs, epsilon), tf.float32) * epsilon
    log = -1. * tf.log(probs)

    onehot_labels = tf.one_hot(labels, num_classes)

    class_frequencies = tf.stop_gradient(tf.bincount(
        labels, minlength=num_classes, dtype=tf.float32))

    weights = (1. / (class_frequencies + tf.constant(1e-8)))
    weights *= (tf.cast(tf.reduce_prod(tf.shape(labels)), tf.float32)
                / tf.cast(num_classes, tf.float32))

    new_shape = (([1, ] * len(labels.get_shape().as_list()))
                 + [logits.get_shape().as_list()[-1]])

    weights = tf.reshape(weights, new_shape)

    loss = tf.reduce_mean(tf.reduce_sum(onehot_labels * log * weights, axis=-1))

    return loss

def dice_loss(logits,
              labels,
              num_classes,
              smooth=1e-5,
              include_background=True,
              only_present=False):
    """Calculates a smooth Dice coefficient loss from sparse labels.
    # Credit: Deep Learning Toolkit (DLTK) for Medical Imaging
    Args:
        logits (tf.Tensor): logits prediction for which to calculate
            crossentropy error
        labels (tf.Tensor): sparse labels used for crossentropy error
            calculation
        num_classes (int): number of class labels to evaluate on
        smooth (float): smoothing coefficient for the loss computation
        include_background (bool): flag to include a loss on the background
            label or not
        only_present (bool): flag to include only labels present in the
            inputs or not
    Returns:
        tf.Tensor: Tensor scalar representing the loss
    """
    # Get a softmax probability of the logits predictions and a one hot
    # encoding of the labels tensor
    probs = tf.nn.softmax(logits)
    onehot_labels = tf.one_hot(
        indices=labels,
        depth=num_classes,
        dtype=tf.float32,
        name='onehot_labels')

    # Compute the Dice similarity coefficient
    label_sum = tf.reduce_sum(onehot_labels, axis=[1, 2, 3], name='label_sum')
    pred_sum = tf.reduce_sum(probs, axis=[1, 2, 3], name='pred_sum')
    intersection = tf.reduce_sum(onehot_labels * probs, axis=[1, 2, 3],
                                 name='intersection')

    per_sample_per_class_dice = (2. * intersection + smooth)
    per_sample_per_class_dice /= (label_sum + pred_sum + smooth)

    # Include or exclude the background label for the computation
    if include_background:
        flat_per_sample_per_class_dice = tf.reshape(
            per_sample_per_class_dice, (-1, ))
        flat_label = tf.reshape(label_sum, (-1, ))
    else:
        flat_per_sample_per_class_dice = tf.reshape(
            per_sample_per_class_dice[:, 1:], (-1, ))
        flat_label = tf.reshape(label_sum[:, 1:], (-1, ))

    # Include or exclude non-present labels for the computation
    if only_present:
        masked_dice = tf.boolean_mask(flat_per_sample_per_class_dice,
                                      tf.logical_not(tf.equal(flat_label, 0)))
    else:
        masked_dice = tf.boolean_mask(
            flat_per_sample_per_class_dice,
            tf.logical_not(tf.is_nan(flat_per_sample_per_class_dice)))

    dice = tf.reduce_mean(masked_dice)
    loss = 1. - dice

    return loss

# ==============================================================================
# Based on tf.dcn
# -------------------------------------------------------------------------
def entropy(predictions, lower=1e-10, upper=1.0):
    """Calculate the entropy from the model output
    Args:
        predictions (tensor?): Model output
        lower (float): minval to calculate entropy over
        upper (float): maxval to calculate entropy over
    """
    return -tf.reduce_sum(predictions*tf.log(tf.clip_by_value(coarse_predictions,lower,upper)))

def hint_loss(feature_set_1, feature_set_2):
    """Calculate the hint loss, presumable per sample in a batch.
    Typcally used to find loss between the features of two layers in a network.
    Args:
        feature_set_1 (): Features from one layer
        feature_set_2 (): Features from a second layer
    """
    hint_loss = tf.reduce_sum(tf.square(feature_set_1, feature_set_2), name='hint_loss')
    return hint_loss

def dense_loss(predictions, labels, batch_size):

    sparse_labels = tf.reshape(labels, [batch_size,1])
    indices = tf.reshape(tf.range(batch_size), [batch_size,1])
    concated = tf.concat(1, [indices, sparse_labels])
    num_classes = predictions.get_shape()[-1].value
    dense_labels = tf.sparse_to_dense(concated, [batch_size, num_classes], 1.0, 0.0)

    losses.cross_entropy_loss(predictions, dense_labels, label_smoothing=0.0, weight=1.0)

# ==============================================================================
# Error and Distance
# ==============================================================================

def mean_squared_error(X1, X2):
    return tf.reduce_mean(tf.square(X1 - X2))

def clipped_error(x):
    # Huber loss
    try:
        return tf.select(tf.abs(x) < 1.0, 0.5 * tf.square(x), tf.abs(x) - 0.5)
    except:
        return tf.where(tf.abs(x) < 1.0, 0.5 * tf.square(x), tf.abs(x) - 0.5)

def euclidean_dist(X1, X2):
    return tf.sqrt(tf.reduce_sum(tf.pow(tf.subtract(X1, X2), 2), 1, keep_dims=True))

def weighted_dist(X1, X2):
    """Calculates the weighted distance using a fully connected layer
    """
    distance_vector = tf.subtract(X1, X2,  name=None)
    return fc_layer(distance_vector, 1, nonlinearity=tf.nn.sigmoid, name='fc_weighted')
