# Copyright 2016-2017 Henry Leopold. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Regularizer and activation function assignment and related scripts """
from __future__ import print_function, division, absolute_import
__author__ = 'Henry Leopold'
import tensorflow as tf
from tensorflow.python.layers import utils as tfutils
from tensorflow.python.layers.core import Dropout
import numpy as np
from config import cfg



__all__ = [ # ==================
            # Actvation functions / Helpers
            # ------------------
                'concat_elu',
                'leaky_relu',
                'prelu',
                'set_activation',
            # ==================
            # Regularizers
            # ------------------
                'alpha_dropout',
                'squash',
                'set_regularizer'
          ]
# ==============================================================================
# Activation Functions / Helpers
# ==============================================================================
def concat_elu(x):
    """ Concatenated ELU varation on CReLU
        Args:
            x (tensor): Input tensor
        Returns: CELu activated tensor
    """
    if cfg.data_format == 'NCHW':
        return tf.nn.elu(tf.concat([x, -x], 1))
    elif cfg.data_format == 'NHWC':
        return tf.nn.elu(tf.concat([x, -x], -1))

def leaky_relu(x, alpha=0.1, name=None):
    """Leaky ReLu activation function
    Args:
        x (tensor): Input tensor
        alpha (float): Leakiness parameter
        name (str): Optional name for the activation
    Returns: Leaky ReLu activated tensor
    """
    return tf.maximum(alpha * x, x, name)

def prelu(x, alpha_regularizer=None):
    """Probabilistic ReLu activation function
    Args:
        x (tensor): Input tensor
        alpha_regularizer (function): Optional variable regularizer.
            Tip: Use regularizers.set_regularizer in main script.
    Returns: PreLu activated tensor
    """ #TODO update with variable storage type creator
    alpha = tf.get_variable('alpha',
                            shape=[],
                            dtype=tf.float32,
                            regularizer=alpha_regularizer)

    return leaky_relu(x, alpha)

def set_activation(name, alpha=None, regularizer=None): #TODO make class or another means of handling parameters.
    """ Retrieves the proper activation function from a string.
    Parameters must be specified in main script.
    Args:
        name (str): Name of the activation
        alpha (float): Wildcard parameter (WIP)
        regularizer (float): Optional variable regularizer (WIP)
    Returns: Tensorflow activation function
    """
    if name == 'concat_elu':
        return concat_elu
    elif name == 'elu':
        return tf.nn.elu
    elif name == 'concat_relu':
        return tf.nn.crelu
    elif name == 'relu':
        return tf.nn.relu
    elif name == 'crelu':
        return tf.nn.crelu
    elif name == 'selu':
        return tf.nn.selu
    elif name == 'softplus':
        return tf.nn.softplus
    elif name == 'softsign':
        return tf.nn.softsign
    elif name == 'tanh':
        return tf.nn.tanh
    elif name == 'sigmoid':
        return tf.nn.sigmoid
    elif name == 'relu6':
        return tf.nn.relu6
    elif name == 'leaky_relu':
        return leaky_relu
    elif name == 'prelu':
        return prelu
    else:
        raise('Activation function "%s" is not supported'%name)

# ==============================================================================
# Activation Functions / Helpers
# ==============================================================================
class AlphaDropout(Dropout):
    def call(self, inputs, training=False):
        def dropped_inputs():
            return tf.contrib.nn.alpha_dropout(inputs, 1 - self.rate,
                            noise_shape=self._get_noise_shape(inputs),
                            seed=self.seed)
        return tfutils.smart_cond(training,
                                dropped_inputs,
                                lambda: array_ops.identity(inputs))

def alpha_dropout(inputs,rate=0.5,noise_shape=None,seed=None,training=False,name=None):
    """ "Applies Alpha Dropout to the input.

      Alpha Dropout is a dropout that maintains the self-normalizing property.
      For an input with zero mean and unit standard deviation, the output of Alpha
      Dropout maintains the original mean and standard deviation of the input.

      See "Self-Normalizing Neural Networks" https://arxiv.org/abs/1706.02515 """
    layer = AlphaDropout(rate=rate, noise_shape=noise_shape, seed=seed, name=name)
    return layer.apply(inputs, training=training)

def squash(inputs, epsilon=1e-8, name='squash'):
    """Squash regularization function
    Args:
        inputs (tensor): Input tensor / vector to be squashed.
        epsilon (float): Value to ensure no division by zero.
        name (str): Name for tensor/scope.
    Returns:
        Squashed tensor.
    """
    with tf.variable_scope(name) as scope:
        squared_norm = tf.reduce_sum(tf.square(inputs), -2, keep_dims=True)
        scalar_factor = squared_norm / (1. + squared_norm) / tf.sqrt(squared_norm + epsilon)
        squashed = scalar_factor * inputs
        return squashed

def set_regularizer(name='xavier', scale=1.0, **kwargs):
    """ Retrieves the proper regularization function from a string.
    Args:
        name (str): Name of the activation


    Returns: Tensorflow regularization function
    """
    try:
        if name == 'l2':
            return tf.contrib.layers.l2_regularizer(float(scale))
        elif name is 'l1':
            return tf.contrib.layers.l1_regularizer(float(scale))
        elif name is 'sum': #TODO enable string conversion
            return tf.contrib.layers.sum_regularizer([set_regularizer(k,v) for k,v in kwargs.items()])
        elif name is None:
            return None
        else: raise Exception
    except Exception as e:
        print('Initializer "%s" is not supported: %s'%(name, e))
