# Copyright 2016-2017 Henry Leopold. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Training and optimization function wrappers """
from __future__ import print_function, division, absolute_import
__author__ = 'Henry Leopold'
import re
import tensorflow as tf
import numpy as np
from .losses import image_loss as loss
from config import cfg



__all__ = [
        # ================
        # train
        # ----------------
            'train',
        # ================
        # Optimize
        # ----------------
            'optimize',
            'config_optimizer',
            #'_configure_learning_rate',
            #'_configure_optimizer',
        ]
# ==============================================================================
# Train
# ==============================================================================

def train(total_loss, learning_rate):
    with tf.variable_scope('optimizer'):
        train_op = tf.train.AdamOptimizer(learning_rate).minimize(total_loss)
        # Add histograms for trainable variables.
        # for var in tf.trainable_variables():
        #     tf.summary.histogram(var.op.name, var)

        # Add summary for learning rate.
        tf.summary.scalar('learning_rate', learning_rate)
        return train_op

def train_v2(total_loss, step):
    learning_rate = tf.train.exponential_decay(cfg.learning_rate,
                                          step,
                                          cfg.lr_decay_steps,
                                          cfg.learning_rate_decay_factor,
                                          staircase=True,
                                          name='exponential_decay_learning_rate')

    with tf.variable_scope('optimizer'):
        train_op = tf.train.AdamOptimizer(
                        learning_rate,
                        beta1=cfg.adam_beta1,
                        beta2=cfg.adam_beta2,
                        epsilon=cfg.opt_epsilon).minimize(total_loss)
        # Add summary for learning rate.
        tf.summary.scalar('learning_rate', learning_rate)

        return train_op

def train_v3(total_loss, step): #learning_rate):
    with tf.variable_scope('optimizer'):
        learning_rate, optimize_op = config_optimizer(step)
        train_op = optimize_op.minimize(total_loss)
        # Add summary for learning rate.
        tf.summary.scalar('learning_rate', learning_rate)
        return train_op


# ---------------------------------------------------------------------------------------------------
# Optimization and Learning Rate Config
# ---------------------------------------------------------------------------------------------------
def optimize(global_step):
    if cfg.optimizer == 'adam':
        # Calculate the learning rate schedule.
        num_batches_per_epoch = (cfg.train_samples_per_epoch *
                                 cfg.batch_size)

        # decay_steps = int(num_batches_per_epoch * cfg.num_epochs_per_decay)
        decay_steps = int(num_batches_per_epoch * cfg.num_epochs_per_decay)

        # Decay the learning rate exponentially based on the number of steps.
        learning_rate = tf.train.exponential_decay(cfg.learning_rate,
                                        global_step,
                                        decay_steps,
                                        cfg.learning_rate_decay_factor,
                                        staircase=cfg.staircase)

        # Create an optimizer that performs gradient descent.
        optimize_op = tf.train.AdamOptimizer(
                            learning_rate,
                            beta1=cfg.adam_beta1,
                            beta2=cfg.adam_beta2,
                            epsilon=cfg.opt_epsilon)
        return optimize_op, learning_rate

def config_optimizer(global_step):
    learning_rate = _configure_learning_rate(global_step)
    opt_op = _configure_optimizer(learning_rate)
    return learning_rate, opt_op

def configure_learning_rate(global_step):
    """Configures the learning rate.
    Args:
        num_samples_per_epoch: The number of samples in each epoch of training.
        global_step: The global_step tensor.
    Returns:
        A `Tensor` representing the learning rate.
    Raises:
        ValueError: if
    """
    # Calculate the learning rate schedule.
    num_batches_per_epoch = (cfg.train_samples_per_epoch / cfg.batch_size)
    decay_steps = int(num_batches_per_epoch * cfg.num_epochs_per_decay)

    if cfg.sync_replicas:
        decay_steps /= cfg.replicas_to_aggregate

    if cfg.learning_rate_decay_type == 'exponential':
        return tf.train.exponential_decay(cfg.learning_rate,
                                          global_step,
                                          decay_steps,
                                          cfg.learning_rate_decay_factor,
                                          staircase=cfg.staircase,
                                          name='exponential_decay_learning_rate')
    elif cfg.learning_rate_decay_type == 'fixed':
        return tf.constant(cfg.learning_rate, name='fixed_learning_rate')
    elif cfg.learning_rate_decay_type == 'polynomial':
        return tf.train.polynomial_decay(cfg.learning_rate,
                                         global_step,
                                         decay_steps,
                                         cfg.end_learning_rate,
                                         power=cfg.poly_power,
                                         cycle=cfg.poly_cycle,
                                         name='polynomial_decay_learning_rate')
    else:
        raise ValueError('learning_rate_decay_type [%s] was not recognized', cfg.learning_rate_decay_type)

def _configure_optimizer(learning_rate):
    """Configures the optimizer used for training.
    Args:
        learning_rate: A scalar or `Tensor` learning rate.
    Returns:
        An instance of an optimizer.
    Raises:
        ValueError: if cfg.optimizer is not recognized.
    """
    if cfg.optimizer == 'adam':
        return tf.train.AdamOptimizer(
            learning_rate,
            beta1=cfg.adam_beta1,
            beta2=cfg.adam_beta2,
            epsilon=cfg.opt_epsilon)
    elif cfg.optimizer == 'sgd':
        return tf.train.GradientDescentOptimizer(learning_rate)
    elif cfg.optimizer == 'momentum':
        optimizer = tf.train.MomentumOptimizer(learning_rate, 0.9)
    # TODO elif cfg.optimizer == "adadelta", "adagrad", "ftrl", "momentum", or "rmsprop"
    else:
        raise ValueError('Optimizer [%s] was not recognized', cfg.optimizer)
