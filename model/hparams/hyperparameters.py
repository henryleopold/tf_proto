#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (c) 2017 Henry Leopold. Unreleased.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Hyperparameter classes and functions """
__author__ = 'Henry A. Leopold'
from collections import OrderedDict, namedtuple

class _Hyperparameters(object):
    """ Base class used to store Hyperparameters
    """
    def __init__(self):
        super(_Hyperparameters, self).__init__()
        self.list = []

    def build(self, hparams):
        """ Method used to store hyperparameters inside this class
        Args:
             hparams (dict): Dictionary storing all hyperparameters values
        """
        for key in hparams:
            self.list.append(key)
            setattr(self, key, hparams[key])

class Hyperparameters(_Hyperparameters):
    """ Enhanced class for storing model and layer parameters.
    ie.
        {
            "model": {
                "name": "CapsNet",
                "learning_rate": 0.0001,
                "epochs": 1000,
                "num_classes": 43,
                "batch_size":50
            },
            "layers":  {
                        "conv" : {
                            "kernel_size": 9,
                            "filters": 256,
                            "strides":2,
                            "dropout":0.7,
                            "kernel_initializer": "tnormal_0.01_0",
                            "use_bias": true,
                            "bias_initializer": "zeros",
                            "use_bn": false,
                            "max_pooling": false,
                            "relu":true,
                            "activation":"relu",
                            "max_pooling":false,
                            "padding":"VALID"
                        },
                        "capsule_conv": {
                            "capsule_vector": 16,
                            "kernel_size": 5,
                            "num_capsules": 16
                        },
                        "capsule_fc": {
                            "capsule_vector": 32,
                            "routing": "dynamic",
                            "routing_iterations": 1,
                            "routing_initializer":"tnormal_0.01_0",
                            "num_capsules":"num_classes"
                        }
        },
            "optimizer": {

    }}
    """
    def __init__(self):
        super(Hyperparameters, self).__init__()
        self.layer_params = []

    def build(self, hparams):
        try:
            Model = namedtuple('Model', [k for k in hparams['model'].keys()])
            self.model = Model._make(hparams['model'].values())

            for layer in hparams['layers'].keys():
                _Layer = namedtuple('_Layer', [k for k in hparams['layers'][layer].keys()])
                _c = _Layer._make(hparams['layers'][layer].values())
                if 'filters' in _c._fields:
                    if _c.filters == 'num_classes':
                        _c = _c._replace(filters=self.model.num_classes)
                if 'num_capsules' in _c._fields:
                    if _c.num_capsules == 'num_classes':
                        _c = _c._replace(num_capsules=self.model.num_classes)
                self.layer_params.append((layer, _c))

        except KeyError:
            self.htype = 'list'
            for key in hparams:
                self.list.append(key)
                setattr(self, key, hparams[key])

    def get_str(self, use='model'):
        """ Converts hyperparameters into a string, typically for filenaming.
        Args:
            use (str): Which parameters to use to generate the string.
                    Options:
                            'model' - Use only model hyperparameters
                            'layers' - Use only layer hyperparameters
                            'both' - Use model hyperparameters and layer names
                            'all' - Use all hyperparameter key value pairs
                            'list' - In the case where the base class is used.
        """
        hparam_string = ''
        if self.htype == 'list':
            hparam_string=self.l2s(self.list, hparam_string)
            return hparam_string
        if use == 'model':
            self.p2s(self.model, hparam_string)
        elif use == 'layer':
            for key in self.layer_params.keys():
                self.s2s(key, hparam_string)
                self.p2s(self.layer_params[key], hparam_string)
        elif use == 'both':
            self.p2s(self.model, hparam_string)
            self.k2s(self.layer_params, hparam_string)
        elif use == 'all':
            self.p2s(self.model, hparam_string)
            for key in self.layer_params.keys():
                self.s2s(key, hparam_string)
                self.p2s(self.layer_params[key], hparam_string)
        elif use == 'list':
            self.l2s(self.list, hparam_string)
        else:
            print("Use type not recognized: %s"%use)
            return
        return hparam_string

    def p2s(self, d, s='', delim='_'):
        for [k,v] in d:
            prepend = "" if len(s) == 0 else delim
            s += "%s%s%s%s" % (prepend, k, delim, v)
        return s

    def k2s(self, d, s='', delim='_'):
        for k in d.keys(): self.s2s(k, s, delim)
        return s

    def s2s(self, d, s='', delim='_'):
        prepend = "" if len(s) == 0 else delim
        s += "%s%s" % (prepend, d)

    def l2s(self, l, s='', delim='_'):
        h = [("".join([p[0] for p in v.split("_")]), getattr(self, v)) for v in l]
        return self.p2s(h, s, delim)

class HyperparametersPlus(Hyperparameters):
    """ Extending the Hyperparameter class for more modular networks.

    Original Hyperparameter list options:
        model:
        layers:

    Extended HyperparameterPlus list options:
        model:
        network: Replaces layers in practice
        encoder:
        decoder:
        ?agents:
        ?ensemble:
        ?losses:
        ?kpis:
        ?train:
        ?test:
    """

    def __init__(self):
        #TODO add check for filepath and if it is run: hyperparameters = read_json(hparams_path, OrderedDict)
        super().__init__()


    def build(self, hparams):
        # super().__init__()
        # try:
        Model = namedtuple('Model', [k for k in hparams['model'].keys()])
        self.model = Model._make(hparams['model'].values())

        self.layer_params = _process_hparams(hparams['layers']) if 'layers' in hparams else None

        self.network = _process_hparams(hparams['network']) if 'network' in hparams else None

        self.encoder = _process_hparams(hparams['encoder']) if 'encoder' in hparams else None

        self.decoder = _process_hparams(hparams['decoder']) if 'decoder' in hparams else None

        # except KeyError:
        #     self.htype = 'list'
        #     for key in hparams:
        #         self.list.append(key)
        #         setattr(self, key, hparams[key])

    def _process_hparams(self, hparams):
        """ Wrapper function for converting hparams.
        Args:
            hparams (json): Subset of the json read. ie. hparams['layers']
        Returns:
            params (list): Hyperparameter named tuples
        """
        _params = []
        for prm in hparams.keys():
            Module = namedtuple('Module', [k for k in hparams[prm].keys()])
            _c = Module._make(hparams[prm].values())
            if 'filters' in _c._fields:
                if _c.filters == 'num_classes':
                    _c = _c._replace(filters=self.model.num_classes)
            if 'num_capsules' in _c._fields:
                if _c.num_capsules == 'num_classes':
                    _c = _c._replace(num_capsules=self.model.num_classes)
            _params.append((prm, _c))
        return _params

    def get_str(self, use='model'):
        """ Converts hyperparameters into a string, typically for filenaming.
        Args:
            use (str): Which parameters to use to generate the string.
                    Options:
                            'model' - Use only model hyperparameters
                            'layers' - Use only layer hyperparameters
                            'network' - Use only network hyperparameters
                            'encoder' - Use only encoder hyperparameters
                            'decoder' - Use only decoder hyperparameters
                            'all' - Use all hyperparameter key value pairs
                            'list' - In the case where the base class is used.
        """
        hparam_string = ''
        if self.htype == 'list':
            hparam_string=self.l2s(self.list, hparam_string)
            return hparam_string
        if use == 'model':
            self.p2s(self.model, hparam_string)
        elif use == 'layer':
            _layer_hp2s(self.layer_params)
        elif use == 'network':
            _layer_hp2s(self.network)
        elif use == 'encoder':
            _layer_hp2s(self.encoder)
        elif use == 'decoder':
            _layer_hp2s(self.decoder)
        # elif use == 'both':
        #     self.p2s(self.model, hparam_string)
        #     self.k2s(self.layer_params, hparam_string)
        elif use == 'all':
            self.p2s(self.model, hparam_string)
            _layer_hp2s(self.layer_params)
            _layer_hp2s(self.network)
            _layer_hp2s(self.encoder)
            _layer_hp2s(self.decoder)
        elif use == 'list':
            self.l2s(self.list, hparam_string)
        else:
            raise TypeError("Use type not recognized: %s"%use)
        return hparam_string

    def _layer_hp2s(self, hparams):
        for key in hparams.keys():
            self.s2s(key, hparam_string)
            self.p2s(hparams[key], hparam_string)
