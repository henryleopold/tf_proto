#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (c) 2017 Henry Leopold. Unreleased.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Layer class declarations and related functions """
__author__ = 'Henry A. Leopold'
from collections import OrderedDict
from model.components import set_initializer, set_activation, set_regularizer, alpha_dropout, squash
import numpy as np
import tensorflow as tf
from config import cfg
from .core import conv_layer, fc_layer, deconv_layer
import horovod.tensorflow as hvd


__all__ = [ # ===========================
            # Layer Classes
            # ---------------------------
                'Layers',
                'Layer',
            # ---------------------------

]
# ==============================================================================
# Layer Classes
# ==============================================================================
class Layers(OrderedDict):
    """Specialized dictionary for storing layers"""
    def __setitem__(self, key, value):
        if key in self:
            del self[key]
        OrderedDict.__setitem__(self, key, value)

    def pv(self, v=-1):
        """Pops out a value at a location"""
        if self._debug(): print('popping: %s'% self.keys()[v])
        return self.pop(self.keys()[v])

    def sort(self, sort):
        """Returns the dictionary sorted by sort"""
        if sort is 'key':
            return sorted(self.items(), key=lambda t: t[0])
        elif sort is 'value':
            return sorted(self.items(), key=lambda t: t[1])
        elif sort is 'length' or 'len':
            return sorted(self.items(), key=lambda t: len(t[0]))
        else:
            return self.items()

    def last(self):
        return (self[next(reversed(self))])

    def debug(self):
        print('Checking contents...\nkey:value\n%s:%s\n'%(((k,v) for k,v in self.items())))


class Layer(object):
    """Abstract layer.
    Args:
    Returns:
    """
    def __init__(self, layer_name, training, last_layer, **kwargs):
        self.layer_name = layer_name
        self.training = training
        self.built = False
        for key, value in kwargs.items():
            if "initializer" in key: value = set_initializer(*value.split('_'))
            if "regularizer" in key: value = set_regularizer(*value.split('_'))
            if key == "activation": value = set_activation(value)
            if value == "num_classes": value = cfg.num_classes
            setattr(self, key, value)

        self.data_format = kwargs.get('data_format',cfg.data_format)
        self._layer_spec = self.build_code.split('_')
        self.layer_type = 'layer'
        self.input_specs = last_layer

        # if self.use_bn: self.activation = None

    def __call__(self, inputs):
        if not self.built:
            try:
                self.input_shape = inputs.get_shape().as_list()
                self.tensor = self._make_layer(inputs)
                self.built = True
            except Exception as e:
                if hvd.rank() ==0:
                    print('Layer "%s" creation has failed: %s'%(self.layer_name,e))
                    self.debug()
        return self.tensor

    def __getattr__(self, item):
        if item == 'padding':
            return 'VALID'
        elif item == 'bias_initializer':
            return tf.zeros_initializer()
        elif item == 'squash_eps':
            return 1e-8
        elif item == 'dtype':
            return 1e-8
        elif any(item == x for x in ('strides', 'kernel_size', 'dilation_rate', 'routing_iterations', 'capnum_prior')):
            return 1
        elif any(item==x for x in ('routing_beta_a_initializer','routing_beta_v_initializer')):
            return set_initializer('tnormal',0.01,0)
        elif any(item == x for x in ('use_bias', 'trainable')):
            return True
        elif item == 'training':
            return False
        elif item == 'units':
            return self.filters
        elif any(item == x for x in ('epsilon', 'routing_lambda','routing_lambda_step', 'data_format')):
            return getattr(cfg, item)
        else: return None

    def __str__(self):
        return self.layer_name

    def __repr__(self):
        pass

    # ==========================================================================
    # Layer functions
    # --------------------------------------------------------------------------
    def _make_layer(self, inputs):#, batch_size=None): #TODO
        if self.data_format == 'NCHW': self._df = 'channels_first'
        else: self._df = 'channels_last'

        if type(self.layer_name) is int:
            self.layer_name = '%s_%s'%(self._layer_spec[0],str(self.layer_name))

        # with tf.variable_scope(self.layer_name) as scope:
        if self._layer_spec[0] == 'fc':
            _layer = self._build_fc(inputs)

        # Convolutional layers
        elif self._layer_spec[0] == 'conv':
            _layer = self._build_conv(inputs)

        elif self._layer_spec[0] == 'deconv':
            _layer = self._build_deconv(inputs)

        elif self._layer_spec[0] == 'sepconv':
            _layer = self._build_sepconv(inputs)

        # Pooling layers
        elif self._layer_spec[0] == 'pool':
            _layer = self._build_pool(inputs)

        # Normalization layers
        elif self.build_code == 'batch_norm':
            _layer = self._build_bn(inputs)

        # Wildcard (build it outside and inject it)
        elif self.build_code == 'wildcard':
            _layer = inputs

        else:
            print('Layer type not recognised: %s'% self.layer_name)
            return

        _layer = self._activation_regularization(_layer)

        if self._debug():
            print('\n************ Layer Debug: %s *************\n layer_spec:%s'%(self.layer_name,self._layer_spec))
            try: print('layer_func: %s'%(layer_func))
            except: pass
            self.debug()

        self.shape = _layer.get_shape().as_list()
        return _layer
    # ==========================================================================
    # Helper functions
    # --------------------------------------------------------------------------
    # Variable creation
    # --------------------------------------------------------------------------
    def var_prep(self, inputs, strides, kernel_size, filters=None, data_format='NCHW'):
        """ Wrapper for getting input shape, strides and kernel size based on data
        format
        """
        input_shape = input.get_shape().as_list()
        kernel_size = var2list(kernel_size)
        strides = var2list(strides)
        filters = kernel_size**2 if filters is None else filters
        if 'NC' in data_format:
            strides = [1, 1, strides[0], strides[1]]
            kernel_shape = [kernel_size[0], kernel_size[1], input_shape[1], filters]
        elif data_format == 'NHWC':
            strides = [1, strides[0], strides[1], 1]
            kernel_shape = [kernel_size[0], kernel_size[1], input_shape[-1], filters]

        return input_shape, strides, kernel_shape

    def _variable(self, name, shape, initializer, dtype, trainable=True, reuse=False):
        """Helper to create a Variable.
        Args:
            name: name of the variable
            shape: list of ints
            initializer: initializer for Variable
        Returns:
            Variable Tensor
        """
        with tf.variable_scope(tf.get_variable_scope(), reuse=reuse):
            if cfg.use_fp16:
                dtype=tf.float16
            else:
                dtype=tf.float32
            if cfg.var_on_cpu:
                with tf.device('/cpu:0'):
                    var = tf.get_variable(name, shape, dtype=dtype, initializer=initializer, trainable=trainable)
                return var
            var = tf.get_variable(name, shape,dtype=dtype, initializer=initializer, trainable=trainable)
            return var

    def _variable_with_weight_decay(self, name, shape, stddev, wd, dtype, trainable=True, reuse=False):
        """Helper to create an initialized Variable with weight decay.

        Note that the Variable is initialized with a truncated normal distribution.
        A weight decay is added only if one is specified.

        Args:
        name: name of the variable
        shape: list of ints
        stddev: standard deviation of a truncated Gaussian
        wd: add L2Loss weight decay multiplied by this float. If None, weight
            decay is not added for this Variable.

        Returns:
        Variable Tensor
        """
        var = self._variable(name, shape,
                             tf.truncated_normal_initializer(stddev=stddev), dtype, reuse=reuse, trainable=trainable)
        if wd is not None:
            weight_decay = tf.multiply(tf.nn.l2_loss(var), wd, name='weight_loss')
            tf.add_to_collection('losses', weight_decay)
        return var

    # --------------------------------------------------------------------------
    # Layer builders
    # --------------------------------------------------------------------------
    def _build_fc(self, inputs):
        return tf.layers.dense(inputs,
                            units=self.units,
                            activation=self.activation,
                            use_bias=self.use_bias,
                            kernel_initializer=self.kernel_initializer,
                            bias_initializer=self.bias_initializer,
                            kernel_regularizer=self.kernel_regularizer,
                            bias_regularizer=self.bias_regularizer,
                            activity_regularizer=self.activity_regularizer,
                            kernel_constraint=self.kernel_constraint,
                            bias_constraint=self.bias_constraint,
                            trainable=self.trainable,
                            name=self.layer_name,
                            reuse=self.reuse)

    def _build_conv(self, inputs):
        if self._layer_spec[-1] == 1: layer_func = tf.layers.conv1d
        elif self._layer_spec[-1] == 3: layer_func = tf.layers.conv3d
        else: layer_func = tf.layers.conv2d
        return layer_func(inputs,
                                filters=self.filters,
                                kernel_size=self.kernel_size,
                                strides=self.strides,
                                padding=self.padding,
                                data_format=self._df, #self.data_format, #
                                dilation_rate=self.dilation_rate,
                                activation=self.activation,
                                use_bias=self.use_bias,
                                kernel_initializer=self.kernel_initializer,
                                bias_initializer=self.bias_initializer,
                                kernel_regularizer=self.kernel_regularizer,
                                bias_regularizer=self.bias_regularizer,
                                activity_regularizer=self.activity_regularizer,
                                kernel_constraint=self.kernel_constraint,
                                bias_constraint=self.bias_constraint,
                                trainable=self.trainable,
                                name=self.layer_name,
                                reuse=self.reuse)

    def _build_deconv(self, inputs):
        if self._layer_spec[-1] == 3: layer_func = tf.layers.conv3d_transpose
        else: layer_func = tf.layers.conv2d_transpose
        return layer_func(inputs,
                                filters= self.filters,
                                kernel_size=self.kernel_size,
                                strides=self.strides,
                                padding=self.padding,
                                data_format=self._df, #self.data_format, #
                                activation=self.activation,
                                use_bias=self.use_bias,
                                kernel_initializer=self.kernel_initializer,
                                bias_initializer=self.bias_initializer,
                                kernel_regularizer=self.kernel_regularizer,
                                bias_regularizer=self.bias_regularizer,
                                activity_regularizer=self.activity_regularizer,
                                kernel_constraint=self.kernel_constraint,
                                bias_constraint=self.bias_constraint,
                                trainable=self.trainable,
                                name=self.layer_name,
                                reuse=self.reuse)

    def _build_sepconv(self, inputs):
        layer_func = tf.layers.separable_conv2d #if self._layer_spec[-1] == 2
        # layer_func = tf.layers.conv3d_transpose if self._layer_spec[-1] == 3
        return layer_func(inputs,
                                filters=self.filters,
                                kernel_size=self.kernel_size,
                                strides=self.strides,
                                padding=self.padding,
                                data_format=self._df,
                                dilation_rate=self.dilation_rate,
                                depth_multiplier=self.depth_multiplier,
                                activation=self.activation,
                                use_bias=self.use_bias,
                                depthwise_initializer=self.depthwise_initializer,
                                depthwise_regularizer=self.depthwise_regularizer,
                                pointwise_initializer=self.pointwise_initializer,
                                pointwise_regularizer=self.pointwise_regularizer,
                                depthwise_constraint=self.depthwise_constraint,
                                pointwise_constraint=self.pointwise_constraint,
                                bias_initializer=self.bias_initializer,
                                bias_regularizer=self.bias_regularizer,
                                bias_constraint=self.bias_constraint,
                                activity_regularizer=self.activity_regularizer,
                                trainable=self.trainable,
                                name=self.layer_name,
                                reuse=self.reuse)

    def _build_bn(self, inputs):
        return tf.layers.batch_normalization(inputs,
                    axis=self.axis,
                    momentum=self.momentum,
                    epsilon=self.epsilon,
                    center=self.center,
                    scale=self.scale,
                    beta_initializer=self.beta_initializer,
                    gamma_initializer=self.gamma_initializer,
                    moving_mean_initializer=self.moving_mean_initializer,
                    moving_variance_initializer=self.moving_variance_initializer,
                    beta_regularizer=self.beta_regularizer,
                    gamma_regularizer=self.gamma_regularizer,
                    beta_constraint=self.beta_constraint,
                    gamma_constraint=self.gamma_constraint,
                    training=self.training,
                    trainable=self.trainable,
                    name=self.layer_name,
                    reuse=self.reuse,
                    renorm=self.renorm,
                    renorm_clipping=self.renorm_clipping,
                    renorm_momentum=self.renorm_momentumself,
                    fused=self.fused)

    def _build_pool(self, inputs):#TODO look into tf.nn.pool
        """ Presume the format is pool_max_2 """
        if self._layer_spec[1] == 'max':
            if self._layer_spec[2] == 1: layer_func = tf.layers.max_pooling1d
            elif self._layer_spec[2] == 3: layer_func = tf.layers.max_pooling3d
            else: layer_func = tf.layers.max_pooling2d
        else:# elif self._layer_spec[0] == 'avg':
            if self._layer_spec[2] == 1: layer_func = tf.layers.avg_pooling1d
            elif self._layer_spec[2] == 3: layer_func = tf.layers.avg_pooling3d
            else: layer_func = tf.layers.avg_pooling2d
        return layer_func(inputs, self.pool_size,
                          self.strides, self.padding,
                          data_format=self._df, name=self.layer_name)


    def _activation_regularization(self, layer):
        if self.squash: layer = squash(layer, self.squash_eps)

        # TODO Make a condition to automatically do this if necessary.
        if self.flatten:
            layer = tf.layers.flatten(layer)

        if self.alpha_dropout is not None:
            layer = alpha_dropout(layer, self.alpha_dropout, self.noise_shape, self.seed, self.training)

        elif self.dropout is not None:
            layer = tf.layers.dropout(layer, self.dropout, self.noise_shape, self.seed, self.training)

        return layer

    # ==========================================================================
    # Utility functions
    # --------------------------------------------------------------------------
    def debug(self):
        if hvd.rank() == 0:
            print('\n***** Debugging Dump *****')
            for k,v in self.__dict__.items():
                print('%s: %s'%(k,v))
            print('GPU:%s of %s'%(hvd.rank()+1, hvd.size()))
            print('***** Finished Dump *****\n')

    def _debug(self):
        """ Internal checker to for debugging.
        Prevents replication for running on horovod """
        return all([cfg.debug, hvd.rank() == 0])

    def _logging(self, detail='info'):
        """ Prints infornation to tf.logging
        """
        if hvd.rank() == 0:
            if detail == 'info':
                tf.logging.info('{} shape: {}'.format(self.layer_name, self.shape))
            elif detail == 'debug':
                tf.logging.info('%s'%(('%s: %s'%(k,v) for k,v in self.__dict__.items())))

    def _log(self, msg, detail='info'):
        """ Prints infornation to tf.logging
        """
        if hvd.rank() == 0:
            if detail == 'info':
                tf.logging.info('{}'.format(msg))
