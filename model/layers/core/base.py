# Copyright 2016-2017 Henry Leopold. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Core layers generation and related functions """
from __future__ import print_function, division, absolute_import
__author__ = 'Henry Leopold'

from six.moves import range
import tensorflow as tf
import numpy as np
from .helpers import *
from model.components import set_initializer, var2list, _variable, var_prep
from config import cfg



__all__ = [
            # ==================
            # Base Layers
            # ------------------
                'fc_layer',
            # ==================
            # Convolution Layers
            # ------------------
                'conv_layer',
            # ------------------
            # Deconvolution Layers
            # ------------------
                'deconv_layer',
          ]

# ==============================================================================
# Base Layers
# ==============================================================================
def fc_layer(input, units, activation=None, activity_regularizer=None, training=False, kernel_regularizer = None, flatten = False, kernel_initializer = None, kernel_constraint=None, bias_regularizer = None, bias_initializer = None, data_format=cfg.data_format, use_bias=True, bias_constraint=None, trainable=True, name=None,reuse=None):
    """Fully connected layer"""
    input_shape = input.get_shape().as_list()
    if flatten:
        dim = input_shape[1] * input_shape[2] * input_shape[3]
        input_processed = tf.reshape(input, [-1, dim])
    else:
        dim = input_shape[1]
        input_processed = input
    # TODO update for global / smart selection of initializers
    weights = _variable('weights', shape=[dim, units], initializer=set_initializer(kernel_initializer))
    output = tf.matmul(input_processed, weights)
    # if use_bias:
    #     biases = _variable('biases', [units], initializer=set_initializer(bias_initializer))
    #     output = tf.add(output, biases)
    # if activation is not None:
    #     output = activation(ouput)
    return output

# ==============================================================================
# Convolution Layers
# ==============================================================================
def conv_layer(input, kernel_size, strides, filters, activation=None, use_bn = False, padding="SAME", training=False, kernel_regularizer = None, kernel_initializer = 'xavier', dilation_rate=None, bias_regularizer = None, bias_initializer = 'xavier', data_format=False, activity_regularizer=None, kernel_constraint=None, bias_constraint=None, use_bias=True, trainable=True, name=None,reuse=None):
    """Convolution layer"""
    input_shape, strides, kernel_shape = var_prep(inputs, strides, kernel_size, filters, data_format)

    weights = _variable('weights', shape=kernel_shape, initializer=set_initializer(kernel_initializer))
    conv = tf.nn.conv2d(input, weights, strides=strides, padding=padding, data_format=data_format)
    # if use_bias:
    #     biases = _variable('biases', [filters], initializer=set_initializer(bias_initializer))
    #     conv = tf.nn.bias_add(conv, biases, data_format=data_format)
    # if use_bn:
    #     return conv
    # if activation is not None:
    #     conv = activation(conv)
    return conv

# ==============================================================================
# Deconvolution Layers
# ==============================================================================
def deconv_layer(input, kernel_size, strides, filters, activation=None, use_bn = False, padding ="SAME", training=False, kernel_regularizer = None, kernel_initializer = 'xavier', bias_regularizer = None, bias_initializer = 'xavier', trainable=True, name=None,reuse=None):
    """Transpose convolution layer"""
    kernel_size = var2list(kernel_size)
    strides = var2list(strides)
    s = int_shape(input)
    if data_format is 'NCHW':
        strides = [1, 1, strides[0], strides[1]]
        kernel_shape =[kernel_size[1],kernel_size[0], filters,  input.get_shape()[1]]
        output_shape = tf.stack([tf.shape(input)[0], filters, tf.shape(input)[2] * strides[0], tf.shape(input)[3] * strides[1]])
        conv_shape = [s[0], filters, s[2] * strides[0], s[3] * strides[1]]
    elif data_format is 'NHWC':
        strides = [1, strides[0], strides[1], 1]
        kernel_shape = [kernel_size[1],kernel_size[0], filters,  input.get_shape()[-1]]
        output_shape = tf.stack([tf.shape(input)[0], tf.shape(input)[1] * strides[0], tf.shape(input)[2] * strides[1], filters])
        conv_shape = [s[0], s[1] * strides[0], s[2] * strides[1], filters]

    weights = _variable('weights', shape=kernel_shape, initializer=set_initializer(kernel_initializer))
    batch_size = tf.shape(input)[0]
    conv = tf.nn.conv2d_transpose(input, weights, output_shape, strides=strides, padding=padding, data_format=cfg.data_format)
    conv = tf.reshape(conv, conv_shape)
    if use_bias:
        biases = _variable('biases', [filters], initializer=set_initializer(bias_initializer))
        conv = tf.nn.bias_add(conv, biases, data_format=cfg.data_format)
    if use_bn:
        return conv
    if activation is not None:
        conv = activation(conv)
    return conv
