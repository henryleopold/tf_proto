# Copyright 2016-2017 Henry Leopold. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Base layers generation and related functions """
from __future__ import print_function, division, absolute_import
__author__ = 'Henry Leopold'
from six.moves import range  # pylint: disable=redefined-builtin
import tensorflow as tf
import numpy as np
from config import cfg



__all__ = [ # ==================
            # Summary functions
            # ------------------
                '_activation_summary',
                '_add_loss_summaries',
                '_image_summary'

          ]

# ==============================================================================
# Summary functions
# ==============================================================================
# ------------------------------------------------------------------------------
def _activation_summary(x):
    """Helper to create summaries for activations.
    Creates a summary that provides a histogram of activations.
    Creates a summary that measure the sparsity of activations.
    Args:
      x: Tensor
    Returns:
      nothing
    """
    tensor_name = x.op.name
    tf.summary.histogram(tensor_name + '/activations', x)
    tf.summary.scalar(tensor_name + '/sparsity', tf.nn.zero_fraction(x))

def _add_loss_summaries(total_loss):
    """Add summaries for losses in [multi-gpu] model.
    Generates moving average for all losses and associated summaries for visualizing the performance of the network.
    Args:
        total_loss: Total loss from loss().
    Returns:
        loss_averages_op: op for generating moving averages of losses.
    """
    # Compute the moving average of all individual losses and the total loss.
    loss_averages = tf.train.ExponentialMovingAverage(0.9, name='avg')
    losses = tf.get_collection('losses')
    loss_averages_op = loss_averages.apply(losses + [total_loss])

    # Attach a scalar summary to all individual losses and the total loss; do the same for the averaged version of the losses.
    for l in losses + [total_loss]:
        # Name each loss as '(raw)' and name the moving average version of the loss as the original loss name.
        tf.summary.scalar(l.op.name + ' (raw)', l)
        tf.summary.scalar(l.op.name, loss_averages.average(l))
    return loss_averages_op

def _image_summary(image, name='image', NCHW=True):
    """Helper to create summaries of input images.
    Args:
      image: Tensor
      name: Name for the summary
    Returns:
      nothing
    """
    # if the shape was NCHW, we need to set it back to NHWC before summary
    if data_format is 'NCHW' and NCHW:
        image = tf.transpose(image, [0, 2, 3, 1])
    tf.summary.image(name, image)
