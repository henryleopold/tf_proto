#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (c) 2017 Henry Leopold. Unreleased.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Capsule Routing class declarations and related functions """
__author__ = 'Henry A. Leopold'

from model.components import squash
import numpy as np
import tensorflow as tf
from tensorflow.python.layers import base
# from model.layers import Layer, Layers
# from model.layers.core import conv_layer, fc_layer
from model.components import squash
from config import cfg

# __all__ = [ # ===========================
#             # Routing Classes
#             # ---------------------------
#                 'Routing'
#             # ---------------------------
#
# ]

# ==============================================================================
# Routing Classes
# ------------------------------------------------------------------------------


class Routing(base.Layer):
    """ Adds a routing operation. Based on [1], [2]
    Arguments:
        inputs (tensor):
        num_capsules (int):
        capsule_vector (int):
        use_b_ij (boolean):
        routing_initializer (str):
        routing_iterations (int):
        routing_operation (str):
        routing_regularizer (str):
        b_ij_initializer (str):
        b_ij_regularizer (str):
        b_ij_constraint (str):
        activity_regularizer (str):
        routing_constraint (str):
        trainable (boolean):
        name (str): Optional scope for name_scope.

    Returns:
        A `Tensor` representing the results of the routing operation.

    Raises:


    References:
        ..[1] Dynamic
        ..[2] Matrix
    """

    def __init__(self,
                 num_capsules,
                 capsule_vector,
                 use_b_ij=True,
                 routing_initializer=None,
                 routing_iterations=1,
                 routing_operation=None,
                 routing_regularizer=None,
                 routing_matrix_size_size=1,
                 b_ij_initializer=tf.zeros_initializer(),
                 b_ij_regularizer=None,
                 b_ij_constraint=None,
                 routing_constraint=None,
                 name=None,
                 **kwargs):
        self.routing_initializer = routing_initializer
        self.routing_iterations = routing_iterations
        self.routing_operation = routing_operation
        self.routing_regularizer = routing_regularizer
        self.routing_constraint = routing_constraint
        self.num_capsules = num_capsules
        self.capsule_vector = capsule_vector
        self.b_ij_initializer=b_ij_initializer,
        self.b_ij_regularizer=b_ij_regularizer,
        self.b_ij_constraint=b_ij_constraint
        super(Routing, self).__init__(name=name, **kwargs)


    def build(self, input_shape):
        """ Input shape should be: [batch_size, input_capsules, input_capvec]
        """
        self.input_capsules= input_capsule
        self.input_capvec =input_capvec

        _rw_shape = (self.input_capsules, self.num_capsules,
                                 self.input_capvec, self.capsule_vector)
        self.routing_kernel = self.add_variable(name='routing_kernel',
                                    shape=_rw_shape,
                                    initializer=self.routing_initializer,
                                    regularizer=self.routing_regularizer,
                                    constraint=self.routing_constraint,
                                    trainable=True,
                                    dtype=self.dtype)

        _b_ij_shape = (1, self.input_capsules, self.num_capsules, 1, 1)
        self.b_ij = self.add_variable(name='b_ij_log_prior',
                                shape=_b_ij_shape,
                                initializer=self.b_ij_initializer,
                                regularizer=self.b_ij_regularizer,
                                constraint=self.b_ij_constraint,
                                trainable=True,
                                dtype=self.dtype)

        u_ij_init = tf.zeros_initializer([self.input_capsules, self.num_capsules, 1,
        self.capsule_vector])

        self.built = True

    def call(self, inputs, training=None):
        inputs = ops.convert_to_tensor(inputs)
        shape = inputs.get_shape().as_list()
        inputs = tf.expand_dims(tf.expand_dims(inputs, axis=2), axis=2)
        inputs = tf.tile(inputs, [1,1,self.num_capsules,1,1])
        prediction = tf.scan(lambda ac, x: tf.matmul(x, self.routing_kernel),
                               inputs, initializer=u_ij_init, name='cap_predicts')

        for i in range(self.routing_iterations):
            c_ij = tf.nn.softmax(self.b_ij, dim=2)
            s_j = tf.multiply(c_ij, self.u_hat)
            s_j = tf.reduce_sum(s_j, axis=1, keep_dims=True)
            v_j = squash(s_j)
            # v_j_tiled = tf.tile(v_j, [1, self.filters_prior, 1, 1, 1])
            if i != self.routing_iterations -1:
                u_dot_v = tf.matmul(self.u_hat, v_j, transpose_a=True)
                self.b_ij += tf.reduce_sum(u_dot_v, axis=0, keep_dims=True)
        return tf.squeeze(v_j, axis=1)


        #
        # if num_channels % self.num_units:
        #     raise ValueError('number of features({}) is not '
        #                      'a multiple of num_units({})'
        #                      .format(num_channels, self.num_units))
        # shape[self.axis] = -1
        # shape += [num_channels // self.num_units]
        #
        # # Dealing with batches with arbitrary sizes
        # for i in range(len(shape)):
        #     if shape[i] is None:
        #         shape[i] = gen_array_ops.shape(inputs)[i]
        # outputs = math_ops.reduce_max(
        #     gen_array_ops.reshape(inputs, shape), -1, keep_dims=False)
        #
        # return outputs

# ==============================================================================
# Routing Functions #TODO convert routing from classes into functions
# ------------------------------------------------------------------------------
# Dynamic Routing
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# EM Routing
# ------------------------------------------------------------------------------
