#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (c) 2017 Henry Leopold. Unreleased.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Capsule class declarations and related functions """
__author__ = 'Henry A. Leopold'

import numpy as np
import tensorflow as tf
from model.layers import Layer, Layers
from model.layers.core import conv_layer, fc_layer
from model.components import squash, var_prep, insert2list
from config import cfg

# __all__ = [ # ===========================
#             # Capsule Classes
#             # ---------------------------
#                 'Capsule'
#             # ---------------------------
#
# ]

# ==============================================================================
# Capsule Classes
# ------------------------------------------------------------------------------
class Capsule(Layer):
    def __call__(self, inputs):
        if not self.built:
            self.capsule = self._make_capsule(inputs)
            print('Compiled %s: %s'%(self.layer_name, self.capsule))
            self.built = True
        return self.capsule

    def _make_capsule(self, inputs):
        if self._debug(): print('Making capsule: %s\nInputs: %s\nInput specs: %s'%(self.layer_name,inputs,self.input_specs))
        self._layer_spec = self._layer_spec[1:]
        self.input_shape = self.input_specs.shape
        self.batch_size = self.input_shape[0]
        self.channel_axis = 1 if 'NC' in self.data_format else 3
        if self.capsule_vector is None:
            self.capsule_vector = self.routing_matrix_size**2 if self.routing_matrix_size is not None else cfg.routing_matrix_size**2
        self.filters = self.num_capsules * self.capsule_vector

        try:
            self.capvec_prior = self.input_specs.capsule_vector
            self.capnum_prior = self.input_specs.num_capsules*self.kernel_size*self.kernel_size
            self.activation_prior = self.input_specs.activation_unit
        except Exception as e:
            print('Input does not appear to be a capsule, so routing is disabled: %s'%e)
            self.routing = None

        # try:
        #
        if self.layer_name == 'PrimaryCaps':
            self.filters_prior = self.input_shape[self.channel_axis]
            self._set_activation_unit(inputs)
            capsule = self._make_layer(inputs)
            self.shape = capsule.get_shape().as_list()
            if self._debug(): print('New capsule: %s\nshape: %s'%(capsule,self.shape))
            capsule = tf.reshape(capsule, shape=(self.batch_size, self.shape[1], self.shape[2], self.num_capsules, -1))
            # capsule = tf.reshape(capsule, shape=(self.batch_size, -1, self.capsule_vector))
            return capsule
        else:
            # if self._debug(): print('\nNew capsule: %s\nlayer_spec: %s\n'%(self.layer_name,self._layer_spec))
            with tf.variable_scope(self.layer_name) as scope:
                if self._layer_spec[0] == 'conv':
                    capsule, self.activation_unit = _conv_capsule(inputs, self.activation_prior, self.kernel_size, self.strides, data_format=self.data_format, padding=self.padding, rate=self.dilation_rate, debug=self._debug())
                    self.shape = capsule.get_shape().as_list()
                    if self._debug(): print('New capsule: %s\nshape: %s'%(capsule,self.shape))
                    return self._routing(capsule)
                elif self._layer_spec[0] == 'fc':
                    self.shape = (self.batch_size, 1, 1, self.num_capsules, self.capsule_vector)
                    return self._routing(inputs)


        # except Exception as e:
        #     print('Capsule creation has failed: %s'%e)
        #     self.debug()

    # ======================================================================
    # Routing functions
    # ======================================================================
    #TODO make b_ij initializer selectable - 0.0, mean or other
    def _routing(self, inputs):
        """ Capsule routing function controller (aka routing-router). """
        if self._debug(): print('Routing capsule "%s" (shape: %s) with %s inputs '%(self.layer_name, inputs, self.routing))

        # try:
        if self._debug(): print('\n******************\nInputs raw: %s \n******************\n'%inputs)
        # if there isn't a matrix, try to use vector based agreement.
        # if self.routing_matrix_size is None:
        self.routing_size = self.batch_size*self.shape[1]*self.shape[1]

        """
        Resize the input for application of appropriate routing weights:
        [batch*(new kernel shape), 1, input capsules*(input kernel shape), m, n]
            If a matrix size is provided: m = n = matrix sizes
                Otherwise [1, -1] for on the fly vectorization
        """
        i_shape = (-1, self.routing_matrix_size, self.routing_matrix_size) if self.routing_matrix_size else (self.capnum_prior, 1, self.input_shape[1]*self.input_shape[1]*self.capvec_prior)

        inputs = tf.reshape(inputs, shape=(self.routing_size, 1,) + i_shape)
        inputs = tf.tile(inputs, [1, self.num_capsules, 1, 1, 1])
        if self._debug(): print('Capsule reshaped: %s'%inputs)

        """ Instantiate routing weight variables """
        r_shape = (self.routing_matrix_size, self.routing_matrix_size) if self.routing_matrix_size else (self.input_shape[1]*self.input_shape[1]*self.capvec_prior, self.capsule_vector)

        route_weights = tf.get_variable('routing_weight', shape=(1, self.num_capsules, self.capnum_prior,)+r_shape, dtype=tf.float32, initializer=self.routing_initializer, regularizer=self.route_regularizer)
        self.route_weights = tf.tile(route_weights, [self.routing_size, 1, 1, 1, 1])

        """ This is the desired output shape for the agreement term (aka votes)."""
        o_shape = (self.routing_size, self.num_capsules, self.capnum_prior, self.capsule_vector)

        self.u_hat = tf.reshape(tf.matmul(inputs, self.route_weights), shape=o_shape)

        if self._debug(): print('\nRouting weights: %s (shape: %s)\nu-hat: %s (shape: %s)'%(self.route_weights,r_shape, self.u_hat, o_shape))

        """ This statement calls the capsule specified router functions """
        if self.routing == 'dynamic':
            return self._dynamic_routing()
        elif self.routing == 'em':
            return self._em_routing()
        else:
            print('Routing type "%s" not recognized.'%(self.routing))
        # except Exception as e:
        #     print('Routing exception has occured: %s'%e)
        #     self.debug()

    def _dynamic_routing(self):
        """ Dynamic routing based on [1]. """
        with tf.name_scope('Dynamic_Routing'):
            b_shape=(self.routing_size, self.num_capsules, self.capnum_prior, 1)
            self.b_ij = tf.constant(0.0, name='b_ij_log_prior', shape=b_shape, dtype=tf.float32)
            frozen_u_hat = tf.stop_gradient(self.u_hat, name='stop_gradient')
            for i in range(self.routing_iterations):
                if self._debug(): print('Dynamic routing iteration %i: capsule "%s"\n\tfrozen_u_hat: %s\n\tb_ij: %s'%(i, self.layer_name, frozen_u_hat, self.b_ij))
                with tf.variable_scope('dynamic_routing_iter_{0}'.format(i)):
                    c_ij = tf.nn.softmax(self.b_ij, dim=1)
                    if i < self.routing_iterations-1:
                        s_j = tf.matmul(frozen_u_hat, c_ij, transpose_a=True)
                        v_j = squash(tf.squeeze(s_j))
                        self.b_ij += tf.reduce_sum(tf.reshape(v_j, shape=[self.routing_size, self.num_capsules, 1, -1])*frozen_u_hat, axis=-1, keep_dims=True)
                    else:
                        s_j = tf.matmul(self.u_hat, c_ij, transpose_a=True)
                        v_j = squash(tf.squeeze(s_j))
                    if self._debug():print('\tc_ij: %s\n\ts_j: %s\n\tv_j: %s\n'%(c_ij,s_j,v_j))
            return v_j


    def _em_routing(self):
        """ EM-Matrix routing based on [2].
        For internal consistency, the "ij" convention supercedes "ic". As such,
        "votes" are referred to by "u_ij" and "r_ic" as "b_ij" - the terms in [1].
        """
        with tf.variable_scope('EM_Routing'):
            activation = self.activation_unit
            b_shape = (self.routing_size, self.capnum_prior, self.num_capsules, 1)
            frozen_u_hat = tf.stop_gradient(self.u_hat, name='stop_gradient')
            self.b_ij = tf.constant(1.0/self.num_capsules, name='b_ij_log_prior-superior', shape=b_shape, dtype=tf.float32)
            routing_lambda = self.routing_lambda
            for i in range(self.routing_iterations):
                if self._debug(): print('\n************************ EM routing iteration %i: capsule "%s" ************************\nfrozen_u_hat: %s\nb_ij: %s\nactivation: %s'%(i, self.layer_name, frozen_u_hat, self.b_ij,activation))
                with tf.variable_scope('em_routing_iter_{0}'.format(i)):
                    if i < self.routing_iterations-1:
                        u_h, variance, m_activation = self._m_step(activation, frozen_u_hat, routing_lambda)
                        self.b_ij = tf.reshape(self._e_step(u_h, variance, m_activation, frozen_u_hat), shape=b_shape)
                        routing_lambda += routing_lambda*self.routing_lambda_step
                        if self._debug():print('\nb_ij: %s\nactivation: %s\nu_h: %s\nlambda:%s'%(self.b_ij, activation, u_h,routing_lambda))
                    else:
                        v_j, _, m_activation = self._m_step(activation, self.u_hat, routing_lambda)
                        if self._debug():print('\nactivation: %s\nv_j: %s\nlambda:%s'%(activation, v_j,routing_lambda))
            self.activation_unit = tf.reshape(activation, shape=(self.shape[:-2] + [-1, 1]))
            print(self.activation_unit)
            return tf.reshape(v_j, shape=(self.shape[:-2] + [-1, self.routing_matrix_size**2]))

    def _m_step(self,activation, u_hat, routing_lambda):
        """ M-step for the EM-Matrix routing technique. """
        with tf.variable_scope('M-Step') as scope:
            if self._debug():print('\n************ M-Step ************\nactivation: %s'%(activation))
            # Step 1
            activation = tf.reshape(activation, shape=(self.routing_size,-1,1,1))

            self.b_ij = self.b_ij * activation
            if self._debug(): print('b_ij: %s\nactivation: %s'%(self.b_ij,activation))
            self.b_ij = self.b_ij / (tf.reduce_sum(self.b_ij, axis=1, keep_dims=True)+cfg.epsilon)
            if self._debug(): print('b_ij: %s'%(self.b_ij))
            self.b_ij = tf.reshape(self.b_ij, shape=(self.routing_size,self.num_capsules,-1,1))
            # Step 2
            b_i_sum = tf.reduce_sum(self.b_ij, axis=2, keep_dims=True)
            b_i = tf.reshape(self.b_ij / (b_i_sum + cfg.epsilon), shape=(self.routing_size, self.num_capsules,-1,1))
            u_h = tf.reduce_sum(u_hat*b_i, axis=2, keep_dims=True)

            # Step 3
            variance = tf.reduce_sum(tf.square(u_hat-u_h)*b_i, axis=2, keep_dims=False) + cfg.epsilon

            if self._debug():print('\nb_i: %s\nb_i_sum: %s\nu_h: %s\nvar: %s'%(b_i, b_i_sum, u_h, variance))

            # Step 4
            beta_v = tf.get_variable('beta_v', shape=[self.num_capsules, self.capsule_vector], dtype=tf.float32, initializer=self.routing_beta_v_initializer, regularizer=self.routing_regularizer)

            if self._debug():print('Beta v - %s\nvariance: %s\nstd: %s'%(beta_v,variance, tf.log(tf.sqrt(variance))))

            cost_h = (beta_v + tf.log(tf.sqrt(variance))) * tf.reshape(b_i_sum, shape=(self.routing_size, self.num_capsules, -1))

            # Step 5
            beta_a = tf.get_variable('beta_a', shape=[self.num_capsules], dtype=tf.float32, initializer = self.routing_beta_a_initializer, regularizer=self.routing_regularizer)

            if self._debug():print('cost_h: %s\nBeta A: %s'%(cost_h, beta_a))

            activation = tf.nn.sigmoid(routing_lambda * (beta_a - tf.reduce_sum(cost_h, axis=-1, keep_dims=False)))

            if self._debug():print('activation: %s'%(activation))
        return u_h, variance, activation

    def _e_step(self, u_h, variance, activation, u_ij):
        """ E-step for the EM-Matrix routing technique. """
        with tf.variable_scope('E-Step'):
            if self._debug():print('\n************ E-Step ************')
            # Step 1
            variance = tf.reshape(variance, shape=(self.routing_size, self.num_capsules,1,-1))

            log_p_j = -tf.log(tf.sqrt(variance)) - (tf.square(u_ij - u_h) / (2 * variance))
            if self._debug():print('log_p_j: %s\nu_ij: %s\nu_h: %s\n'%(log_p_j, u_ij, u_h))
            log_p_j = log_p_j - (tf.reduce_max(log_p_j, axis=[1,3], keep_dims=True)-tf.log(10.0))
            p_j = tf.exp(tf.reduce_sum(log_p_j, axis=3, keep_dims=True))
            if self._debug():print('log_p_j(reduced): %s\np_j: %s\nactivation: %s'%(log_p_j, p_j, activation))
            # Alternatively:
            # p_j = tf.exp(-(tf.reduce_sum(tf.square(u_ij - u_h) / (2 * variance), axis=-1, keep_dims=True))) / (tf.reduce_prod(tf.sqrt(2 * variance), axis=2, keep_dims=True) + cfg.epsilon)
            # if self._debug():print('p_j: %s\nactivation: %s'%(p_j, activation))
            # Step 2
            activation = tf.reshape(activation, shape=(self.routing_size, self.num_capsules,1,1))
            b_j = tf.nn.softmax(activation * p_j, dim=1)
            if self._debug():print('\nb_j: %s\nactivation: %s'%(b_j, activation))
        return b_j

    # ==========================================================================
    # Helper functions
    # ==========================================================================
    # Convolution capsule
    # --------------------------------------------------------------------------
    def _set_activation_unit(self, inputs):
        """
        """
        if self.routing_activation_unit == 'logistic':
            self.activation_unit = tf.expand_dims(tf.layers.conv2d(inputs, self.num_capsules,
                                          kernel_size=self.kernel_size,
                                          strides=self.strides,
                                          activation=tf.nn.sigmoid,
                                          activity_regularizer=self.kernel_regularizer), axis=-1)
        # elif self.routing_activation_unit == 'euclidean':
        #     self.activation_unit = tf.sqrt(tf.reduce_sum(tf.square(inputs), axis=2, keepdims=True) + cfg.epsilon)
        else:
            self.activation_unit = None
        if self._debug(): print('Activation unit set to %s: %s'%(self.routing_activation_unit,self.activation_unit)) if self.activation_unit is not None else None

    # ==========================================================================
    # Utility functions
    # --------------------------------------------------------------------------
    def _logging(self, detail='info'):
        """ Prints infornation to tf.logging
        """
        if hvd.rank() == 0:
            if detail == 'info':
                tf.logging.info('{} (capsule type:{})\n\tshape: {}\n\trouting: {}'.format(self.layer_name, self._layer_spec, self.shape, self.routing))
            elif detail == 'debug':
                tf.logging.info('%s'%(('%s: %s'%(k,v) for k,v in self.__dict__.items())))

# ==============================================================================
#   Functions
# ==============================================================================
def _conv_capsule(inputs, activations = None, kernel_size=3, strides=1, data_format='NCHW', padding='VALID', rate=None, name=None, debug=False):
    """ Creates a convolution capsule kernel from inputs using depthwise convolutions.
    Args:
        inputs (tensor): Input of shape NHWC or NCWH
        kernel_size (int):
        strides (int):
    Returns:
        conv_filters (tensor):
    """
    channel_axis = 1 if 'NC' in cfg.data_format else -1

    if activations is not None:
        inputs = tf.concat([inputs, activations], axis=channel_axis)

    input_shape = inputs.get_shape().as_list()

    conv_shape, strides, kernel_shape, rate = var_prep(inputs, strides, kernel_size, kernel_size**2, data_format, rate)
    #TODO add dtype controller
    filters = np.zeros(shape=kernel_shape, dtype=np.float32)

    inputs = tf.reshape(inputs, shape=conv_shape)

    for i in range(kernel_size):
        for j in range(kernel_size):
            filters[i, j, :, i * kernel_size + j] = 1.0

    filter_op = tf.constant(filters, dtype=tf.float32)

    conv_filters = tf.nn.depthwise_conv2d(inputs, filter_op, strides=strides, padding=padding, data_format=data_format, rate=rate, name=name)

    output_shape = conv_filters.get_shape().as_list()

    _shape=insert2list(insert2list(output_shape[0:3], -1, channel_axis), input_shape[-1], -1)

    conv_filters = tf.reshape(conv_filters, shape=_shape)

    if debug: print('\nInput shape: %s\nConv shape: %s\nFilter shape: %s\nCapsule shape: %s\n'%(input_shape,conv_shape,output_shape,conv_filters.get_shape().as_list()))
    return conv_filters if activations is None else (conv_filters[...,:-1], conv_filters[...,-1])
