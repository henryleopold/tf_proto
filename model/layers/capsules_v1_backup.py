#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (c) 2017 Henry Leopold. Unreleased.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Capsule class declarations and related functions """
__author__ = 'Henry A. Leopold'

from model.components import squash
import numpy as np
import tensorflow as tf
from model.layers import Layer, Layers
from model.layers.core import conv_layer, fc_layer
from config import cfg

# __all__ = [ # ===========================
#             # Capsule Classes
#             # ---------------------------
#                 'Capsule'
#             # ---------------------------
#
# ]

# ==============================================================================
# Capsule Classes
# ------------------------------------------------------------------------------
class Capsule(Layer):
    def __call__(self, inputs):
        # inputs = ops.convert_to_tensor(inputs, dtype=self.dtype)
        # self.input_shape = inputs.get_shape().as_list()
        # check if each input is a capsule and route accordingly.
        if not self.built:
            self.tensor = self._make_capsule(inputs)
            if self.routing is not None:
                self.tensor = self._routing()
            print('Compiled %s: %s'%(self.layer_name,self.tensor))
            self.built = True
        return self.tensor

    def _make_capsule(self, inputs):
        if self._debug(): print('Making capsule: %s\nInputs: %s'%(self.layer_name,inputs))
        if self._debug(): print('Input specs: %s'%(self.input_specs))
        self._layer_spec = self._layer_spec[1:]
        self.input_shape = inputs.get_shape().as_list()
        self.batch_size = self.input_shape[0]
        self.kernel_prior1 = self.input_shape[1]
        self.kernel_prior2 = self.input_shape[2]
        self.channel_axis = 1 if 'NC' in self.data_format else 3
        self.input_channels = self.input_shape[self.channel_axis]

        try:
            self.capvec_prior = self.input_specs.capsule_vector
            self.capnum_prior = self.input_specs.num_capsules
        except Exception as e:
            print('Input does not appear to be a capsule, so routing is disabled: %s'%e)
            self.routing = None


        # try:
        if self.routing is None: #self.layer_name == 'PrimaryCaps':
            self.filters = self.num_capsules * self.capsule_vector
            capsule = self._make_layer(inputs)
            self.shape = capsule.get_shape().as_list()
            if self._debug(): print('New capsule: %s\nshape: %s'%(capsule,self.shape))
            # capsule = tf.reshape(capsule, shape=(-1, _shape[1], _shape[2], self.num_capsules,
            # self.capsule_vector))

        else:
            if 'conv' in self.build_code:
                self.filters_prior = self.capnum_prior * self.capvec_prior
                self.filters = self.num_capsules * self.capsule_vector
                # self.filters_prior = self.input_specs.filters
                # num_inputs = self.kernel_size * self.kernel_size * self.input_channels
                # self.batch_shape = [self.batch_size, self.kernel_size, self.kernel_size, self.input_channels]
                # _shape = (1, num_inputs, self.filters) + tuple([self.filters_prior,self.num_capsules])
                #
                # T_matrix = tf.get_variable("transformation_matrix", shape=T_shape, regularizer=regularizer)
                # T_matrix_batched = tf.tile(T_matrix, [batch_size, 1, 1, 1, 1])
                # capsule = tf.reshape(inputs, shape=(-1, self.filters_prior, 1, self.capvec_prior, 1))
                # if self._debug(): print('\n******************\nInputs: ',inputs)
                # _shape = (self.batch_size, self.kernel_prior1, self.kernel_prior2, -1)
                # capsule = tf.reshape(inputs, shape=_shape)

                # capsule = self._make_layer(inputs)
                # if self._debug(): print('\n******************\nCapsule raw: ',capsule)
                # self.shape = capsule.get_shape().as_list()
                # self.route_shape = (-1, self.filters_prior, 1, self.capvec_prior, 1)
                # capsule = tf.reshape(capsule, shape=self.route_shape)
                # if self._debug(): print('\n******************\nReshaped: ',capsule)
                # # capsule = tf.expand_dims(capsule, axis=2)
                # capsule = tf.tile(capsule, [1, 1, self.num_capsules, 1, 1])
                # if self._debug(): print('\n******************\nCapsule Tiled: ',capsule)

            elif 'fc' in self.build_code:
                self.filters_prior = self.capnum_prior * self.kernel_prior1 * self.kernel_prior2
                self.route_shape = (-1, self.filters_prior, 1, self.capvec_prior, 1)
                with tf.variable_scope(self.layer_name) as scope:
                    if self._debug(): print('\n******************\nCapsule raw: ',inputs)
                    capsule = tf.reshape(inputs, shape=self.route_shape)
                    if self._debug(): print('Capsule reshaped: ',capsule)
                    capsule = tf.tile(capsule, [1, 1, self.num_capsules, 1, 1])
                    if self._debug(): print('Capsule tiled (Unrouted): %s'%(capsule))

                    self.shape = capsule.get_shape().as_list()

        return capsule

        # except Exception as e:
        #     print('Capsule creation has failed: %s'%e)
        #     self.debug()

# ======================================================================
# Routing functions
# ======================================================================

    def _routing(self):
        """ Capsule routing function controller (aka routing-router). """
        if self._debug(): print('routing capsule "%s" (shape: %s) with %s  '%(self.layer_name, self.tensor, self.routing))
        # try:
        with tf.name_scope('Routing'):
            _shape = (self.filters_prior, self.num_capsules, self.capsule_vector, self.capvec_prior)

            self.route_weights = tf.get_variable('routing_weight', shape=_shape, dtype=tf.float32, initializer=self.route_initializer)
            if self._debug(): print('\nrouting shape: %s\n%s'%(self.route_weights, self.tensor))

            self.u_hat = tf.einsum('abdc,iabcf->iabdf', self.route_weights, self.tensor)# TODO FIX u_hat (main breaking point in routing)
            self.b_ij = tf.get_variable('b_ij_log_prior', shape=[1, self.filters_prior, self.num_capsules, 1, 1], dtype=tf.float32, initializer=tf.zeros_initializer)

            if self.routing == 'dynamic':
                return self._dynamic_routing()
            else:
                print('Routing type "%s" not recognized.'%(self.routing))
        # except Exception as e:
        #     print('Routing exception has occured: %s'%e)
        #     self.debug()


    def _dynamic_routing(self):
        for i in range(self.routing_iterations):
            if self._debug(): print('Dynamic routing iteration %i: capsule "%s" (shape: %s)'%(i, self.layer_name, self._capsule))
            with tf.variable_scope('dynamic_routing_{0}'.format(i)):
                c_ij = tf.nn.softmax(self.b_ij, dim=2)
                if self._debug():print('c_ij: %s'%(c_ij))
                s_j = tf.multiply(c_ij, self.u_hat)
                if self._debug():print('s_j: %s'%(s_j))
                s_j = tf.reduce_sum(s_j, axis=1, keep_dims=True)
                if self._debug():print('s_j - reduce sum: %s'%(s_j))
                v_j = squash(s_j)
                if self._debug():print('v_j: %s'%(v_j))
                v_j_tiled = tf.tile(v_j, [1, self.filters_prior, 1, 1, 1])
                if self._debug():print('v_j - tiled: %s'%(v_j))
                u_dot_v = tf.matmul(self.u_hat, v_j_tiled, transpose_a=True)
                if self._debug():print('u_dot_v: %s'%(u_dot_v))
                self.b_ij += tf.reduce_sum(u_dot_v, axis=0, keep_dims=True)
                if self._debug():print('b_ij: %s'%(self.b_ij))
        return tf.squeeze(v_j, axis=1)




# ======================================================================
# Builder helper functions
# ======================================================================
    def _conv_capsule(self, inputs):
            capsule = conv_layer(inputs,
                                filters=self.filters,
                                kernel_size=self.kernel_size,
                                strides=self.strides,
                                padding=self.padding,
                                dilation_rate=self.dilation_rate,
                                activation=self.activation,
                                use_bias=self.use_bias,
                                kernel_initializer=self.kernel_initializer,
                                bias_initializer=self.bias_initializer,
                                kernel_regularizer=self.kernel_regularizer,
                                bias_regularizer=self.bias_regularizer,
                                activity_regularizer=self.activity_regularizer,
                                kernel_constraint=self.kernel_constraint,
                                bias_constraint=self.bias_constraint,
                                trainable=self.trainable,
                                data_format=cfg.data_format,
                                name=self.layer_name,
                                reuse=self.reuse)
            return self._activation_regularization(capsule)

    def _fc_capsule(self, inputs):
        capsule = tf.layers.dense(inputs,
                            units=self.units,
                            activation=self.activation,
                            use_bias=self.use_bias,
                            kernel_initializer=self.kernel_initializer,
                            bias_initializer=self.bias_initializer,
                            kernel_regularizer=self.kernel_regularizer,
                            bias_regularizer=self.bias_regularizer,
                            activity_regularizer=self.activity_regularizer,
                            kernel_constraint=self.kernel_constraint,
                            bias_constraint=self.bias_constraint,
                            trainable=self.trainable,
                            flatten=self.flatten,
                            name=self.layer_name,
                            data_format=cfg.data_format,
                            reuse=self.reuse)
        return self._activation_regularization(capsule)
