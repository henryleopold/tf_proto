#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (c) 2017 Henry Leopold. Unreleased.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Base model class and related functions """
__author__ = 'Henry A. Leopold'
import os
import tensorflow as tf
layers = tf.contrib.layers
from config import cfg
import horovod.tensorflow as hvd
from utils import make_checkpoint_path, build_path


class Model(object):
    """ Base model class
    Args:

    Returns:
    """
    def __init__(self, inputs, labels, is_training=True, save_path=None):
        self._summaries = [] # private list of summaries.
        self._inputs(inputs,labels)
        self._initialize(save_path)
        self.training = tf.placeholder_with_default(True, None, name='training') #TODO enable training placeholder

        self._build_arch()
        if is_training:
            self.global_step = tf.Variable(0, name='global_step', trainable=False)
            with tf.name_scope('Train'):
                with tf.name_scope('Loss'):
                    self._loss()
                with tf.name_scope('Predict'):
                    self._predict()
                with tf.name_scope('KPIs'):
                    self._performance()
                with tf.name_scope('Optimizer'):
                    self._optimizer()
                    self._train()

        # Wrapper for adding summaries to checkpoints/tensorboard
        self._summarize()

    # ==========================================================================
    # Builder helper functions. These are to be modified in children
    # --------------------------------------------------------------------------
    #TODO Should placeholders be generated in here?
    #TODO make smart
    def _inputs(self, inputs, labels):
        with tf.name_scope('Inputs') as scope:
            self.inputs, self.labels = inputs,labels# tf.cast(labels, tf.int64)
            self.one_hot = tf.one_hot(self.labels, depth=self.num_classes, axis=1, dtype=tf.float32)
            self._summaries.append(tf.summary.image('input', self.inputs, max_outputs=3))

    # _initialize function in utilities; modifacation not recommended.

    def _build_arch(self):
        with tf.variable_scope('Model'):
            with tf.variable_scope('conv_layer1'):
                h_conv1 = layers.conv2d(
                    self.inputs, 32, kernel_size=[5, 5], activation_fn=tf.nn.relu)
                h_pool1 = tf.nn.max_pool(
                    h_conv1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

            # Second conv layer will compute 64 features for each 5x5 patch.
            with tf.variable_scope('conv_layer2'):
                h_conv2 = layers.conv2d(
                    h_pool1, 64, kernel_size=[5, 5], activation_fn=tf.nn.relu)
                h_pool2 = tf.nn.max_pool(
                    h_conv2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
                # reshape tensor into a batch of vectors
                h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])

            # Densely connected layer with 1024 neurons.
            h_fc1 = layers.dropout(
                layers.fully_connected(
                    h_pool2_flat, 1024, activation_fn=tf.nn.relu),
                keep_prob=0.5,
                is_training=self.training)

            # Compute logits
            self.logits = layers.fully_connected(h_fc1, self.num_classes, activation_fn=None)

    def _predict(self):
        self.prediction = tf.to_int32(tf.argmax(self.logits, 1))
        # self.prediction = tf.reshape(self.prediction, shape=(cfg.batch_size, )),

    def _loss(self):
        self.loss = tf.losses.softmax_cross_entropy(self.one_hot, self.logits)
        self._summaries.append(tf.summary.scalar('loss', self.loss))

    # TODO parameterize learning rate and optimization
    def _optimizer(self):
        # Horovod: adjust learning rate based on number of GPUs.
        optimizer = tf.train.AdamOptimizer(self.learning_rate * hvd.size(), self.adam_beta1, self.adam_beta2, self.adam_eps)
        self._summaries.append(tf.summary.scalar('learning_rate', self.learning_rate))

        # Horovod: add Horovod Distributed Optimizer.
        self.optimizer = hvd.DistributedOptimizer(optimizer)

    def _train(self):
        gradients = self.optimizer.compute_gradients(self.loss)
        tf.summary.scalar('opt_gradients', tf.global_norm(gradients))
        self.train_op = self.optimizer.apply_gradients(gradients,
                                                        global_step=self.global_step)
        # self.train_op = self.optimizer.minimize(self.loss, global_step=self.global_step)

    def _performance(self):
        #TODO kpis
        correct_prediction = tf.equal(self.prediction, self.labels)
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        self._summaries.append(tf.summary.scalar('accuracy', self.accuracy))

    def _summarize(self):
        """ Wrapper for adding summaries to checkpoints/tensorboard """
        if len(self._summaries) >= 1:
            self.summaries = tf.summary.merge(self._summaries)

    # --------------------------------------------------------------------------
    #
    # --------------------------------------------------------------------------
    def _initialize(self, save_path=None):
        """ Aesthetic function that initializes directory and related variables. """
        print('\n**************** Initializing... *******************') if hvd.rank() == 0 else None
        # Output folder
        if save_path is None:
            _sp = build_path([
#                 os.path.dirname(os.path.abspath(__file__)), self.log_dir, model_name])
                cfg.log_dir, cfg.model])
            self.save_path = make_checkpoint_path(_sp, [cfg.exp_id, cfg.subexp], build_as_dir=False)
        else:
            self.save_path = save_path
        self.ckpt_dir = os.path.join(self.save_path, "checkpoints")
        self.current_dir = os.path.dirname(os.path.realpath(__file__))
        self._learning_rate()
        self.checkArgs() if hvd.rank() == 0 else None


    # TODO make lr class factory instead?
    def _learning_rate(self):
        if self.learning_rate_decay_type == 'exponential':
            self.learning_rate = tf.train.exponential_decay(self.learning_rate, self.global_step,
                                                       cfg.learning_rate_decay_steps, cfg.learning_rate_decay_factor,
                                                       staircase=cfg.learning_rate_staircase)
        elif self.learning_rate_decay_type == 'polynomial':
            self.learning_rate = tf.train.polynomial_decay(self.learning_rate, self.global_step,
                                                       cfg.learning_rate_decay_steps, end_learning_rate=cfg.learning_rate_end,
                                                       power=cfg.learning_rate_poly_power,
                                                       cycle=cfg.learning_rate_poly_cycle)
        #TODO elif self.learning_rate_decay_type == 'moving_average':

        else:
            self.learning_rate

    # --------------------------------------------------------------------------
    # Summary Functions
    # --------------------------------------------------------------------------
    def _activation_summary(self, x):
        """Helper to create summaries for activations.
        Creates a summary that provides a histogram of activations.
        Creates a summary that measure the sparsity of activations.
        Args:
          x: Tensor
        Returns:
          nothing
        """
        tensor_name = x.op.name
        self._summaries.append(tf.summary.histogram(tensor_name + '/activations', x))
        self._summaries.append(tf.summary.scalar(tensor_name + '/sparsity', tf.nn.zero_fraction(x)))

    def _image_summary(self, image, name='image', NCHW=True):
        """Helper to create summaries of input images.
        Args:
          image: Tensor
          name: Name for the summary
        Returns:
          nothing
        """
        # if the shape was NCHW, we need to set it back to NHWC before summary
        if self.data_format is 'NCHW' and NCHW:
            image = tf.transpose(image, [0, 2, 3, 1])
        self._summaries.append(tf.summary.image(name, image))
    # ==========================================================================
    # Utility functions. Modification not suggested.
    # --------------------------------------------------------------------------
    # getattr
    # --------------------------------------------------------------------------
    def __getattr__(self, item):
        # try:
        #     item = cfg.__dict__['__flags'][item]
        # except KeyError:
        #     # if item == 'learning_rate': return 'valid'
        #     # elif any(item == x for x in ('strides', 'dilation_rate', 'routing_iterations')): return 1
        #     # else: raise AttributeError(name)
        #     raise AttributeError(item)
        # return item

        if any(item == x for x in ('num_classes', 'batch_size', 'hparams', 'hparams_dir', 'model', 'model_dir')): return cfg.__dict__['__flags'][item]
        elif any(x in item for x in ('loss_', 'learning_rate', 'adam_')): return cfg.__dict__['__flags'][item]
    # --------------------------------------------------------------------------
    def _debug(self):
        """ Internal checker to for debugging.
        Prevents replication for running on horovod.
        """
        return all([cfg.debug, hvd.rank() == 0])

    def debug(self):
        print('\n****************************************************')
        print('**************** Model Debugging *****************')
        print('****************************************************\n')
        for k,v in self.__dict__.items():
            print('%s: %s'%(k,v))
        print('GPU:%s of %s'%(hvd.rank()+1, hvd.size()))
        print('****************************************************\n')

    def checkArgs(self):
        print('\n****************************************************')
        print('**************** Model Initialized *****************')
        print('****************************************************\n')
        if cfg.execute == 'test':
            print('The model is set to Testing')
    #         checkpoint_dir = build_path([cfg.exp_id, cfg.model, 'train/rgb', cfg.subexp])
    #         print("Test dir: %s"%)
    #         print("DATA dir: %s"%datasets[cfg.data_set][cfg.execute])
        elif cfg.execute == 'finetune':
            print('The model is set to Finetune from ckpt')
            # print("Data dir: %s"%train_data_dir)
            # print("Validation dir: %s"%test_data_dir)
        elif cfg.execute == 'data_gen':
            print('The model is set to Finetune from ckpt')
            # print("Data dir: %s"%train_data_dir)
        elif cfg.execute == 'debug':
            print('The model is set to Debugging')
            print("Debugging Iterations: %d"%cfg.debug_steps)
            print("Initial lr: %f"%self.learning_rate)
        else:
            print('The model is set to Training')
            print("Max training Iteration: %d"%cfg.max_steps)
            print("Initial lr: %f"%self.learning_rate)
            # print("Data dir: %s"%train_data_dir)
            # print("Validation dir: %s"%test_data_dir)

        # print("Batch Size: %d"%cfg.batch_size)
        print("Checkpoint dir: %s"%self.ckpt_dir)
        print("Log dir: %s"%cfg.log_dir)
        print("GPUs: %d"%hvd.size())
        print('\n****************************************************\n')
