#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (c) 2017 Henry Leopold. Unreleased.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Model class that generates its graph from json files of hyperparameters. """
__author__ = 'Henry A. Leopold'
import os
from collections import OrderedDict
import tensorflow as tf
from config import cfg
from .base import Model
from model import Hyperparameters, Layers, Layer, Capsule, set_initializer, set_activation, set_regularizer
from utils import read_json
import horovod.tensorflow as hvd

# ==========================================================================
# Base Hyperparameter-Model Class
# ==========================================================================

class HpModel(Model):
    def __init__(self, inputs, labels, hparams_dir=None, hparams_file=None, is_training=True, save_path=None):

        # ----------------------------------------------------------------------
        # Set hparams path
        try: hparams_path = os.path.join(hparams_dir, hparams_file)
        except TypeError: hparams_path = os.path.join(cfg.hparams_dir, cfg.hparams+'.json')
        # ----------------------------------------------------------------------
        # Load hparams content
        hyperparameters = read_json(hparams_path, OrderedDict)

        # ----------------------------------------------------------------------
        # Set hparams
        self.hprms = Hyperparameters()
        self.hprms.build(hyperparameters)

        for hparam in self.hprms.model._fields:
            value = getattr(self.hprms.model, hparam)
            if "initializer" in hparam: value = set_initializer(*value.split('_'))
            if "regularizer" in hparam: value = set_regularizer(*value.split('_'))
            if hparam == "activation": value = set_activation(value)
            if value == "num_classes": value = cfg.num_classes
            setattr(self, hparam, value)

        # ----------------------------------------------------------------------
        # Instantiate layers and set other layer variables
        # ----------------------------------------------------------------------
        # TODO update layer and model generators for modules/blocks
        self._layers = Layers()
        self._layer_specs = Layers()

        super().__init__(inputs, labels, is_training, save_path)
    # ======================================================================
    # Initialization and function seperator (for legibility).
    # ======================================================================
    # Builder helper functions
    # ----------------------------------------------------------------------
    def _build_arch(self):
        """ Build the model """
        inputs = self.inputs
        last_layer = None
        with tf.variable_scope('Model') as scope:
            for l in range(len(self.hprms.layer_params)):
                ln, _layer = self.hprms.layer_params[l]
                self._layers[ln], self._layer_specs[ln] = self._build_layer(inputs, _layer, name=ln, last_layer=last_layer)
                inputs = self._layers[ln]
                last_layer = self._layer_specs[ln]
                if self._debug(): print('Compiled Layer: %s\nNew Inputs: %s'%(self._layers[ln],inputs))
            self._post_build()
            if self._debug():
                print('\n************ Network Debug *************')
                for l in self._layers: print('%s'%self._layers[l])
                print('GPU:%s of %s'%(hvd.rank()+1, hvd.size()))
                print('******************************************\n')


    def _build_layer(self, inputs, layer_params, name, last_layer):
        """ Helper function for compiling layers """
        # with tf.variable_scope(name) as scope:
        if self._debug(): print('\n******************** Making Layer/Cap: %s *********************\n'%name, layer_params)
        if layer_params.build_code.split('_')[0] == 'capsule':
            _capsule = Capsule(name, self.training, last_layer, **layer_params._asdict())
            return _capsule(inputs), _capsule
        else:
            _layer = Layer(name, self.training, last_layer, **layer_params._asdict())
            return _layer(inputs), _layer

    def _post_build(self):
        """ Post build inheritance function """
        self.logits = self._layers.last()
    # ======================================================================
    # ... functions
    # ----------------------------------------------------------------------


    # ======================================================================
    # Utility functions
    # ----------------------------------------------------------------------
    def layers(self, do_print=False):
        """Returns layer names for the entire model"""
        if do_print: print(list(self._layers.keys()))
        return self._layers.keys()
