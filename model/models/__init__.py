from .core import *
from .capsnet import *


# TODO Convert into model factory
models_dict = {
    'Model': Model,
    'HpModel': HpModel,
    'CapsNet': CapsNet
}

def models(model=cfg.model):
    print('Loading "%s"'%model)
    return models_dict[model]
