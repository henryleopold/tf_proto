#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ==============================================================================
# Copyright (c) 2017 Henry Leopold. Unreleased.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Capsule network model classes and related functions """
__author__ = 'Henry A. Leopold'
from .core import Model, HpModel

# TODO get references

class CapsNet(Model):
    """ Base capsule network class

    Implements dynamic routing as described in [1]_.
    Based on [2]_.

    Args:


    References:
    .. [1]  Dynamic Routing with Capsules


    """
    def _decoder(self):
        with tf.variable_scope('decoder'):
            pass

    def _reconstruct(self):
        pass


    def _masking(self):
        pass


    def _loss(self):
        pass

    def _performance(self):
        pass
