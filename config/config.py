# Copyright (c) 2017 Henry Leopold. Unreleased.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Experiment Configuration Flags and Dictionaries"""
from __future__ import print_function, division, absolute_import
__author__ = 'Henry Leopold'
import tensorflow as tf



####################################################################################
# Table of Contents
####################################################################################
##### Experiment Flags #####
#
###########################
##### Network Flags #####
#
###########################
##### Fine-tuning Flags #####
#
###########################
##### Testing Flags #####
#
###########################
##### Preformance Metric Flags - Default returns SN/SP/Acc/AUC #####
#
###########################
##### Logging/debug Flags #####
#
###########################
##### Dictionaries #####
#
#########################################
########## See conf/data.py #############
##### Data Flags #####
# >>> Data Attributes
# >>> Directories and Files
#
##### Preprocessing Flags #####
# >>> Colour Space Enhancement
# >>> Image (data) Manipulation
#
########## See conf/cluster.py #############
##### Session Config Flags #####
# >>> Config Flags
# >>> Data -> GPU allocation flags
# >>> Cluster Specification Flags
#
########## See conf/train.py #############
##### Training Flags #####
# >>> Batches / Queues
# >>> Cross Validation Parameters
# >>> Batch Normalization Flags
# >>> Optimization Flags
# >>> Learning Rate Flags
# >>> Validation Flags
# >>> Loss Flags


# ==============================================================================
# Experiment Flags
# ==============================================================================
tf.app.flags.DEFINE_string('exp_id', 'test','Experiment Identifier.')
tf.app.flags.DEFINE_string('subexp', 'exp1','Experiment Identifier.')
tf.app.flags.DEFINE_string('execute','train','Flag for controlling data generation (data_gen), training (train) or testing (test).')
tf.app.flags.DEFINE_bool('clean', True, 'False will resume previous checkpoints.')
tf.app.flags.DEFINE_bool('use_fp16', False, 'Controls use of float32 vs float16 data types.')
tf.app.flags.DEFINE_string('load_ckpt', None,'Specific checkpoint to load when initiating an operation.')
# ==============================================================================
# Network Flags
# ==============================================================================
tf.app.flags.DEFINE_string('model_dir', 'model/models', 'Base folder path to model classes.')
tf.app.flags.DEFINE_string('model', 'CapsNet', 'Identifier for which model to build with.')
# ----------------------------------------------------------------------
# Hyperparameters
# ----------------------------------------------------------------------
tf.app.flags.DEFINE_string('hparams', None, 'Identifier for which model hyperparameter file to load.')
tf.app.flags.DEFINE_string('hparams_dir', 'model/hparams', 'Base folder path to model hyperparameter files.')
# ----------------------------------------------------------------------
# Model / architectures # TODO Upadate configs
# ----------------------------------------------------------------------
tf.app.flags.DEFINE_string('output_type', 'mask', 'Network output type. Presently only masks.')
tf.app.flags.DEFINE_integer('num_resblocks', 3, 'Encoder/decoder sizes.')
tf.app.flags.DEFINE_integer('num_resnet_layers', 3, 'Number of layers within each downward stream block.')
tf.app.flags.DEFINE_string('nonlinearity', 'concat_elu', 'Stream block nonlinearity. ["concat_elu", "elu", "concat_relu", "relu"]')
tf.app.flags.DEFINE_float('keep_prob', 0.6, 'Keep probability for dropout layers.')
tf.app.flags.DEFINE_integer('num_filters', 16, 'Number of filters in the first layer.')
tf.app.flags.DEFINE_float('epsilon', 1e-8, 'global divisor epsilon')
tf.app.flags.DEFINE_float('routing_lambda', 0.3, 'Initial value for routing temperature hyperparameter.')
tf.app.flags.DEFINE_float('routing_lambda_step', 0.3, 'Iteration based temperature increment.')
# ==============================================================================
# Fine-tuning Flags
# ==============================================================================
tf.app.flags.DEFINE_string('checkpoint_exclude_scopes', None,'Comma-separated list of scopes of variables to exclude when restoring from a checkpoint. ex: vgg_19/fc8/biases,vgg_19/fc8/weights')
tf.app.flags.DEFINE_string('trainable_scopes', None, 'Comma-separated list of scopes to filter the set of variables to train. By default, None would train all the variables.')
tf.app.flags.DEFINE_boolean('ignore_missing_vars', False,'When restoring a checkpoint would ignore missing variables.')

# ==============================================================================
# Testing Flags
# ==============================================================================
tf.app.flags.DEFINE_string('eval_dir', 'test','Data sub-folder for testing tasks')
tf.app.flags.DEFINE_string('test_id', 'drive_test','Test Identifier.')
tf.app.flags.DEFINE_bool('save_image', False, 'Save the output of a test operation as an image.')
tf.app.flags.DEFINE_bool('save_results', True, 'Save the performance output of a test operation in a pickle.')
# tf.app.flags.DEFINE_bool('use_thresh', False, 'Apply threshold during post processing.')
tf.app.flags.DEFINE_float('postproc_thresh', 0.5, 'Threshold to apply to network predictions prior performance eval.')

# ==============================================================================
# Preformance Metric Flags - Default returns SN/SP/Acc/AUC
# ==============================================================================
tf.app.flags.DEFINE_bool('kappa', True, 'Find kappa metric.')
tf.app.flags.DEFINE_bool('mcc', True, 'Find Matthew\'s Correlation Coefficient.')
tf.app.flags.DEFINE_bool('gmean', True, 'Find G-mean.')
tf.app.flags.DEFINE_bool('fscore', True, 'Find F-score.')
tf.app.flags.DEFINE_float('fscore_beta', 1.0, 'Beta value for F-score; default is 1 for F1-score.')
tf.app.flags.DEFINE_bool('precision', True, 'Find precision metric.')
tf.app.flags.DEFINE_bool('dice', False, 'Find Dice Coefficient metric.')

# ==============================================================================
# Logging/Debug Flags
# ==============================================================================
tf.app.flags.DEFINE_string('log_dir', 'output', 'Base directory for saving logs and checkpoints.') #TODO revise checkpoint functions
tf.app.flags.DEFINE_integer('log_frequency', 100, 'How often to log results to the console.')
tf.app.flags.DEFINE_integer('cp_frequency', 1000, 'How often to save checkpoints.')
tf.app.flags.DEFINE_string('log_level', 'INFO', 'Set tensorflow logging level: "INFO", "DEBUG", "ERROR", "FATAL", "WARN".') #TODO make Wrapper
# ----------------------------------------------------------------------
# Debug Flags
# ----------------------------------------------------------------------
tf.app.flags.DEFINE_bool('debug', False, 'If true, functions produce debug messages where applicable.')
tf.app.flags.DEFINE_boolean('view_images',False,'If true, images are shown as a windowed output.')
tf.app.flags.DEFINE_integer('debug_steps', 360, 'How often to save checkpoints.')

# ==============================================================================
# Dictionaries
# ==============================================================================
datasets = {
    'drive':{
        'train':'../../data/fundus/DRIVE/repak/train/',
        'test':'../../data/fundus/DRIVE/repak/test/',
        'model_base':'checkpoints/drive_rgb/fragsnet/train/rgb/rgb',
        'source':'../../data/fundus/DRIVE/',
        'minval': 0.21,
        'minval2': 0.3,
        'ksize':9
    },
    'stare':{
        'train':'../../data/fundus/STARE/repak/train/',
        'test':'../../data/fundus/STARE/repak/test/',
        'source':'../../data/fundus/STARE/',
        'xval':'data/stare_rgb/',
        'model_base':'checkpoints/stare_rgb/fragsnet/train/rgb/xval/rgb/xval_',
        'minval': 0.28,
        'minval2': 0.1,
        'ksize':7
    },
    'chase':{
        'train':'../../data/fundus/CHASEDB1/repak/train/',
        'test':'../../data/fundus/CHASEDB1/repak/test/',
        'xval':'data/chase_rgb/',
        'source':'../../data/fundus/CHASEDB1/',
        'model_base':'checkpoints/chase_rgb/fragsnet/train/rgb/xval/rgb/xval_',
        'minval': 0.0525,
        'minval2': 0.50,
        'ksize':13
    }
}

test_dict = {
    'drive_drive':{
        'test_data': 'drive',
        'model_data': 'test',
        'test_model': 'drive'
    },'stare_drive':{
        'test_data': 'stare',
        'model_data': 'train',
        'test_model': 'drive'
    },'chase_drive':{
        'test_data': 'chase',
        'model_data': 'train',
        'test_model': 'drive'
    },'drive_drive':{
        'test_data': 'drive',
        'model_data': 'test',
        'test_model': 'drive'
    },'drive_stare':{
        'test_data': 'drive',
        'model_data': 'test',
        'test_model': 'stare'
    },'drive_chase':{
        'test_data': 'drive',
        'model_data': 'test',
        'test_model': 'chase'
    },'stare_stare':{
        'test_data': 'stare',
        'model_data': 'train',
        'test_model': 'stare'
    },'chase_stare':{
        'test_data': 'chase',
        'model_data': 'train',
        'test_model': 'stare'
    },'stare_chase':{
        'test_data': 'stare',
        'model_data': 'train',
        'test_model': 'chase'
    },'chase_chase':{
        'test_data': 'chase',
        'model_data': 'train',
        'test_model': 'chase'
    },
}
