# Copyright (c) 2017 Henry Leopold. Unreleased.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Training Configuration Flags"""
from __future__ import print_function, division, absolute_import
__author__ = 'Henry Leopold'
import tensorflow as tf

####################################################################################
# Table of Contents (for this file)
####################################################################################
##### Training Flags #####
# >>> Batches / Queues
# >>> Cross Validation Parameters
# >>> Batch Normalization Flags
# >>> Optimization Flags
# >>> Learning Rate Flags
# >>> Validation Flags
# >>> Loss Flags
#

# ==================================================================================
# Training Flags
# ==================================================================================
tf.app.flags.DEFINE_integer('max_steps', 20000, ' max number of steps to train ')
tf.app.flags.DEFINE_integer('num_epochs', 1, 'The number of epochs for training.')
tf.app.flags.DEFINE_float('weight_decay', 4e-5, 'The weight decay on the model weights.')
tf.app.flags.DEFINE_boolean('use_distortion', True, 'Determines if training images will be distorted.')

# -----------------------------
# Batches / Queues
# -----------------------------
tf.app.flags.DEFINE_integer('batch_size', 128, 'Batch size to be generated.')
tf.app.flags.DEFINE_float('min_fraction_of_examples_in_queue', 0.5, 'Size of queue to be generated.')
tf.app.flags.DEFINE_integer('min_queue_examples', 1000, ' min examples to queue up')
tf.app.flags.DEFINE_integer('num_preprocess_threads', 10, 'Number of threads to use during batch generation.')
tf.app.flags.DEFINE_boolean('shuffle', True, 'Whether to shuffle during batch generation.')
# -----------------------------
# Cross Validation Parameters
# -----------------------------
tf.app.flags.DEFINE_boolean('use_xval', False, 'Whether to use cross-validation during data generation and training.')
tf.app.flags.DEFINE_integer('xval_splits',4,'Cross-validation size [number of k-folds].')
tf.app.flags.DEFINE_integer('xval_start',0,'Resume training xval at a specific segment.')
tf.app.flags.DEFINE_integer('xval_stop',-1,'Stop training xval at a specific segment.')
tf.app.flags.DEFINE_integer('xval_key_clip_f',0,'Cross-validation data-generation image name clipping [prefix].')
tf.app.flags.DEFINE_integer('xval_key_clip_b',0,'Cross-validation data-generation image name clipping [postfix].')

# -----------------------------
# Batch Normalization Flags
# -----------------------------
tf.app.flags.DEFINE_bool('use_batchnorm', True, 'Whether to use batch normalization during training.')
tf.app.flags.DEFINE_float('bn_decay', 0.9, 'Sets the default weight decay for batch norm.')
tf.app.flags.DEFINE_float('bn_epsilon', 1e-5, 'Sets the default variance epsilon for batch norm.')

# -----------------------------
# Optimization Flags
# -----------------------------
tf.app.flags.DEFINE_string('optimizer', 'adam','The name of the optimizer, one of "adadelta", "adagrad", "adam", "ftrl", "momentum", "sgd" or "rmsprop".')
# ------------ General Params -----------------
tf.app.flags.DEFINE_float('momentum', 0.9,'The momentum for the MomentumOptimizer and RMSPropOptimizer.')
tf.app.flags.DEFINE_float('opt_epsilon', 1e-08, 'Epsilon term for the optimizer.')
tf.app.flags.DEFINE_float('decay_rate', 0.9, 'Sets the default weight decay for batch norm.')
# ------------- Adam Optimizer ----------------
tf.app.flags.DEFINE_float('adam_beta1', 0.9,'The exponential decay rate for the 1st moment estimates.')
tf.app.flags.DEFINE_float('adam_beta2', 0.999,'The exponential decay rate for the 2nd moment estimates.')
tf.app.flags.DEFINE_float('adam_eps', 1e-08, '')
# TODO "adadelta", "adagrad", "ftrl", "momentum", or "rmsprop"
# ------------- Adagrad ----------------
# ------------- RMSProp----------------

# -----------------------------
# Learning Rate Flags
# -----------------------------
tf.app.flags.DEFINE_float('learning_rate', 1e-5, 'Initial learning rate.')
tf.app.flags.DEFINE_float('label_smoothing', 0.0, 'The amount of label smoothing.')
tf.app.flags.DEFINE_integer('train_samples_per_epoch', 1000,'The Number of training samples to process per training epoch.')
tf.app.flags.DEFINE_bool('sync_replicas', False, 'Whether or not to synchronize the replicas during training.')
tf.app.flags.DEFINE_integer('replicas_to_aggregate', 1,'The Number of gradients to collect before updating params.')
# ----------- General Decay Params --------------
tf.app.flags.DEFINE_string('learning_rate_decay_type','fixed','Specifies how the learning rate is decayed. One of "fixed", "exponential","" or "polynomial"')
tf.app.flags.DEFINE_float('num_epochs_per_decay', 5.0,'Number of epochs after which learning rate decays.')
tf.app.flags.DEFINE_integer('learning_rate_decay_steps', 500,'Number of steps after which learning rate decays.')
# ----------- Moving Average Decay --------------
tf.app.flags.DEFINE_float('learning_rate_moving_average_decay', 0.9,'The decay to use for the moving average.'
                          'If left as None, then moving averages are not used.')
# ----------- Polynomial Decay ------------------
tf.app.flags.DEFINE_float('learning_rate_poly_power', 1.0,'Polynomial power variable.')
tf.app.flags.DEFINE_bool('learning_rate_poly_cycle', False, 'Cycle variable; see tf docs.')
tf.app.flags.DEFINE_float('learning_rate_end', 0.0001, 'The minimal end learning rate used by a polynomial decay learning rate.')
# ----------- Exponential Decay -----------------
tf.app.flags.DEFINE_float('learning_rate_decay_factor', 0.94, 'Learning rate decay factor.')
tf.app.flags.DEFINE_bool('learning_rate_staircase', True, 'Whether or not to use staircase typed exponential decay.')

# -------------------------------------------------------------
# Validation Flags
# -------------------------------------------------------------
tf.app.flags.DEFINE_integer('eval_examples_per_epoch', 1,'The Number of training samples to process per validation during training.')
tf.app.flags.DEFINE_integer('eval_batch_size', 1,'The Number of training samples to process per validation during training.')
# -----------------------------
# Loss Flags # TODO loss config
# -----------------------------
tf.app.flags.DEFINE_string('loss_type', 'margin_loss', 'Specified which loss function to use')
tf.app.flags.DEFINE_float('loss_m_minus', 0.1, '')
tf.app.flags.DEFINE_float('loss_m_plus', 0.9, '')
tf.app.flags.DEFINE_float('loss_lambda', 0.5, '')
tf.app.flags.DEFINE_float('loss_gamma', 0.0005, '')
