# Copyright (c) 2017 Henry Leopold. Unreleased.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Dataset Configuration Flags"""
from __future__ import print_function, division, absolute_import
__author__ = 'Henry Leopold'
import tensorflow as tf



####################################################################################
# Table of Contents (for this file)
####################################################################################
##### Dataset Flags #####
# >>> Data Attributes
# >>> Directories and Files
###########################
##### Preprocessing Flags #####
# >>> Colour Space Enhancement
# >>> Image (data) Manipulation

# ==============================================================================
# Data Flags
# ==============================================================================
tf.app.flags.DEFINE_string('input_type', 'tfrecords','Type of files being input into the system. Defaults to "tfrecords"."images" is another option.')

# -------------------------------------------------------------
# Data Attributes
# -------------------------------------------------------------
tf.app.flags.DEFINE_integer('num_classes',10,'Number of input channels the network/model should expect.')
tf.app.flags.DEFINE_integer('class_depth',1,'Depth of class hierarchy/parallelisms.')
tf.app.flags.DEFINE_integer('num_channels',3,'Number of input channels the network/model should expect.')
tf.app.flags.DEFINE_integer('mask_channels',1,'Number of mask input channels (usually one per label) the network/model should expect.')

# -------------------------------------------------------------
# Directories and Files
# -------------------------------------------------------------
tf.app.flags.DEFINE_string('root_dir', '/root/','Absolute base directory')
tf.app.flags.DEFINE_string('data_dir', 'data/dataset/','Directory path for data. Defaults to DRIVE training folder.')
tf.app.flags.DEFINE_string('data_set', 'drive','Name of dataset images to load (drive, chase, stare).')
tf.app.flags.DEFINE_string('train_dir', 'train/','Data sub-folder for training tasks')
tf.app.flags.DEFINE_string('test_dir', 'test','Data sub-folder for testing tasks')
tf.app.flags.DEFINE_string('checkpoint_dir', None,'If left as "None", defaults to train_dir.')
tf.app.flags.DEFINE_string('tfrecord_dir', 'data/','directory to save/load tfrecords')
tf.app.flags.DEFINE_string('tfrecord_name', 'train','filename to save/load tfrecords')
tf.app.flags.DEFINE_string('mask_ext', '_mask','File name extension that identifies mask files.\nEx. File name Image01.png; Mask name Image01_mask.png')

# ==============================================================================
# Preprocessing Flags
# ==============================================================================
# -------------------------------------------------------------
# Colour Space Enhancement
# -------------------------------------------------------------
tf.app.flags.DEFINE_bool('prep_histo', False, 'Use histogram normalization during preprocessing.')
tf.app.flags.DEFINE_bool('prep_clahe', True, 'Use CLAHE during preprocessing.')
tf.app.flags.DEFINE_bool('mean_norm', True, 'Use mean normalization during preprocessing.')
tf.app.flags.DEFINE_float('prep_clahe_clip', 2.0, 'CLAHE clip limit')
tf.app.flags.DEFINE_integer('prep_clahe_ksize', 8, 'Size of CLAHE kernel')
tf.app.flags.DEFINE_string('lab_cs',False,'Convert images to LAB colour space during training/testing.')
# tf.app.flags.DEFINE_string('hsv_cs',False,'Convert images to HSV colour space during training/testing. Not enabled.')
# tf.app.flags.DEFINE_string('gray_cs',False,'Convert images to gray colour space during training/testing. Not enabled.')
tf.app.flags.DEFINE_bool('prep_gamma', False, 'Use adjust gamma during preprocessing.')
tf.app.flags.DEFINE_float('prep_gamma_val', 1, 'Gamma value')
# -------------------------------------------------------------
# Image (data) Manipulation
# -------------------------------------------------------------
tf.app.flags.DEFINE_bool('split_channels', False, 'Whether to split channels during data generation.')
tf.app.flags.DEFINE_boolean('crop', False, 'If false, the entire image is processed during training. If true, the image is cropped.')
tf.app.flags.DEFINE_boolean('crop_resize', True, 'If true, the image is cropped to a random dimensions (crop_size/4 to crop_size) and resized to crop_size. This overrides crop.')
tf.app.flags.DEFINE_boolean('resize', True, 'If true, the image resized to norm_resize [typically for testing].')
tf.app.flags.DEFINE_integer('min_crop', 216, 'Minimum size input image crop to be extracted [SIZExSIZE in pixels].')
tf.app.flags.DEFINE_integer('crop_size', 256, 'Size of input image [SIZExSIZE in pixels] to be generated for training.')
tf.app.flags.DEFINE_integer('norm_resize', 256, 'New size for image to normalize accross datasets [SIZExSIZE in pixels].')
tf.app.flags.DEFINE_string('data_format', 'NCHW', 'Converts images to NCHW format. Else, assumes NHWC')
