# Copyright (c) 2017 Henry Leopold. Unreleased.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Cluster Configuration Flags"""
from __future__ import print_function, division, absolute_import
__author__ = 'Henry Leopold'
import tensorflow as tf



####################################################################################
# Table of Contents (for this file)
####################################################################################
##### Session Config Flags #####
# >>> Config Flags
# >>> Data -> GPU allocation flags
# >>> Cluster Specification Flags
#

# ==============================================================================
# Session Config Flags
# ==============================================================================
tf.app.flags.DEFINE_bool('var_on_cpu', True, 'Whether to specifically place variables on the cpu or allow for dynamic allocation (False).')
tf.app.flags.DEFINE_string('var_dtype', 'float32','How to store variables. Typically float32 or float16')

# -------------------------------------------------------------
# Config Flags
# -------------------------------------------------------------
tf.app.flags.DEFINE_boolean('allow_gpu_growth', True, 'Control if GPU allocates all memory upfront or enable growth.')
tf.app.flags.DEFINE_boolean('allow_soft_placement', True, 'Variable placement on multiple devices.')
tf.app.flags.DEFINE_boolean('force_gpu_compatible', True, 'Variable placement on multiple devices.')
tf.app.flags.DEFINE_integer('num_intra_threads', 0, 'Number of threads to use for intra-op parallelism. When training on CPU set to 0 to have the system pick the appropriate number or alternatively set it to the number of physical CPU cores.')
tf.app.flags.DEFINE_integer('num_inter_threads', 0, 'Number of threads to use for inter-op parallelism. If set to 0, the system will pick an appropriate number.')
tf.app.flags.DEFINE_boolean('log_device_placement', False, 'Whether to log device placement.')
tf.app.flags.DEFINE_float('per_process_gpu_memory_fraction', 1.0, 'Percent of GPU memory.')

# -------------------------------------------------------------
# Data -> GPU allocation flags
# -------------------------------------------------------------
tf.app.flags.DEFINE_integer('num_gpus', 1,'How many GPUs to use.')
tf.app.flags.DEFINE_integer('gpu_batch_queue', 4,'Size of batch queues on each gpu.')
#TODO Update for each GPU...
tf.app.flags.DEFINE_boolean('clone_on_cpu', False,'Use CPUs to deploy clones.')
# tf.app.flags.DEFINE_boolean('sync', False,'If present when running in a distributed environment will run on sync mode') # a parser argument
# tf.app.flags.DEFINE_string('variable-strategy', 'CPU', 'Where to locate variable operations') # a parser argument

# -------------------------------------------------------------
# Cluster Specification Flags
# -------------------------------------------------------------
# ------------ tf.train.ClusterSpec -----------
tf.app.flags.DEFINE_string("ps_hosts", "",
                           "Comma-separated list of hostname:port pairs")
tf.app.flags.DEFINE_string("worker_hosts", "",
                           "Comma-separated list of hostname:port pairs")

# --------------- tf.train.Server --------------
tf.app.flags.DEFINE_string("job_name", "", "One of 'ps', 'worker'")
tf.app.flags.DEFINE_integer("task_index", 0, "Index of task within the job")
