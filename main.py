# Copyright (c) 2017 Henry A Leopold.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Main script for running project scripts"""
__author__ = 'Henry A Leopold'

import tensorflow as tf
import horovod.tensorflow as hvd

from utils import create_tfrecords, clean_dir, make_checkpoint_path, build_path, make_dir, debug, clear_dir
from train import train
from test import test
from config import cfg

if any((cfg.log_level == 'debug', cfg.debug)):
    tf.logging.set_verbosity(tf.logging.DEBUG)
else:
    tf.logging.set_verbosity(tf.logging.INFO)

def main(argv=None):  # pylint: disable=unused-argument

    # Initialize Horovod
    hvd.init()

    # TODO dataset gen
    dataset = tf.contrib.learn.datasets.mnist.read_data_sets('datasets/MNIST-data-%d' % hvd.rank())

    # Set up session config
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = cfg.allow_gpu_growth
    config.gpu_options.per_process_gpu_memory_fraction = cfg.per_process_gpu_memory_fraction
    config.gpu_options.visible_device_list = str(hvd.local_rank())
    config.allow_soft_placement=cfg.allow_soft_placement
    config.log_device_placement=cfg.log_device_placement

    if cfg.execute == 'train':
        train(dataset, config)
    elif cfg.execute == 'test': #TODO TEST
        test(dataset, config)
    elif cfg.execute == 'finetune': # TODO FINETINE
        train(dataset, config)
    elif cfg.execute == 'data_gen':
        make_dir(build_path([cfg.tfrecord_dir, cfg.exp_id]))
        create_tfrecords()
    elif cfg.execute == 'debug':
        model_ckpt_dir = make_checkpoint_path(build_path([cfg.log_dir, cfg.model]), [cfg.exp_id, cfg.subexp], build_as_dir=False)
        clear_dir(model_ckpt_dir)
        debug(config)


if __name__ == '__main__':
    tf.app.run()
