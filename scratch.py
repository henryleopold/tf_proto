import numpy as np
import tensorflow as tf
from model.layers import *
from config import cfg

input_shape = [128, 9, 9, 32]
kernel_size = 3

filters = np.zeros(shape=[kernel_size, kernel_size, input_shape[3], kernel_size * kernel_size], dtype=np.float32)

for i in range(kernel_size):
    for j in range(kernel_size):
        filters[i, j, :, i * kernel_size + j] = 1.0


test = [1,33,6]
ap = np.zeros((4,1,3))
ap.shape
test.extend(ap.shape[-2:])
test


def _routing(self, inputs):
""" Capsule routing function controller (aka routing-router). """
if self._debug(): print('Routing capsule "%s" (shape: %s) with %s inputs '%(self.layer_name, inputs, self.routing))

# try:
if self._debug(): print('\n******************\nInputs raw: %s \n******************\n'%inputs)
# if there isn't a matrix, try to use vector based agreement.
# if self.routing_matrix_size is None:
self.routing_size = self.batch_size*self.shape[1]*self.shape[1]

inputs = tf.reshape(inputs, shape=(self.routing_size, 1, self.capnum_prior, 1, -1))
if self._debug(): print('Capsule reshaped: %s'%inputs)

# self.filters_prior = inputs.get_shape().as_list()[-1]
r_shape = (1, self.num_capsules, self.capnum_prior, inputs.get_shape().as_list()[-1], self.capsule_vector)
o_shape = (self.routing_size, self.num_capsules, self.capnum_prior, self.capsule_vector)
inputs.get_shape().as_list()

route_weights = tf.get_variable('routing_weight', shape=r_shape, dtype=tf.float32, initializer=self.routing_initializer, regularizer=self.route_regularizer)
self.route_weights = tf.tile(route_weights, [self.routing_size, 1, 1, 1, 1])
inputs = tf.tile(inputs, [1, self.num_capsules, 1, 1, 1])
self.u_hat = tf.reshape(tf.matmul(inputs, self.route_weights), shape=o_shape)
if self._debug(): print('\nRouting weights: %s\nu-hat: %s\t(shape: %s)'%(self.route_weights, self.u_hat, o_shape))

if self.routing == 'dynamic':
    return self._dynamic_routing()
elif self.routing == 'em':
    return self._em_routing()
else:
    print('Routing type "%s" not recognized.'%(self.routing))
# except Exception as e:
#     print('Routing exception has occured: %s'%e)
#     self.debug()
