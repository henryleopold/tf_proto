import tensorflow as tf

flags = tf.app.flags

flags.DEFINE_integer('num_classes', 10, 'number of lables')
flags.DEFINE_float('epsilon', 1e-8, 'global divisor epsilon')
flags.DEFINE_integer('batch_size', 128, 'batch size')
flags.DEFINE_float('learning_rate', 0.001, '')
flags.DEFINE_float('adam_beta1', 0.9, '')
flags.DEFINE_float('adam_beta2', 0.999, '')
flags.DEFINE_float('adam_eps', 1e-08, '')

cfg = tf.app.flags.FLAGS
