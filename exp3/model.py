import tensorflow as tf
layers = tf.contrib.layers
from config import cfg
import horovod.tensorflow as hvd

def conv_model(feature, target, mode):
    """2-layer convolution model."""
    # Convert the target to a one-hot tensor of shape (batch_size, 10) and
    # with a on-value of 1 for each one-hot vector of length 10.
    target = tf.one_hot(tf.cast(target, tf.int32), cfg.num_classes, 1, 0)

    # Reshape feature to 4d tensor with 2nd and 3rd dimensions being
    # image width and height final dimension being the number of color channels.
    feature = tf.reshape(feature, [-1, 28, 28, 1])

    # First conv layer will compute 32 features for each 5x5 patch
    with tf.variable_scope('conv_layer1'):
        h_conv1 = layers.conv2d(
            feature, 32, kernel_size=[5, 5], activation_fn=tf.nn.relu)
        h_pool1 = tf.nn.max_pool(
            h_conv1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

    # Second conv layer will compute 64 features for each 5x5 patch.
    with tf.variable_scope('conv_layer2'):
        h_conv2 = layers.conv2d(
            h_pool1, 64, kernel_size=[5, 5], activation_fn=tf.nn.relu)
        h_pool2 = tf.nn.max_pool(
            h_conv2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        # reshape tensor into a batch of vectors
        h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])

    # Densely connected layer with 1024 neurons.
    h_fc1 = layers.dropout(
        layers.fully_connected(
            h_pool2_flat, 1024, activation_fn=tf.nn.relu),
        keep_prob=0.5,
        is_training=mode == tf.contrib.learn.ModeKeys.TRAIN)

    # Compute logits (1 per class) and compute loss.
    logits = layers.fully_connected(h_fc1, cfg.num_classes, activation_fn=None)
    loss = tf.losses.softmax_cross_entropy(target, logits)

    return tf.argmax(logits, 1), loss

class Model(object):
    def __init__(self, images, labels, is_training=True):
        self.X, self.labels = images, labels
        self.Y = tf.one_hot(self.labels, depth=cfg.num_classes, axis=1, dtype=tf.float32)
        self.build_arch(is_training)
        if is_training:
            self._loss()
            self._performance()
            self.summary()
            self.global_step = tf.Variable(0, name='global_step', trainable=False)
            self._optimizer()
            self.train_op = self.optimizer.minimize(self.loss, global_step=self.global_step)

    def build_arch(self, is_training):
        with tf.variable_scope('conv_layer1'):
            h_conv1 = layers.conv2d(
                self.X, 32, kernel_size=[5, 5], activation_fn=tf.nn.relu)
            h_pool1 = tf.nn.max_pool(
                h_conv1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

        # Second conv layer will compute 64 features for each 5x5 patch.
        with tf.variable_scope('conv_layer2'):
            h_conv2 = layers.conv2d(
                h_pool1, 64, kernel_size=[5, 5], activation_fn=tf.nn.relu)
            h_pool2 = tf.nn.max_pool(
                h_conv2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
            # reshape tensor into a batch of vectors
            h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])

        # Densely connected layer with 1024 neurons.
        h_fc1 = layers.dropout(
            layers.fully_connected(
                h_pool2_flat, 1024, activation_fn=tf.nn.relu),
            keep_prob=0.5,
            is_training=is_training)

        # Compute logits (1 per class) and compute loss.
        self.logits = layers.fully_connected(h_fc1, cfg.num_classes, activation_fn=None)
        self.argmax = tf.to_int32(tf.argmax(self.logits, 1))
        # self.argmax = tf.reshape(self.argmax, shape=(cfg.batch_size, )),

    def _loss(self):
        self.loss = tf.losses.softmax_cross_entropy(self.Y, self.logits)

    def _optimizer(self):
        # Horovod: adjust learning rate based on number of GPUs.
        optimizer = tf.train.AdamOptimizer(cfg.learning_rate * hvd.size(), cfg.adam_beta1, cfg.adam_beta2, cfg.adam_eps)

        # Horovod: add Horovod Distributed Optimizer.
        self.optimizer = hvd.DistributedOptimizer(optimizer)

    def _performance(self):
        #TODO
        self.accuracy, _ = tf.metrics.accuracy(tf.to_int32(self.labels), self.argmax)
        self.precision, _ = tf.metrics.precision(tf.to_int32(self.labels), self.argmax)
        self.recall, _ = tf.metrics.recall(tf.to_int32(self.labels), self.argmax)
        self.auc, _ = tf.metrics.auc(tf.to_int32(self.labels), self.argmax)

    def summary(self):
        train_summary = []
        train_summary.append(tf.summary.scalar('train/loss', self.loss))
        # train_summary.append(tf.summary.scalar('train/acc', self.accuracy))
        # train_summary.append(tf.summary.scalar('train/auc', self.auc))
        # train_summary.append(tf.summary.scalar('train/Pr', self.precision))
        # train_summary.append(tf.summary.scalar('train/Sn', self.recall))
        input_img = tf.reshape(self.X, shape=(cfg.batch_size, 28, 28, 1))
        self.train_summary = tf.summary.merge(train_summary)

    def train(self, sess, inputs):
        _, loss, train_acc, summary_str = sess.run([self.train_op, self.loss, self.accuracy, self.train_summary])
