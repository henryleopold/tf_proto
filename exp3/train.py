#!/usr/bin/env python3
import tensorflow as tf
import numpy as np
import horovod.tensorflow as hvd
learn = tf.contrib.learn
from model import conv_model, Model
from config import cfg
tf.logging.set_verbosity(tf.logging.INFO)


def train(inputs, config, checkpoint_dir):
    with tf.name_scope('input'):
        image = tf.placeholder(tf.float32, [cfg.batch_size, 28, 28, 1], name='image')
        label = tf.placeholder(tf.int32, [cfg.batch_size,], name='label')


    tf.logging.info('Loading Graph...')

    model = Model(image, label)
    tf.logging.info('Graph loaded')

    hooks = [
        # Horovod: BroadcastGlobalVariablesHook broadcasts initial variable states
        # from rank 0 to all other processes. This is necessary to ensure consistent
        # initialization of all workers when training is started with random weights
        # or restored from a checkpoint.
        hvd.BroadcastGlobalVariablesHook(0),

        # Horovod: adjust number of steps based on number of GPUs.
        tf.train.StopAtStepHook(last_step=20000 // hvd.size()),

        tf.train.LoggingTensorHook(tensors={'step': model.global_step, 'loss': model.loss,
        # 'labels': model.labels, 'pred': model.argmax, 'acc': model.accuracy, 'Pr': model.precision, 'Sn': model.recall, 'auc': model.auc
        }, every_n_iter=10),
    ]

    # Horovod: save checkpoints only on worker 0 to prevent other workers from
    # corrupting them.
    checkpoint_dir = './checkpoints' if hvd.rank() == 0 else None

    # The MonitoredTrainingSession takes care of session initialization,
    # restoring from a checkpoint, saving to a checkpoint, and closing when done
    # or an error occurs.
    with tf.train.MonitoredTrainingSession(checkpoint_dir=checkpoint_dir,
                                           hooks=hooks,
                                           #save_summaries_steps=,
                                           config=config) as mon_sess:
        while not mon_sess.should_stop():
            # Run a training step synchronously.
            image_, label_ = inputs.train.next_batch(cfg.batch_size)
            mon_sess.run(model.train_op, feed_dict={image: np.reshape(image_, [-1, 28,28, 1]), label: label_})
