#!/usr/bin/env python

import tensorflow as tf
import horovod.tensorflow as hvd
learn = tf.contrib.learn

from train import train


tf.logging.set_verbosity(tf.logging.INFO)


def main(_):
    # Horovod: initialize Horovod.
    hvd.init()

    # Download and load MNIST dataset.
    mnist = learn.datasets.mnist.read_data_sets('MNIST-data-%d' % hvd.rank())

    # Horovod: pin GPU to be used to process local rank (one GPU per process)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.gpu_options.visible_device_list = str(hvd.local_rank())

    # Horovod: save checkpoints only on worker 0 to prevent other workers from
    # corrupting them.
    checkpoint_dir = './checkpoints' if hvd.rank() == 0 else None

    train(mnist, config, checkpoint_dir)


if __name__ == "__main__":
    tf.app.run()
