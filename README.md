**Status:** Archive (code is provided as-is, no updates expected)
# Tensorflow Experiments and a "Proto Factory" that uses hyperparameters stored in dictionary like tuples

<!--![Contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=plastic)](CONTRIBUTING.md)-->
<!--![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=plastic)](https://opensource.org/licenses/Apache-2.0)-->
<!--![completion](https://img.shields.io/badge/completion%20state-80%25-blue.svg?style=plastic)-->

## Description
- exp1: initial horovod experiments (*working*)
- exp2: Supervisor instead of *MonitoredTrainingSession* (*failing likely due to deprication of supervisor*)
- exp3: Custom on *MonitoredTrainingSession* (*working*: kpis not)

## Requirements
- Python 3
- NumPy 1.13.1
- Tensorflow 1.3.0
- Sklearn: 0.18.1
- Matplotlib

## Install

    $> git clone https://github.com/
    $> cd .git
    $> wget
    $> unzip datasets.zip

## Executing
    $> python main.py -h

Checkpoints are saved to...

### Train

    $> python main.py